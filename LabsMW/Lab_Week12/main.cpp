/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 12, 2014, 4:09 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

const int COL = 3;
void problem1();
void outputArray (int[], int);
void bubbleSort (int[], int);
void problem2();
void selectionSort (int[], int);
void problem3();
void outputMultiArray (int[][COL], int);
void problem4();
void selectionSort (vector<int>&);
int binarySearch (const vector<int>&, int);



/*
 * 
 */
int main(int argc, char** argv) {
    
    //Main Program Menu
    cout << "Lab Week 12" << endl << endl;
    
    cout << "You may enter -1 to Quit" << endl << endl;
    cout << "Enter 1 for Problem #1" << endl;
    cout << "Enter 2 for Problem #2" << endl;
    cout << "Enter 3 for Problem #3" << endl;
    cout << "Enter 4 for Problem #4" << endl;
    
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1:
            problem1();
            break;
        case 2:
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 4:
            problem4();
            break;
        default: 
            cout << "Error" << endl;
            break;
    }

    return 0;
}

void problem1()
{
    const int size = 5;
    int array1[] = {3,6,1,4,2};
    
    cout << "Before bubble sort: " << endl;
    outputArray (array1, size);
    
    cout << endl;
    
    cout << "After bubble sort: " << endl;
    bubbleSort (array1, size);
    outputArray (array1, size);
    
    
}

void outputArray (int array1[], int size)
{
    
    for (int i = 0; i < size; i++)
    {
        cout << array1[i] << " ";
    }
}

void bubbleSort (int array1[], int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (array1[j] > array1[j+1])
            {
                swap (array1 [j], array1 [j+1]);
            }
        }
    }
    
}

void problem2()
{
    const int size = 7;
    int array1[] = {3,6,1,4,2,7,9};
    
    cout << "Before selection sort: " << endl;
    outputArray (array1, size);
    
    cout << endl;
    
    cout << "After selection sort: " << endl;
    selectionSort (array1, size);
    outputArray (array1, size);
    
  
    
}

void selectionSort (int array1[], int size)
{
    for (int i = 0; i < size; i++)
    {
        int loc = -1;
        int min = 999999999;
        for (int j = i; j < size; j++)
        {
            if (array1[j] < min)
            {
                min = array1[j];
                loc = j;
            }
        }
        swap (array1[i], array1[loc]);
    }
    
}

void problem3()
{
    ifstream infile;
    
    cout << "Enter 'scores.dat' to read tests scores: ";
    string fileName;
    cin >> fileName;
    
    infile.open(fileName.c_str());
    
    while(!infile)
    {
        cout << "Wrong file name. Try again: ";
        cin >> fileName;
        
        infile.open(fileName.c_str());
    }
    
    string word;
    infile >> word;
    infile >> word;
    infile >> word;
    int numStudents;
    infile >> numStudents;
    
    infile >> word;
    infile >> word;
    infile >> word;
    int numTests;
    infile >> numTests;
    
    int size = numStudents * numTests;
    int scores [numStudents][COL];
    
    
        for (int i = 0; i < numStudents; i++)
        {
            int num;
            for (int j = 0; j < COL; j++)
            {
                infile >> num;
                scores [i][j] = num;
            }

        }
    
    outputMultiArray (scores, numStudents);
    
    infile.close();
    
    
}
void outputMultiArray (int scores[][COL], int row)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            cout << scores [i][j] << " ";
        }
        cout << endl;
    }
}

void problem4()
{
    cout << "Sorting vector..." << endl;
    
    vector<int> v;
    
    srand(time(0));
    for (int i = 0; i < 100000; i++)
    {
        int num = (rand() % (100000 - 0 + 1) + 0);
        v.push_back(num);
    }
    
    selectionSort (v);
    
    cout << "Enter a value between 1 - 100,000 to find: ";
    int value;
    cin >> value;
    
    
    int locVal = binarySearch (v, value);
    
    if (locVal == -1)
        cout << "The vector is empty." << endl;
    else if (locVal == -2)
        cout << "The value does not exist in the vector." << endl;
    else
        cout << "The value you entered is at location " << locVal << 
                " in the vector." << endl;
  
    
    
}
void selectionSort (vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        int loc = -1;
        int min = 9999999;
        for (int j = i; j < v.size(); j++)
        {
            if (v[j] < min)
            {
                min = v[j];
                loc = j;
            }
        }
        swap (v[i], v[loc]);
    }
    
}
int binarySearch (const vector<int>& v, int value)
{
    if (v.size() == 0)
        return -1;
    
    int first, last, middle, pos;
    first = 0;
    last = v.size() - 1;
    middle = (first + last) / 2;
    
    while (v[middle] != value)
    {
        if (first > last) return -2;
        if (v[middle] > value)
        {
            last = middle - 1;
        }
        else 
        {
            first = middle + 1;
        }
        middle = (first + last) / 2;
        
    }
    
    return middle;
    
}
