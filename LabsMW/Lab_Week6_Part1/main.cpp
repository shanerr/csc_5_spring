/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 24, 2014, 4:35 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Lab Week 6 Problem 1
    //Prompt user
    cout << "Enter a filename: ";
    ifstream infile;
    string fileName;
    cin >> fileName;

    
    //Open File
    infile.open (fileName.c_str());
    
    //Read from file
    string string_from_file;
    int charCount = 0;
    
    cout << "The file reads: ";
    
    while (!infile.eof())
    {
        infile >> string_from_file;
        cout << " " << string_from_file;
        
        for (int i = 0; i < string_from_file.size(); i++)
        {
            string_from_file [i];
            charCount++;
        }
        
        
    }
    
    //Outputting Character Count
    cout << endl << "Number of Characters in file: " << charCount << endl;
    
    //Close file 
    infile.close();
    
    
    //Lab Week 6 Problem 3
    int n;
    cin >> n;
    double x = 0;
    double s;
    do
    {
        s = 1.0 / (1 + n * n);
        n++;
        x = x + s;
    }
    while (s > 0.01);
    
    cout << s << endl;
    cout << n << endl;
    
    
    int n2;
    cin >> n2;
    double x2 = 0;
    double s2;
    
    
    while (true)
    {
        if (s2 > 0.01)
        {       
            s2 = 1.0 / (1 + n2 * n2);
            n2++;
            x2 = x2 + s2;
        }
        else
        {
            break;
        }
    }
    
    cout << s2 << endl;
    cout << n2 << endl;
   
    
    return 0;
}

