/* 
 * File:   main.cpp
 * Author: Thomas
 *
 * Created on March 11, 2014, 10:08 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Lab Week 4 PROBLEM #1
    
    //Variable Declaration
    int numGrade;
    
    //Prompting User
    
    do 
    {   
        cout << "You can enter '150' at any time to quit this program." <<
                endl;
    cout << "Please enter a number between 1-100 to determine "
            "your letter grade: ";
    cin >> numGrade;
    
    //If-else-if statements
    
    if (numGrade <= 100 && numGrade >= 90)
    {
        cout << "Your grade is an 'A'" << endl << endl;
    }
    else if (numGrade <= 89 && numGrade >= 80) 
    {
        cout << "Your grade is a 'B'" << endl << endl;
    }
    else if (numGrade <= 79 && numGrade >= 70)
    {
        cout << "Your grade is a 'C'" << endl << endl;
    }
    else if (numGrade <= 69 && numGrade >= 60)
    {
        cout << "Your grade is a 'D'" << endl << endl;
    }
    else  
    {
        cout << "Your grade is an 'F'" << endl << endl;
    }
        }
    while (numGrade != 150);
        
    //***The previous problem must be commented out to run this 
    //program***
    
    //Lab Week 4 PROBLEM #2
    
    //a)
    int x = 0;
    while (x < 10)
    {
        cout << x << endl;
        x++;
    }
    
    
    //Lab Week 4 PROBLEM #3
    
    for (int i = 0; i < 10; i++)
    {
        cout << "Please enter a value: ";
        cin >> i;
        cout << "You entered: " << i << endl << endl;
    }
    
    //Lab Week 4 PROBLEM #4
    
    string letterGrade; 
            
    
    //Prompting User
    cout << "You can enter 'Quit' at any time to quit this program." <<
            endl;
    cout << "Please enter a letter grade to "
            "determine your percentage range: "
            << endl;
    
   
    cin >>  letterGrade;
    
    while (letterGrade != "Quit")
    {
    if ( letterGrade == "A+" || letterGrade == "a+")
    {
        cout << "Your percentage is 100+%" << endl << endl;
        cout << "Enter another letter grade: " << endl;
    }
    else if (letterGrade == "A" || letterGrade == "a")
    {
        cout << "Your percentage is 93-100%" << endl << endl;
        cout << "Enter another letter grade: " << endl;   
    }
    else if (letterGrade == "A-" || letterGrade == "a-")
    {
        cout << "Your percentage is 90-92.9%" << endl << endl;
        cout << "Enter another letter grade: " << endl;
    }
    else if (letterGrade == "B+" || letterGrade == "b+")
    {
        cout << "Your percentage is 87-89.9%" << endl << endl;
        cout << "Enter another letter grade: " << endl;
    }
    else if (letterGrade == "B" || letterGrade == "b")
    {
        cout << "Your percentage is 83-86.9%" << endl << endl;
        cout << "Enter another letter grade: " << endl;
    }
    else if (letterGrade == "B-" || letterGrade == "b-")
    {
        cout << "Your percentage is 80-82.9%" << endl << endl;
        cout << "Enter another letter grade: " << endl;
    }
    else 
    {
        cout << "The letter grade you entered was incorrect" << endl;
        cout << "Try another grade: " << endl;
    }
    }
    
    
    
    
    return 0;
}

