/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 17, 2014, 4:34 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Lab Week 4 Problem #5
    //Guessing Game
  
    srand (time (0));
    
    int num = ((rand() % 100) + 1 );
    int userNum;
    int count = 0;
    
    //User Input 
         cout << "You have 10 attempts to guess the number between 1-100"
            << endl;
         cout << "Please enter a guess between 1-100: ";
         
         
    
    while (userNum != num && count < 10)
    {
        count++;
        cin >> userNum;
        
        
        if (userNum != num && count == 10)
        {
            cout << "You have exceeded the maximum number of attempts." <<
                    endl;
        }
        else if (userNum < num && count <= 10)
        {
            cout << endl <<"The number you entered was too low..." 
                    << endl;
            cout << "Please enter another guess: ";
        }
        else if (userNum > num && count <= 10)
        {
            cout << endl << "The number you entered was too high..." 
                    << endl;
            cout << "Please enter another guess: ";
        }
        else 
        {
            cout << "You guessed the correct number!" << endl;
        }
        
    }
         
         
         
         
    
    
    
       
    

    return 0;
}

