/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 19, 2014, 4:46 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>



using namespace std;


/*
 * 
 */
int main(int argc, char** argv) {
    
//    //Lab Week 5 Part 1 Problem 1
//    //Balance/Interest Calculator
//    
//    int quit = 999;
//    cout << "You may enter '999' to quit this program." << endl;
//    cout << "Please enter your balance to calculate the interest due: $"; 
//            
//    double balance;
//    double interestDue;
//    double totalAmountDue;
//    double minPayment;
//    
//    
//    
//    while (balance != 999)
//    {
//        cin >> balance;
//        if  (balance == quit)
//        {
//            cout << "Thank for using our program." << endl << "Goodbye.";
//        }
//        else
//        {
//        if (balance <= 1000.00)
//        {
//            interestDue = (balance * .015);
//        }
//        else 
//        {
//            interestDue = (1000 * .015) + ((balance - 1000)* .010);
//         }
//        if (balance < 10.00)
//        {
//            minPayment = (balance + interestDue);
//        }
//        else if ( (balance * .10) > 10.00 )
//        {
//            minPayment = (balance + interestDue) * .10;
//        }
//        else
//        {
//            minPayment = 10.00;
//        }
//        
//        cout << "Your interest due is: $" << setprecision(2) <<
//                fixed << interestDue << endl;
//        cout << "Your minimum payment is: $" << setprecision(2)
//         << fixed << minPayment << endl;
//        cout << endl << "Enter another amount: $";
//        
//        }
//     }
    
//    //Lab Week 5 Part 1 Problem #2
//    //Toothpick Game
//    
//    int numToothpicks = 23;
//    int compNum, userNum;
//    
//    cout << "Let's play the game known as '23'." << endl;
//    cout << "Whoever draws the last toothpick loses the game." << endl;
//    cout << "When it is your turn, you may draw 1,2, or 3 toothpicks." << endl
//            << endl;
//    
//    bool userWin;
//    
//    while (numToothpicks > 0)
//    {
//        
//        
//        cout << endl << "How many toothpicks would you like to draw? ";
//        int userNum;
//        cin >> userNum;
//
//        if (userNum < 1 || userNum > 3)
//        {
//            cout << "Invalid entry. Try again.";
//
//        }
//        else if (numToothpicks - userNum < 0)
//        {
//            cout << "Invalid entry. Try again.";
//        }
//        else
//        {
//            cout << "You drew " << userNum << " toothpicks." << endl;
//            numToothpicks -= userNum;
//            cout << "There are " << numToothpicks << " remaining." << endl;
//
//            int compNum;
//            srand (time (0));
//            compNum = (rand() % 1) + 1;
//
//            if(numToothpicks > 4)
//            {
//                compNum = 4 - userNum;
//            }
//            else if (numToothpicks == 4)
//            {
//                compNum = 3;
//                userWin = false;
//            }
//            else if (numToothpicks == 3)
//            {
//                compNum = 2;
//                userWin = false;
//            }
//            else if (numToothpicks == 2)
//            {
//                compNum = 1;
//                userWin = false;
//
//            }
//            else 
//            {
//                compNum = 1;
//            }
//
//            
//
//                if(numToothpicks <= 0)
//                {
//                    if (userWin)
//                    {
//                        cout << "You beat the computer!" << endl;
//                    }
//                    else 
//                    {
//                        cout << "The computer wins." << endl;
//                    }
//
//
//                }
//            
//
//            if (numToothpicks != 0)
//            {
//                cout << "The computer drew " << compNum << " toothpicks."
//                        << endl;
//                numToothpicks -= compNum;
//                cout << "There are " << numToothpicks << " remaining." 
//                        << endl;
//            }
//            else
//            {
//                cout << "Thank you for playing." << endl;
//            }
//        
//         }
//    }
//    
    
//    // Lab Week 5 Part 1 PROBLEM #3
//    //Palindrome 
//    
//    cout << "Please enter a word to determine if it is a palindrome: ";
//    string word;
//    cin >> word;
//    bool pal = true;
//    
//    for (int i = 0; i < word.size(); i++)
//    {
//        if (word [i] != word [word.size() - i - 1] )
//        {
//            pal = false;
//        }
//    }
//    
//    if (pal)
//    {
//        cout << "The word you entered IS a Palindrome." << endl;
//    }
//    else 
//    {
//        cout << "The word you entered IS NOT a Palindrome." << endl;
//    }
    
    
    // Lab Week 5 Part 1 PROBLEM #4
    // Star Staircase
    
    cout << "Enter a number to create a star staircase: ";
    int numStars;
    cin >> numStars;
    
    for (int i = 0; i <= numStars - 1; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            cout << "*";
        }
        i++;
        cout << " _ ";
        for (int k = numStars; k > i; k--)
        {
            cout << "*";
        }
        cout << endl;
    }
    

    return 0;
}

