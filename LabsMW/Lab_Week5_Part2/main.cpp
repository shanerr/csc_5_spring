/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 19, 2014, 4:20 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Lab Week 5 Part 2 Question 1
    
    int num1 = 10;
    int num2 = 15;
    int num3 = 23;
    int num4 = 27;
    int num5 = 11;
    int num6 = 13;
    int num7 = 5;
    int num8 = 91;
    int num9 = 99;
    int num10 = 1337;
    
    cout << num1 << "..." << num2 << endl;
    if (num2 > num1)
    {
        cout << num2 << " is greater than "<< num1 << endl;
    }
    else
    {
        cout << num1 << " is greater than " << num2 << endl;
    }
    
    cout << num3 << "..." << num4 << endl;
    if (num4 > num3)
    {
        cout << num4 << " is greater than " << num3 << endl;
    }
    else 
    {
        cout << num3 <<" is greater than " << num4 << endl;
    }
    
    cout << num5 << "..." << num6 << endl;
    if (num6 > num5)
    {
        cout << num6 << " is greater than "<< num5 << endl;
    }
    else 
    {
        cout << num5 << " is greater than " << num6 << endl;
    }
    
    cout << num7 << "..." << num8 << endl;
    if (num8 > num7)
    {
        cout << num8 << " is greater than " << num7 << endl;
    }
    else 
    {
        cout << num7 << " is greater than " << num8 << endl;
    }
    
    cout << num9 << "..." << num10 << endl;
    if (num10 > num9)
    {
        cout << num10 << " is greater than " << num9 << endl;
    }
    else 
    {
        cout << num9 << " is greater than " << num10 << endl;
    }
    
    
    return 0;
}

