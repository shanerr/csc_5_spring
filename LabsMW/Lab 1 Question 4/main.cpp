/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2014, 4:23 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * PROBLEM #4
 */
int main(int argc, char** argv) {
    
    // Variable Declaration 
    double input; 
    
    // Prompting user to enter a measurement in METERS
    cout << "Please enter a measurement in METERS: " << endl; 
    
    // User input 
    cin >> input; 

    // Variable Initialization 
    double miles = 1609.344; 
    double feet = 3.281;
    double inches = 3.281 * 12;
    
    
    // Calculate conversions
    double convertMiles = input / miles;
    double convertFeet = feet * input;
    double convertInches = inches * input;
    
    // Display conversions
    cout << endl; 
    cout << "Miles: "<< convertMiles << endl; 
    cout << "Feet: " << convertFeet << endl;
    cout << "Inches: " << convertInches << endl; 
    
    
    
    
    
    return 0;
}

