/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 30, 2014, 3:56 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctype.h>

using namespace std;

void problem1();
void problem2();
void problem3();
void problem4();
bool findValue(int array1[], int);
void problem5();
void numDuplicated (vector <int> numbers);

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Lab Week 10" << endl;
    cout << "Enter 1 for Vector Storing (#1)" << endl;
    cout << "Enter 2 for Array Storing (#2)" << endl;
    cout << "Enter 3 for Array Random Storing (#3)" << endl;
    cout << "Enter 4 for Value Finding (#4)" << endl;
    
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1: 
            problem1();
            break;
        case 2:
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 4:
            problem4();
            break;
        case 5:
            problem5();
            break;
        default:
            cout << "Error." << endl;
            break;
    }
    
    
    
    
    return 0;
}

void problem1()
{
    ifstream infile;
    cout << "Please enter a file: ";
    
    string fileName;
    cin >> fileName;
    
    infile.open (fileName.c_str());
    
    while (infile.fail())
    {
        cout << "The file does not exist. Try another: ";
        cin >> fileName;
        
        infile.open (fileName.c_str());
    }
    
    int contents;
    vector <int> numbers;
    
    while (!infile.eof())
    {   
        for (int i = 0; i < contents; i++)
        {
            infile >> contents;
            numbers.push_back(contents);
        }
    }
    infile.close();
}

void problem2()
{
    ifstream infile;
    cout << "Please enter a file: ";
    
    string fileName;
    cin >> fileName;
    
    infile.open (fileName.c_str());
    
    while (infile.fail())
    {
        cout << "The file does not exist. Try another: ";
        cin >> fileName;
        
        infile.open (fileName.c_str());
    }
    
    int numberArray [10];
    int contents;
    
    while (!infile.eof())
    {   
        for (int i = 0; i < fileName.size() ; i++)
        {
            infile >> contents;
            numberArray [i] = contents;
        }
    }
    
    infile.close();
}

void problem3()
{
    srand (time (0));
    int size = 50;
    
    int array1 [50];
    
    for (int i = 0; i < size; i++)
    {
        array1 [i] = ((rand()% (100 - 50 + 1) + 50));
    }
    
}

void problem4()
{   
    srand (time (0));
    int size = 50;
    
    int array1 [50];
    
    for (int i = 0; i < size; i++)
    {
        array1 [i] = ((rand()% (100 - 50 + 1) + 50));
    }
    
    cout << "Please enter a value: ";
    int value;
    cin >> value;
    
    bool valueFound = findValue (array1, value);
    
    if (valueFound)
    {
        cout << "The value you entered WAS found in the array." << endl;
    }
    else
    {
        cout << "The value you entered WAS NOT found in the array." << endl;
    }
    
}

bool findValue (int array1[], int value)
{
    int size = 50;
    
    for (int i = 0; i < size; i++)
    {
        if (array1 [i] == value)
            return true;
    }
    return false;
}

void problem5()
{
    srand (time (0));
    int size = 50;
    
    vector <int> numbers;
    
    for (int i = 0; i < size; i++)
    {
        int randNum = ((rand()% (100 - 50 + 1) + 50));
        numbers.push_back (randNum);
    }
    
    numDuplicated (numbers);
    
}

void numDuplicated (vector <int> numbers)
{
    for (int i = 0; i < numbers.size(); i++)
    {
       
    }
}
