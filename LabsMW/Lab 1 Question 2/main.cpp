/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 5:25 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * PROBLEM #1
 */
int main(int argc, char** argv) {
    
    int a = 7;
    int b = 10;
    int c = a;
    
    cout << "a:" << a << " " << "b:" << b << endl;
    a = b;
    b = a;
    cout << "a:" << b << " " << "b:" << c << endl; 

    return 0;
}

