/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 5:01 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //LAB WORKSHEET 2 PROBLEM #1
    
    cout<< "Welcome to the MLB Slugging Percentage Calculator" << endl; 
    cout<< endl;
    
    //Variable Declaration
    int singles, doubles, triples, home_runs, at_bats; 
    double slugging_percentage;
    
    //User Input
    cout << "Number of Singles: ";
    cin >> singles;
    
    cout << "Number of Doubles: ";
    cin >> doubles; 
    
    cout << "Number of Triples: ";
    cin >> triples;
    
    cout << "Number of Home Runs: ";
    cin >> home_runs;
    
    cout << "Number of At Bats: ";
    cin >> at_bats; 
    
    // Math Calculations
    slugging_percentage =(singles+ 2 * doubles+ 3 * triples+ 4 * home_runs)
    / static_cast <double> (at_bats);
            
    
    //Output Slugging Percentage
    cout << endl;
    cout << "Slugging Percentage for this player: " << slugging_percentage
            << "%" << endl; 
        
    
    
    
    // LAB WORKSHEET 2 PROBLEM #2
    
    cout<< endl;
    
    //Variable Declaration
    int x, y, z;
    
   
    //Prompting User
    cout << "Please enter two numbers between 1 and 1000" << endl; 
    
    //User Input
    cout << "Number 1: ";
    cin >> x;
    
    cout << "Number 2: "; 
    cin >> y;
    cout << endl;
    
    //Variable Initialization and Display
    cout << "X = " << setw(5) << left << x;
    
    cout << "Y = " << y << endl;
    
    cout << setfill('-') << setw(97) << left << "-" << endl;
    
    //Switching Values of X and Y using 3rd variable "z"
    z = x; 
    x = y;
    
    //Making Dashed line and Outputting Switched values
    cout << setfill(' ');
    
    cout << "X = " << setw(5) << left << x;
    
    cout << "Y = " << z << endl;
    
    
    
    
    
    //LAB WORKSHEET 2 Problem #3 
//    int a = 4;
//    int b = 0;
//    
//    if (a == 4) 
//    {b = 4;}
//    else if (a == 9) 
//    {b = 5;}
//    else {b = 6;}
    
    int a = 10;
    int b = 5;
    
    if (a > 5)
    {
        b = 6;
    }
    else if (a < 5)
    {
        b = 1;
    }
    else 
    {
        b = 5;
    }
    
    cout << endl << "a = " << a << endl;
    cout << "b = " << b << endl; 
    
    


    return 0;
}

 
    
    