/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 19, 2014, 3:50 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>


using namespace std;

void problem1();
void problem2();
void problem3();
int* createDynamicArray (int);
void outputArray (int[], int);
void problem4();
void sortVector (vector<int>&, int);
void outputVector (const vector<int>&);
void problem5();
void addValue (int*, int, int&);
int* allocateMemory (int*, int&);
void outputArray2 (int*, int&);


/*
 * 
 */
int main(int argc, char** argv) {

    //Main Menu
    int selection;
    
    while (selection != -1)
    {
        cout << endl;
        
        cout << "Lab Week 13" << endl << endl;

        cout << "You may enter -1 to Quit" << endl << endl;

        cout << "Enter 1 for Problem #1" << endl;
        cout << "Enter 2 for Problem #2" << endl;
        cout << "Enter 3 for Problem #3" << endl;
        cout << "Enter 4 for Problem #4" << endl;
        cout << "Enter 5 for Problem #5" << endl;

        cin >> selection;

        switch (selection)
        {
            case -1:
                break;
            case 1: 
                problem1();
                break;
            case 2:
                problem2();
                break;
            case 3:
                problem3();
                break;
            case 4:
                problem4();
                break;
            case 5:
                problem5();
                break;
            default:
                cout << "Error." << endl;
                break;
        }
    }
    
    return 0;
}

void problem1()
{
    int *a = new int (22);
    int *b = new int (1337);
    
    cout << "Pointer #1 value: " << *a << endl;
    cout << "Pointer #1 address: " << a << endl;
    
    cout << endl;
    
    cout << "Pointer #2 value: " << *b << endl;
    cout << "Pointer #2 address: " << b << endl;
    
    delete a;
    delete b;
}

void problem2()
{
    int size = 5;
    int array [] = {1,2,3,4,5};
    
    int* p;
    p = array;
    
    for (int i = 0; i < size; i++)
    {
        cout << array[i] << " ";
    }
    
    cout << endl;
    
    for (int i = 0; i < size; i++)
    {
        cout << p[i] << " ";
    }
    
    cout << endl;
    
    
}

void problem3()
{
    cout << "Enter the size you wish for an array: ";
    int size;
    cin >> size;
    
    int* array = createDynamicArray (size);
    
    outputArray (array, size);
    
    srand(time(0));
    for (int i = 0; i < size; i++)
    {
        int num = (rand() % 9);
        array [i] = num;
    }
    
    outputArray (array, size);
    
    delete[] array;
    
}

int* createDynamicArray (int size)
{
    int* array = new int [size];
    
    for (int i = 0; i < size; i++)
    {
        array[i] = i;
    }
    return array;
    
}

void outputArray (int array [], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}

void problem4()
{
    vector <int> v;
    
    cout << "You may enter -1 to stop and display final vector." << endl;
    cout << "Enter a value to store into a vector: " << endl;
    int value;
    cin >> value;
    v.push_back(value);
    
    while (value != -1)
    {
        cin >> value;
        
        if (value != -1)
        {
            v.push_back(value);
            sortVector (v, value);
        }
        
        
    }
    
    outputVector (v);
    
    
}

void sortVector (vector <int>& v, int value)
{
    if (value == -1)
    {
        return;
    }
    
    for (int i = 0; i < v.size(); i++)
    {
        int loc = 0;
        int min = 999999;
        for (int j = i; j < v.size(); j++)
        {
            if (v[j] < min)
            {
                min = v[j];
                loc = j;
            }
        }
        swap (v[i], v[loc]);
    }
    
}

void outputVector (const vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
    
}

void problem5()
{
    int size = 3;
    int min = 999999;
    int* p = new int[size];
    int value;
    int loc = 0;
    
    cout << "You may enter -1 to stop and display final array." << endl;
    cout << "Enter values to put into an array: " << endl;
    
    while (value != -1)
    {
        cin >> value;
        
        addValue (p, value, loc);

        
        if (size <= loc)
        p = allocateMemory (p, size);
        
    }
    
    outputArray2 (p, size);
    
}

void addValue (int* p, int value, int& loc)
{
    
    if (value != -1)
        p[loc] = value;
    
    loc++;
}

int* allocateMemory (int* p, int& size)
{
    int tempSize = size * 2;
    int* temp = new int [tempSize];
    
    for (int i = 0; i < size; i++)
    {
        temp[i] = p[i];
    }
    
    delete[] p;
    size = tempSize;
    
    return temp;
    
}

void outputArray2 (int* p, int& size)
{
    for (int i = 0; i < size; i++)
    {
        cout << p[i] << " ";
    }
    cout << endl << endl;
}
