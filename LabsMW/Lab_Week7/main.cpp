/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 2, 2014, 4:08 PM
 */

#include <cstdlib>
#include <iostream>


using namespace std;

//Function Prototypes
double exponent (int, double);






/*
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
int main(int argc, char** argv) {

    //DO NOT DO #3, 8, or 9
    
    //Lab Week 7 
    //Problem #1
    cout << "Enter an integer or decimal: ";
    int userNum;
    cin >> userNum;
    
    cout << "Enter an exponent: ";
    double userExponent;
    cin >> userExponent;
    
    double outputNum = exponent (userNum, userExponent);
    
    cout << "The number now is: " << outputNum << endl;
    
    
    //Problem #4
    //Leap Year Determination
    cout << endl << "Please enter a year to determine if it is "
            "a leap year: ";
    int year;
    cin >> year;
    
    
    
    
    
    return 0;
}

/*
 * Exponent function will take the number entered by the user and exponentiate
 * it based on the exponent the user puts in.
 * 
 * Returns userNum1 ^ userExponent;
 * 
 * Parameters are a double as the base number and an integer as the 
 * exponent.
 */
double exponent (int num1, double power)
{
    int num2 = num1;
    for (int i = 0; i < power - 1; i++)
    {
        num2 = num2 * num1;
    }
    return num2;
}

//Problem #2
double exponent (int num1, int power)
{
    int num2 = num1;
    for (int i = 0; i < power - 1; i++)
    {
        num2 = num2 * num1;
    }
    return num2;
}