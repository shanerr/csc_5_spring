/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 21, 2014, 4:00 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <iomanip>


using namespace std;

void checkAt(string);

/*
 * 
 */
int main(int argc, char** argv) {

    //Lab Week 9 Problem #1
    
    string sentence;
    
    cout << "Please type in a sentence, type '@' to exit: " << endl;
    getline (cin, sentence);
    checkAt (sentence);
    
    
    return 0;
}

void checkAt (string sentence)
{
    ofstream outfile;
    outfile.open ("output.dat");
   
    for (int i = 0; i < sentence.size(); i++)
    {
             if (sentence [i] != '@')
             {
                 outfile << sentence [i];
             }
             else if (sentence [i] == '@')
             {
                 outfile << sentence.substr (0, sentence [i - 1]);
             }
             
     }
     outfile.close();
     
    
}

