/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 3:54 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // LAB PROBLEM #1
    
    int x = 5;
    x = x+5;
    
    cout << x << endl;

    
  
    cout << endl; 
    
    
    
    // LAB PROBLEM #2
    
    string var1 = "1";
    int var2 = 2;
    cout << var1 << "+" << var1 << "=" << var1 + var1 << endl;
    cout << var2 << "+" << var2 << "=" << var2 + var2 << endl; 
    
    
    
    
    cout << endl;
    
    
    
    // LAB PROBLEM #3
    
    // Variable Declaration
    int num1,num2,num3;
     
            
    
    // Prompt User
    cout << "Please enter 3 Test Scores" << endl;
    
    //User input
    cout << "Test Score #1: ";
    cin >> num1;
    
    cout << "Test Score #2: "; 
    cin >> num2;
    
    cout << "Test Score #3: ";
    cin >> num3; 
    
    // Math Calculations
    int total = num1 + num2 +num3;
    double testAverage = total / 3.0; 
    
    // Average Output
    cout << "Your Test average is " << testAverage << "." << endl;
    

    
    cout << endl;
    
    // LAB PROBLEM #3 Part B
    
     // Variable Declaration 
    int num4,num5,num6;
     
            
    
    // Prompt User
    cout << "Please enter 3 Test Scores in the format (_ _ _): " << endl;
    
    cin >> num4; cin >> num5; cin >> num6; 
    
    //Math Calculations 
    int total2 = num4+num5+num6;
    double testScoreAverage = total2 / 3.0; 
    
    //Average Ouput 
    cout << "Your Test average is " << testScoreAverage << "." << endl; 
    
    
    
    
    
   
    
    return 0;
}

