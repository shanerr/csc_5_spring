/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 5, 2014, 3:52 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>


using namespace std;

void problem1();
void fill (vector<int>&, int array[],int size);
void problem2();
void userFill (int userArray[], int size, int& loc);
void problem3();
void findMin (int array1[], int size);
void problem4();
void problem5();
bool foundSpace (string);

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Main Menu
    cout << "Lab Week 11" << endl << endl;
    cout << "Enter 1 for Problem #1" << endl;
    cout << "Enter 2 for Problem #2" << endl;
    cout << "Enter 3 for Problem #3" << endl;
    cout << "Enter 4 for Problem #5" << endl;
    cout << "Enter 5 for Problem #6" << endl;
    
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1:
            problem1();
            break;
        case 2:
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 4:
            problem4();
            break;
        case 5:
            problem5();
            break;
        default:
            cout << "Error." << endl;
            break;
    }

    return 0;
}

//////////////PROBLEM #1 
void problem1()
{
    vector <int> tenRandom;
    int array1 [10];
    
    fill (tenRandom, array1, 10);
    
    
}

void fill (vector<int>& tenRandom, int array1[], int size)
{
    srand(time(0));
    //Filling both vector and array with 10 random values
    for (int i = 0; i < size; i++)
    {
        int randNum = (rand() % 9);
        tenRandom.push_back(randNum);
        int randNum2 = (rand() % 49);
        array1[i] = randNum2;
    }
    
    
    //Outputting Array
    cout << "The array reads: " << endl;
    for (int i = 0; i < size; i++)
    {
        cout << array1[i] << " ";
    }
    cout << endl;
    
    //Outputting Vector
    cout << "The vector reads: " << endl;
    for (int i = 0; i < tenRandom.size(); i++)
    {
        cout << tenRandom[i] << " ";
    }
    cout << endl;
}

/////////////////PROBLEM #2
void problem2()
{
    int userArray [5];
    int loc = 0;
    
    userFill (userArray, 5, loc);
    
    //Outputting Array to Console
    cout << "Your array reads: " << endl;
    for (int i = 0; i < loc; i++)
    {
        cout << userArray[i] << " ";
    }
    
}

void userFill (int userArray[], int size, int& loc)
{
    //Getting 5 values from user to put into userArray
    cout << "You can enter -1 to Quit" << endl;
    for (int i = 0; i < size; i++)
    {
        cout << "Please enter a value to put into an array: ";
        int value;
        cin >> value;
        
        if (value == -1)
        {
            return;
        }
        else
        {
            userArray [i] = value;
        }
        loc++;
    }
    
}

///////////////PROBLEM #3
void problem3()
{
    int array1[] = {12, 13, 5, 64, 7, 2};
    
    findMin (array1, 6);
}

void findMin (int array1[], int size)
{
    //Finding Minimum value using for loop
    //Initializing currentMin as a very large value
    int currentMin = 99999999;
    for (int i = 0; i < size; i++)
    {
        if (array1[i] < currentMin)
        {
            currentMin = array1[i];
        }
    }
    
    //Outputting Array
    for (int i = 0; i < size; i++)
    {
        cout << array1[i] << " ";
    }
    cout << endl;
    
    //Outputting Minimum Value
    cout << "The minimum value in the array is: " << endl;
    cout << currentMin;
    
}

/*
 * PROBLEM #4 Pseudocode for Bubble Sort Algorithm
 * 
 * void bubbleSort (vector)
 * {
 *     //Outer for loop iterates n times (n represents number of elements)
 *     for (int i = 0; i < size of vector; i++)
 *     {  
 *         //Inner for loop compares 2 values at a time
 *         //Also swaps values if needed
 *         for (int j = 0; i < size of vector - 1; i++)
 *         { 
 *             if (value at location i > value at location i+1)
 *             {
 *                 swap (value at i, with value at i+1);
 *             }
 *         }
 *      }
 *  }
 *         
 */

////////////////PROBLEM #5
void problem4()
{
    vector <string> firstNames;
    vector <string> lastNames;
    
    ifstream infile;
    
    cout << "Please enter the file name 'names.dat' to read from it: ";
    string fileName;
    cin >> fileName;
    
    infile.open(fileName.c_str());
    
    while(infile.fail())
    {
        cout << "The file does not exist. Try again: ";
        cin >> fileName;
        infile.open (fileName.c_str());
    }
    
    string contents, contents2;
    
    while (!infile.eof())
    {
        for (int i = 0; i < fileName.size() / 2; i++)
        {
            //Reading first name
            infile >> contents;
            firstNames.push_back(contents);
            
            //Reading last name
            infile >> contents2;
            lastNames.push_back(contents2);
        }
    }
    infile.close();
    
    
    
    //Outputting contents in both vectors
    for (int i = 0; i < firstNames.size(); i++)
    {
        cout << firstNames [i] << " " << lastNames[i] << endl;
    }
    
}

void problem5()
{
    vector <string> firstNames;
    vector <string> lastNames;
    
    ifstream infile;
    
    cout << "Please enter the file name 'broken.dat' to read from it: ";
    string fileName;
    cin >> fileName;
    
    infile.open(fileName.c_str());
    
    while(infile.fail())
    {
        cout << "The file does not exist. Try again: ";
        cin >> fileName;
        infile.open (fileName.c_str());
    }
    
    string contents, contents2;
    
    while (!infile.eof())
    {       
        for (int i = 0; i < ; i++)
        {
            //Reading first name
            getline (infile, contents);
            
            if (foundSpace (contents))
            {
                if (word.find (" ") == 0)
    *********************************************
            }
            
            
        }
    }
    infile.close();
    
    
    
    //Outputting contents in both vectors
    for (int i = 0; i < firstNames.size(); i++)
    {
        cout << firstNames [i] << " " << endl << lastNames[i] << endl;
    }
    
}

bool foundSpace (contents)
{
    for (int i = 0; i < contents.size(); i++)
    {
        if (contents [i] == ' ')
            return true;
    }
    return false;
}