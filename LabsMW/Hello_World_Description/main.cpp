/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 3:22 PM
 */

#include <cstdlib> // cstdlib is unneeded
#include <iostream> // iostream is for input and output from console.

// std is short for standard
// all my libraries come from the standard C++ namespace
// also defines context of key words 
using namespace std; 

/*
 * 
 */

// All code is located within main
// There is only 1 main file per program
// Code is always executed from top to bottom, left to right
// Argc and Argv come from outside the program

int main(int argc, char** argv) {
    
    // variable definition
    // Data type is a string
    // Variable name is message
    // Assigning "Hello World" to message 
    //string message = "Hello World"; 

    // All statements must end in a semi-colon
    // Cout means console output
    // << is the stream operator 
    // "Hello World" is a string literal
    // endl makes a new line
    // cout << "Hello World" << endl;
    
    string message; // Variable declaration 
    // Don't have to initialize message because I am using "cin" to store a new value
    message = "Hello World"; // Variable initialization 
    
    // Prompt the user
    cout << "Please enter a word" << endl;
    
    // Cin gets input from console and stored into the variable
    // message 
    cin >> message; 
    
    // Output the variable "message"
     cout << "You entered " << message << endl;
     
     cout << "Shane Brown" << endl;
     
     
     
    
    
    
    // If code gets here then it executed correctly
     // Return flag
    return 0;
} // All code must be inside main curly brackets

