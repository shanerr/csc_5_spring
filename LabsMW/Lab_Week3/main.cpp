/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 10, 2014, 4:09 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Lab Week 3 Part 1
    //Change problem
    
    //Variable Declaration and Initialization
    double purchasePrice, amountTendered, changeDue;
    const int DOLLAR = 100;
    const int QUARTER = 25;
    const int DIME = 10;
    const int NICKEL = 5;
    const int PENNY = 1;
    
    
    //Prompting User
    cout << "Please enter the purchase price for an item: $";
    cin >> purchasePrice;
    
    cout << endl << "Please enter the amount tendered: $";
    cin >> amountTendered;
    
    //Calculate Change Due
    changeDue = amountTendered - purchasePrice;
    
    //Output Change Due
    cout << endl << setprecision(2) << fixed << "Change Due: $"
                << changeDue << endl;
    
    
    //Static Cast for Modulus 
    changeDue *= 100;
    int changePennies = static_cast <int> (changeDue) + .05;
    
    
    
    //Calculate Number of Dollars
    int numDollars = changePennies / DOLLAR;
    //Get remaining change
    changePennies %= DOLLAR;
    
    //Calculate Number of Quarters
    int numQuarters = changePennies / QUARTER;
    changePennies %= QUARTER;
    
    //Calculate Number of Dimes
    int numDimes = changePennies / DIME;
    changePennies %= DIME;
    
    //Calculate Number of Nickels
    int numNickels = changePennies / NICKEL;
    changePennies %= NICKEL;
    
    //Calculate Number of Pennies
    int numPennies = changePennies;
    
    
    
    if (amountTendered < purchasePrice)
    {
        cout << "Insufficient amount provided" << endl;
    }
    else if (purchasePrice == amountTendered )
    {
        cout << "Your change is $0.00" << endl;
        cout << "Thank you and please come again!";
        }
    else 
    { 
          
        cout << "Dollars: " << numDollars << endl;
        cout << "Quarters: " << numQuarters << endl;
        cout << "Dimes: " << numDimes << endl; 
        cout << "Nickels: " << numNickels << endl; 
        cout << "Pennies: " << numPennies << endl;
        cout << endl << "Thank you and please come again!";
    }
    
        
    
    
    
    
    return 0;
}

