/* 
 * File:   main.cpp
 * Author: Thomas
 *
 * Created on June 3, 2014, 12:55 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctype.h>

using namespace std;
string getInput();
char checkOperation (string);
void extractNumbers (string&, string&, string, char);
double performOperation (double, double, char);
void outputSolution (double);

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Project 2" << endl << "Calculator Application" <<
            endl << endl;
    
    cout << "Enter your calculation: " << endl;
    string input = getInput();
    
    char symbol = checkOperation (input);
    
    if (symbol == '!')
    {
        cout << "Invalid operation." << endl;
    }
    
    
    double solution = performOperation (num1, num2, symbol);
    
    outputSolution (solution);
    
    
    return 0;
}

string getInput()
{
    string userInput;
    getline(cin, userInput);
    
    return userInput;
}

char checkOperation (string input)
{
    for (int i = 0; i < input.size(); i++)
    {
        if (input[i] == '+')
            return '+';
        else if (input[i] == '-')
            return '-';
        else if (input[i] == '/')
            return '/';
        else if (input[i] == '*')
            return '*';
    }
    
    return '!';
}

void extractNumbers (string& numOne, string& numTwo, string input,
        char symbol)
{
    numOne = input.substr(0,1);
    numTwo = input.substr(symbol + 1, 1);
}

double performOperation (double num1, double num2, char symbol)
{
    
    if (symbol == '+')
    {
        double sum = num1 + num2;
        return sum;
    }
    
    else if (symbol == '-')
    {
        double difference = num1 - num2;
        return difference;
    }
    
    else if (symbol == '/')
    {
        double division = num1 / num2;
        return division;
    }
    else 
    {
        double product = num1 * num2;
        return product;
    }
    
}

void outputSolution (double solution)
{
    cout << "The answer is: " << solution << endl;
}



