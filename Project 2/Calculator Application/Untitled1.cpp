//Including All SDL Libraries
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <cstdlib>

using namespace std;

//Screen Attributes
const int SCREEN_WIDTH = 380;
const int SCREEN_HEIGHT = 450;
const int SCREEN_BPP = 32;

//Numbers for Math Operations
int num1 = 0;
int num2 = 0;

string firstNum = "";
string secondNum = "";

char operation;
double result;

//Button Sprite States
const int CLIP_MOUSEOUT = 0;
const int CLIP_MOUSEDOWN = 1;

//The font that is going to be used
TTF_Font* font = NULL;
SDL_Color textColor = {0,0,0};

//Rectangles to hold button sprites
SDL_Rect clip[2];
SDL_Rect clip2[2];
SDL_Rect clip3[2];
SDL_Rect clip4[2];
SDL_Rect clip5[2];
SDL_Rect clip6[2];
SDL_Rect clip7[2];
SDL_Rect clip8[2];
SDL_Rect clip9[2];
SDL_Rect clip0[2];
SDL_Rect clipAdd[2];
SDL_Rect clipSubtract[2];
SDL_Rect clipDivide[2];
SDL_Rect clipMultiply[2];
SDL_Rect clipClear[2];
SDL_Rect clipEquals[2];

//FUNCTION TO APPLY IMAGES TO SURFACES////////////////////////
/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

void apply_surface (int x, int y, SDL_Surface* source, SDL_Surface*
destination, SDL_Rect* clip = NULL)
{
            //Make a temporary rectangle to hold the offsets
            SDL_Rect offset;
            
            //Give the offsets to the rectangle
            offset.x = x;
            offset.y = y;
            
            //Blit the surface
            SDL_BlitSurface (source, clip, destination, &offset);
}

//Event Structure for Mouse, Keys, etc.
SDL_Event event;

//Boolean for user quit
bool quit = false;

//Surface pointers for images to be placed on
SDL_Surface* message = NULL;
SDL_Surface* screen = NULL;
SDL_Surface* calc_background = NULL;
SDL_Surface* calc_screen = NULL;
SDL_Surface* number1 = NULL;
SDL_Surface* number2 = NULL;
SDL_Surface* number3 = NULL;
SDL_Surface* number4 = NULL;
SDL_Surface* number5 = NULL;
SDL_Surface* number6 = NULL;
SDL_Surface* number7 = NULL;
SDL_Surface* number8 = NULL;
SDL_Surface* number9 = NULL;
SDL_Surface* number0 = NULL;
SDL_Surface* clear = NULL;
SDL_Surface* divide = NULL;
SDL_Surface* add = NULL;
SDL_Surface* multiply = NULL;
SDL_Surface* subtract = NULL;
SDL_Surface* equals = NULL;

//Function to clip button sprite sheets
void set_clips()
{
     clip[CLIP_MOUSEOUT].x = 0;
     clip[CLIP_MOUSEOUT].y = 0;
     clip[CLIP_MOUSEOUT].w = 60;
     clip[CLIP_MOUSEOUT].h = 60;
     
     clip[CLIP_MOUSEDOWN].x = 60;
     clip[CLIP_MOUSEDOWN].y = 0;
     clip[CLIP_MOUSEDOWN].w = 60;
     clip[CLIP_MOUSEDOWN].h = 60;
}

///CLASSES FOR BUTTONS/////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

//Number 1 Button/////////////////////
class Button1
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips;
              
      public:
             //Initialize the variables 
             Button1(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 2 Button//////////////////////
class Button2
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips2;
              
      public:
             //Initialize the variables 
             Button2(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 3 Button//////////////////////////
class Button3
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips3;
              
      public:
             //Initialize the variables 
             Button3(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 4 Button//////////////////////////
class Button4
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips4;
              
      public:
             //Initialize the variables 
             Button4(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 5 Button////////////////////////////////
class Button5
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips5;
              
      public:
             //Initialize the variables 
             Button5(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 6 Button/////////////////////////////////
class Button6
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips6;
              
      public:
             //Initialize the variables 
             Button6(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 7 Button//////////////////////////////////
class Button7
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips7;
              
      public:
             //Initialize the variables 
             Button7(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 8 Button/////////////////////////////////
class Button8
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips8;
              
      public:
             //Initialize the variables 
             Button8(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 9 Button///////////////////////////////////
class Button9
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips9;
              
      public:
             //Initialize the variables 
             Button9(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Number 0 Button///////////////////////////////
class Button0
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clips0;
              
      public:
             //Initialize the variables 
             Button0(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Addition Button//////////////////////////////////
class ButtonAdd
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clipsAdd;
              
      public:
             //Initialize the variables 
             ButtonAdd(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Subtraction Button/////////////////////////////////
class ButtonSubtract
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clipsSubtract;
              
      public:
             //Initialize the variables 
             ButtonSubtract(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Division Button////////////////////////////////////
class ButtonDivide
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clipsDivide;
              
      public:
             //Initialize the variables 
             ButtonDivide(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Multiplication Button////////////////////////////
class ButtonMultiply
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clipsMultiply;
              
      public:
             //Initialize the variables 
             ButtonMultiply(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Clear Button//////////////////////////////////////
class ButtonClear
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clipsClear;
              
      public:
             //Initialize the variables 
             ButtonClear(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//Equals Button////////////////////////////////////////
class ButtonEquals
{
      private:
              //The attributes of the button
              SDL_Rect box;
              
              //The part of the button sprite sheet that will be shown
              SDL_Rect* clipsEquals;
              
      public:
             //Initialize the variables 
             ButtonEquals(int x, int y, int w, int h);
             
             //Handles events and set the button's sprite region
             void handle_events();
             
             //Shows the button on the screen
             void show();
};

//CONSTRUCTORS FOR ALL BUTTONS///////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

Button1::Button1 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips = &clip[ CLIP_MOUSEOUT ];
}

Button2::Button2 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips2 = &clip[ CLIP_MOUSEOUT ];
}

Button3::Button3 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips3 = &clip[ CLIP_MOUSEOUT ];
}

Button4::Button4 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips4 = &clip[ CLIP_MOUSEOUT ];
}

Button5::Button5 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips5 = &clip[ CLIP_MOUSEOUT ];
}

Button6::Button6 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips6 = &clip[ CLIP_MOUSEOUT ];
}

Button7::Button7 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips7 = &clip[ CLIP_MOUSEOUT ];
}

Button8::Button8 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips8 = &clip[ CLIP_MOUSEOUT ];
}

Button9::Button9 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips9 = &clip[ CLIP_MOUSEOUT ];
}

Button0::Button0 ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clips0 = &clip[ CLIP_MOUSEOUT ];
}

ButtonAdd::ButtonAdd ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clipsAdd = &clip[ CLIP_MOUSEOUT ];
}

ButtonSubtract::ButtonSubtract ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clipsSubtract = &clip[ CLIP_MOUSEOUT ];
}

ButtonDivide::ButtonDivide ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clipsDivide = &clip[ CLIP_MOUSEOUT ];
}

ButtonMultiply::ButtonMultiply ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clipsMultiply = &clip[ CLIP_MOUSEOUT ];
}

ButtonClear::ButtonClear ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clipsClear = &clip[ CLIP_MOUSEOUT ];
}

ButtonEquals::ButtonEquals ( int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
    
    //Set the default sprite
    clipsEquals = &clip[ CLIP_MOUSEOUT ];
}

//HANDLE EVENTS FUNCTIONS FOR ALL BUTTONS/////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

void Button1::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "1";
                else
                secondNum += "1";

            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button2::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips2 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips2 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips2 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "2";
                else
                secondNum += "2";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips2 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button3::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips3 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips3 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips3 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "3";
                else
                secondNum += "3";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips3 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button4::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips4 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips4 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips4 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "4";
                else
                secondNum += "4";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips4 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button5::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips5 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips5 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips5 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "5";
                else
                secondNum += "5";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips5 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button6::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips6 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips6 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips6 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "6";
                else
                secondNum += "6";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips6 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button7::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips7 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips7 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips7 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "7";
                else
                secondNum += "7";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips7 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button8::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips8 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips8 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips8 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "8";
                else
                secondNum += "8";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips8 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button9::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips9 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips9 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips9 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "9";
                else
                secondNum += "9";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips9 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void Button0::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clips0 = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clips0 = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips0 = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (65, 40, calc_screen, screen);
                
                if (operation != '+' && operation != '-' && 
                operation != '/' && operation != '*')
                firstNum += "0";
                else
                secondNum += "0";
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clips0 = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void ButtonAdd::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clipsAdd = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clipsAdd = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsAdd = &clip[ CLIP_MOUSEDOWN ];
                
                if (operation != '-' && operation != '/' && operation != '*')
                operation = '+';
                
                std::ostringstream oss1;
                oss1 << firstNum;
                message = TTF_RenderText_Solid (font,oss1.str().c_str(), 
                textColor);
                
                apply_surface (250, 45, message , screen);
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsAdd = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void ButtonSubtract::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clipsSubtract = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clipsSubtract = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsSubtract = &clip[ CLIP_MOUSEDOWN ];
                
                if (operation != '+' && operation != '/' && operation != '*')
                operation = '-';
                
                std::ostringstream oss2;
                oss2 << firstNum;
                message = TTF_RenderText_Solid (font,oss2.str().c_str(), 
                textColor);
                
                apply_surface (250, 45, message , screen);
                
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsSubtract = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void ButtonDivide::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clipsDivide = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clipsDivide = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && 
            ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsDivide = &clip[ CLIP_MOUSEDOWN ];
                
                if (operation != '+' && operation != '-' && operation != '*')
                operation = '/';
                
                std::ostringstream oss3;
                oss3 << firstNum;
                message = TTF_RenderText_Solid (font,oss3.str().c_str(), 
                textColor);
                
                apply_surface (250, 45, message , screen);
            
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsDivide = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void ButtonMultiply::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clipsMultiply = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clipsMultiply = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsMultiply = &clip[ CLIP_MOUSEDOWN ];
                
                if (operation != '+' && operation != '-' && operation != '/')
                operation = '*';
                
                std::ostringstream oss4;
                oss4 << firstNum;
                message = TTF_RenderText_Solid (font,oss4.str().c_str(), 
                textColor);
                
                apply_surface (250, 45, message , screen);
                
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsMultiply = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void ButtonClear::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clipsClear = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clipsClear = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsClear = &clip[ CLIP_MOUSEDOWN ];
                
                apply_surface (0, 0, calc_background, screen);
                apply_surface (65, 40, calc_screen, screen);
                
                firstNum = "";
                secondNum = "";
                result = 0;
                num1 = 0;
                num2 = 0;
                operation = 'n';
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsClear = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}

void ButtonEquals::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( event.type == SDL_MOUSEMOTION )
    {
        //Get the mouse offsets
        x = event.motion.x;
        y = event.motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
            //Set the button sprite
            clipsEquals = &clip[ CLIP_MOUSEOUT ];    
        }
        //If not
        else
        {
            //Set the button sprite
            clipsEquals = &clip[ CLIP_MOUSEOUT ];
        }    
    }
    //If a mouse button was pressed
    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsEquals = &clip[ CLIP_MOUSEDOWN ];
                
                num1 = atoi (firstNum.c_str());
                num2 = atoi (secondNum.c_str());
                
                if ((operation == '+' || operation == '-' 
                || operation == '*' || operation == '/') && num1 >= 0 
                && num2 >= 0)
                {   
                    if (operation == '+')
                    result = num1 + num2;
                    else if (operation == '-')
                    result = num1 - num2;
                    else if (operation == '/')
                    result = num1 / static_cast<double>(num2);
                    else 
                    result = num1 * num2;
                
                }
                
                apply_surface (65, 40, calc_screen, screen);
                
                std::ostringstream oss;
                oss << result;
                message = TTF_RenderText_Solid (font,oss.str().c_str(), 
                textColor);
                
                
                apply_surface (250, 45, message, screen);
                
            }
        }
    }
    
    //If a mouse button was released
    if( event.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( event.button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = event.button.x;
            y = event.button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clipsEquals = &clip[ CLIP_MOUSEOUT ];
            }
        }
    }
}
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
                    
//FUNCTION TO LOAD IMAGES FROM DIRECTORY/////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
SDL_Surface *load_image( std::string filename ) 
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;
    
    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;
    
    //Load the image using SDL_image
    loadedImage = IMG_Load( filename.c_str() );
    
    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );
        
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }
    
    //Return the optimized image
    return optimizedImage;
}


//Show MEMBER FUNCTIONS FROM BUTTON CLASSES///////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

void Button1::show()
{
    //Show the button
    apply_surface( box.x, box.y, number1, screen, clips );
}

void Button2::show()
{
    //Show the button
    apply_surface( box.x, box.y, number2, screen, clips2 );
}

void Button3::show()
{
     //Show the button
    apply_surface( box.x, box.y, number3, screen, clips3 );
}

void Button4::show()
{
     //Show the button
    apply_surface( box.x, box.y, number4, screen, clips4 );
}

void Button5::show()
{
     //Show the button
    apply_surface( box.x, box.y, number5, screen, clips5 );
}

void Button6::show()
{
     //Show the button
    apply_surface( box.x, box.y, number6, screen, clips6 );
}

void Button7::show()
{
     //Show the button
    apply_surface( box.x, box.y, number7, screen, clips7 );
}

void Button8::show()
{
     //Show the button
    apply_surface( box.x, box.y, number8, screen, clips8 );
}
     
void Button9::show()
{
     //Show the button
    apply_surface( box.x, box.y, number9, screen, clips9 );
}

void Button0::show()
{
     //Show the button
    apply_surface( box.x, box.y, number0, screen, clips0 );
}

void ButtonAdd::show()
{
     //Show the button
    apply_surface( box.x, box.y, add, screen, clipsAdd );
}

void ButtonSubtract::show()
{
     //Show the button
    apply_surface( box.x, box.y, subtract, screen, clipsSubtract );
}

void ButtonDivide::show()
{
     //Show the button
    apply_surface( box.x, box.y, divide, screen, clipsDivide );
}

void ButtonMultiply::show()
{
     //Show the button
    apply_surface( box.x, box.y, multiply, screen, clipsMultiply );
}

void ButtonClear::show()
{
     //Show the button
    apply_surface( box.x, box.y, clear, screen, clipsClear);
}

void ButtonEquals::show()
{
     //Show the button
    apply_surface( box.x, box.y, equals, screen, clipsEquals );
}

     

/////MAIN ///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
int main( int argc, char* args[] )
{
    
    //Start SDL
    SDL_Init (SDL_INIT_EVERYTHING);
    
    //Initialize SDL_ttf
    TTF_Init();
                   
    
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return 1;    
    }
    
    //Set up screen
    screen = SDL_SetVideoMode (SCREEN_WIDTH, SCREEN_HEIGHT,
    SCREEN_BPP, SDL_SWSURFACE);
    
    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return 1;    
    }
    
    //Set the window caption
    SDL_WM_SetCaption( "Calculator", NULL );
    
    //Load the images from directory
    calc_background = load_image ("calc_background.png");
    calc_screen = load_image ("calc_screen.png");
    number1 = load_image ("number1.png");
    number2 = load_image ("number2.png");
    number3 = load_image ("number3.png");
    number4 = load_image ("number4.png");
    number5 = load_image ("number5.png");
    number6 = load_image ("number6.png");
    number7 = load_image ("number7.png");
    number8 = load_image ("number8.png");
    number9 = load_image ("number9.png");
    number0 = load_image ("number0.png");
    add = load_image ("add.png");
    subtract = load_image ("subtract.png");
    multiply = load_image ("multiply.png");
    divide = load_image ("divide.png");
    clear = load_image ("clear.png");
    equals = load_image ("equals.png");
    
    //Load font
    font = TTF_OpenFont ("ARLRDBD.ttf", 28);
    
    
    //Set Button sprites
    set_clips ();
    
    //Create instances of buttons using button classes 
    Button1 numOne (55, 120, 60, 60);
    Button2 numTwo (125, 120, 60, 60);
    Button3 numThree (195, 120, 60, 60);
    Button4 numFour (265, 120, 60, 60);
    Button5 numFive (55, 190, 60, 60);
    Button6 numSix (125, 190, 60, 60);
    Button7 numSeven (195, 190, 60, 60);
    Button8 numEight (265, 190, 60, 60);
    Button9 numNine (55, 260, 60, 60);
    Button0 numZero (125, 260, 60, 60);
    ButtonAdd numAdd (195, 260, 60, 60);
    ButtonSubtract numSubtract (265, 260, 60, 60);
    ButtonDivide numDivide (125, 330, 60, 60);
    ButtonMultiply numMultiply (55, 330, 60, 60);
    ButtonClear numClear (195, 330, 60, 60);
    ButtonEquals numEquals (265, 330, 60, 60);
    
    
    //Apply the images to the screen surface
    apply_surface (0, 0, calc_background, screen);
    apply_surface (65, 40, calc_screen, screen);
    apply_surface (55, 120, number1, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (125, 120, number2, screen, &clip2[CLIP_MOUSEOUT]);
    apply_surface (195, 120, number3, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (265, 120, number4, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (55, 190, number5, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (125, 190, number6, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (195, 190, number7, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (265, 190, number8, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (55, 260, number9, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (125, 260, number0, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (195, 260, add, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (265, 260, subtract, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (55, 330, multiply, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (125, 330, divide, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (195, 330, clear, screen, &clip[CLIP_MOUSEOUT]);
    apply_surface (265, 330, equals, screen, &clip[CLIP_MOUSEOUT]);
    
    
//////MAIN PROGRAM FOR CALCULATOR///////////////////////////////
////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

    //While the user hasn't quit
    while (quit == false)
    {
          //While there's an event to handle
          while (SDL_PollEvent (&event))
          {
                //Handle button events
                numOne.handle_events();
                numTwo.handle_events();
                numThree.handle_events();
                numFour.handle_events();
                numFive.handle_events();
                numSix.handle_events();
                numSeven.handle_events();
                numEight.handle_events();
                numNine.handle_events();
                numZero.handle_events();
                numAdd.handle_events();
                numSubtract.handle_events();
                numDivide.handle_events();
                numMultiply.handle_events();
                numClear.handle_events();
                numEquals.handle_events();
                
                //If the user has X'ed out the window
                if (event.type == SDL_QUIT)
                {
                    //Quit the program 
                    quit = true;
                }
          }
          numOne.show();
          numTwo.show();
          numThree.show();
          numFour.show();
          numFive.show();
          numSix.show();
          numSeven.show();
          numEight.show();
          numNine.show();
          numZero.show();
          numAdd.show();
          numSubtract.show();
          numDivide.show();
          numMultiply.show();
          numClear.show();
          numEquals.show();
          
           //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;    
        }
    }  
    
    //Free the surface
    SDL_FreeSurface (calc_background);
    SDL_FreeSurface (calc_screen);
    SDL_FreeSurface (number1);
    SDL_FreeSurface (number2);
    SDL_FreeSurface (number3);
    SDL_FreeSurface (number4);
    SDL_FreeSurface (number5);
    SDL_FreeSurface (number6);
    SDL_FreeSurface (number7);
    SDL_FreeSurface (number8);
    SDL_FreeSurface (number9);
    SDL_FreeSurface (number0);
    SDL_FreeSurface (add);
    SDL_FreeSurface (subtract);
    SDL_FreeSurface (divide);
    SDL_FreeSurface (multiply);
    SDL_FreeSurface (clear);
    SDL_FreeSurface (equals);
    SDL_FreeSurface (message);
    
    //Close font used
    TTF_CloseFont (font);
    
    //Quit SDL_ttf
    TTF_Quit();
    
    //Quit SDL
    SDL_Quit();
    
    return 0;    
}
