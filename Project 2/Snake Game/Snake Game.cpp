//Include SDL Functions and Databases
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

int mainMenu();
void createButtons();
int snakeGame();
void resetScores (int&, int&);
int statsPage();
void quitApp();
bool checkCollisionWalls (SDL_Rect A, SDL_Rect B, SDL_Rect C, SDL_Rect D,
SDL_Rect E);
bool checkCollisionFood (SDL_Rect head, SDL_Rect food);

//Declaring SDL Rectangles for collision detection
SDL_Rect leftWall;
SDL_Rect rightWall; 
SDL_Rect topWall;
SDL_Rect bottomWall;
SDL_Rect leftFood;
SDL_Rect rightFood;
SDL_Rect topFood;
SDL_Rect bottomFood;
SDL_Rect head;
SDL_Rect food;

//Color of the text used 
SDL_Color textColor = {255, 0, 0};

//Declaring TTF font 
TTF_Font* font = NULL;

//Make sure program waits for a quit
bool quit = false;

//Bools for Game
bool gameOver = false;
bool collisionWalls = false;
bool collisionFood = false;

//The frame rate
const int FRAMES_PER_SECOND = 10;

const int HEAD_HEIGHT = 20;
const int FOOD_HEIGHT = 20;
const int FOOD_WIDTH  = 20;

//Bool to check if play button has been selected
bool snakeGameRunning;

//Attributes of the Screen
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;

//The button states in the sprite sheet
const int CLIP_MOUSEOVER = 2;
const int CLIP_MOUSEOUT = 0;
const int CLIP_MOUSEDOWN = 2;
const int CLIP_MOUSEUP = 0;

//The portions of the play sprite maps to be blitted
SDL_Rect play_clip [3];
SDL_Rect stats_clip [3];
SDL_Rect exit_clip [3];


//The images
SDL_Surface* background = NULL;
SDL_Surface* screen = NULL;
SDL_Surface* exitGame = NULL;
SDL_Surface* play = NULL;
SDL_Surface* stats = NULL;
SDL_Surface* button = NULL;
SDL_Surface* gameBackground = NULL;
SDL_Surface* statsBackground = NULL;
SDL_Surface* left_wall = NULL;
SDL_Surface* right_wall = NULL;
SDL_Surface* top_wall = NULL;
SDL_Surface* bottom_wall = NULL;
SDL_Surface* bomb = NULL;
SDL_Surface* foodIMG = NULL;
SDL_Surface* snake_head = NULL;
SDL_Surface* game_over = NULL;
SDL_Surface* score = NULL;
SDL_Surface* high_score = NULL;
SDL_Surface* most_eaten = NULL;
SDL_Surface* game_scoreOutput = NULL;

//The event structure that will be used
SDL_Event event;

///CLASSES FOR BUTTONS////////////////////////////////////////
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

   ////////PLAY BUTTON CLASS//////////
    class playButton 
    {
          private:
                  //Attributes of the Button
                  SDL_Rect box;
          
                  //Part of the button sprite sheet that will be shown
                  SDL_Rect* clip;
          
          public:
                 //Initialize the variables
                 playButton (int x, int y, int w, int h);
          
                 //Handles events and sets the button's sprite region
                 void handle_events();
          
                 //Shows the button on the screen
                 void show();
                 
                 //Function to remove button from screen
                 void remove();
                 
                 void reset();
    };
    
    ///////STATS BUTTON CLASS///////
    class statsButton
    {
          private:
                  //Atrributes of the Button
                  SDL_Rect box;
                  
                  //Part of the button sprite sheet that will be shown
                  SDL_Rect* clip2;
                  
          public:
                 //Initialize the variables 
                 statsButton (int x, int y, int w, int h);
                 
                 //Handles events and sets the button's sprite region
                 void handle_events();
                 
                 //Shows the button on the screen
                 void show();
                 
                 //Function to remove button from screen
                 void remove();
                 
                 void reset();
    };
    
    ///////EXIT BUTTON CLASS///////
    class exitButton
    {
          private:
                  //Atrributes of the Button
                  SDL_Rect box;
                  
                  //Part of the button sprite sheet that will be shown
                  SDL_Rect* clip3;
                  
          public:
                 //Initialize the variables 
                 exitButton (int x, int y, int w, int h);
                 
                 //Handles events and sets the button's sprite region
                 void handle_events();
                 
                 //Shows the button on the screen
                 void show();
                 
                 //Function to remove button from screen
                 void remove();
                 
                 void reset();
    };
    
    //The dot that will move around on the screen
class SnakeHead
{
    private:
    //The X and Y offsets of the snake head
    int x, y;
    
    //Dimensions of the snake head
    int w, h;
    
    //The velocity of the snake head
    int xVel, yVel;
    
    public:
    //Initializes the variables
    SnakeHead();
    
    SDL_Rect head;
    
    //Takes key presses and adjusts the head velocity
    void handle_input();
    
    //Moves the head
    void move();
    
    //Shows the head on the screen
    void show();

};

class Food
{
      private:
      //Coordinates of food
      int x, y;
      
      int w, h;
      
      public:
      //Default constructor
      Food();
      
      //Rectangle to hold food attributes
      SDL_Rect food;
      
      //Shows the food on the screen
      void show();
      
      void handle_collision();
      
      void newPosition();
      
};


    //Creating instances of the buttons
    playButton playButton1 (260, 210, 100, 50);
    statsButton statsButton1 (240, 285, 140, 50);
    exitButton exitButton1 (260, 350, 100, 50);
    
    //Creating instance of snake head
    SnakeHead SnakeHead1;
    
    //Creating instance of food
    Food food1;
    
void createButtons()
{
    //Creating instances of the buttons
    playButton playButton1 (260, 210, 100, 50);
    statsButton statsButton1 (240, 285, 140, 50);
    exitButton exitButton1 (260, 350, 100, 50);
}    
    
    
void playButton::remove()
{
     SDL_FreeSurface (play);
     
     box.x = 0;
     box.y = 0;
     box.w = 0;
     box.h = 0;
     
     SDL_Flip (screen);
}

void statsButton::remove()
{
     SDL_FreeSurface (stats);
     
     box.x = 0;
     box.y = 0;
     box.w = 0;
     box.h = 0;
     
     SDL_Flip (screen);
}

void exitButton::remove()
{
     SDL_FreeSurface (exitGame);
     
     box.x = 0;
     box.y = 0;
     box.w = 0;
     box.h = 0;
     
     SDL_Flip (screen);
}

//RESET FUNCTIONS FOR BUTTONS///////////////////////////////////
///////////////////////////////////////////////////////////////
void playButton::reset()
{
     box.x = 260;
     box.y = 210;
     box.w = 100;
     box.h = 50;
}

void statsButton::reset()
{
     box.x = 240;
     box.y = 285;
     box.w = 140;
     box.h = 50;
}

void exitButton::reset()
{
     box.x = 260;
     box.y = 350;
     box.w = 100;
     box.h = 50;
}
     
///CONSTRUCTORS FOR BUTTON CLASSES//////////////////////////////
///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// 
    
    //PLAY BUTTON Constructor
    playButton::playButton (int x, int y, int w, int h)
    {
        //Set the button's attributes
        box.x = x;
        box.y = y;
        box.w = w;
        box.h = h;
        
        //Default sprite (MOUSE_OUT)
        clip = &play_clip[CLIP_MOUSEOUT];
    }
    
    //STATS BUTTON Constructor
    statsButton::statsButton (int x, int y, int w, int h)
    {
        //Set the button's attributes
        box.x = x;
        box.y = y;
        box.w = w;
        box.h = h;
        
        //Default sprite (MOUSE_OUT)
        clip2 = &stats_clip[CLIP_MOUSEOUT];
    }
    
        //EXIT BUTTON Constructor
    exitButton::exitButton (int x, int y, int w, int h)
    {
        //Set the button's attributes
        box.x = x;
        box.y = y;
        box.w = w;
        box.h = h;
        
        //Default sprite (MOUSE_OUT)
        clip3 = &exit_clip[CLIP_MOUSEOUT];
    }
    
    SnakeHead::SnakeHead ()
    {
        //Initialize the offsets 
        x = 300;
        y = 225;
        
        //Setting dimensions
        w = HEAD_HEIGHT;
        h = HEAD_HEIGHT;
        
        //Initialize the velocity 
        xVel = 0;
        yVel = 0;
    }
    
    Food::Food()
    {
        //Initialize the offsets
        x = 570;
        y = 225;
        
        //Dimensions
        w = FOOD_WIDTH;
        h = FOOD_HEIGHT;
    }

//Function to load images//////////////////////////////
SDL_Surface *load_image( std::string filename ) 
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;
    
    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;
    
    //Load the image using SDL_image
    loadedImage = IMG_Load( filename.c_str() );
    
    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );
        
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }
    
    //Return the optimized image
    return optimizedImage;
}
//Function to apply images to screen////////////////////
void apply_surface (int x, int y, SDL_Surface* source, SDL_Surface* destination
, SDL_Rect* clip = NULL)
{
     //Make a temporary rectangle to hold the offsets 
     SDL_Rect offset;
     
     //Give the offsets to the rectangle
     offset.x = x;
     offset.y = y;
     
     //Blit the surface
     SDL_BlitSurface (source, clip, destination, &offset);
}
/////FUNCTIONS TO HANDLE EVENTS WITH BUTTONS/////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

//Function to handle events with PLAY BUTTON///////////////////
void playButton::handle_events()
    {
         //The mouse offsets
         int x = 0, y =0;
         
         //If the mouse moved
         if (event.type == SDL_MOUSEMOTION)
         {
             //Get the mouse offsets
             x = event.motion.x;
             y = event.motion.y;
             
             //If the mouse is over the button
             if ((x > box.x) && (x < box.x + box.w) && (y > box.y) &&
             (y < box.y + box.h))
             {
                  //Set the button sprite
                  clip = &play_clip[CLIP_MOUSEOVER];
             }
             
             //If not 
             else
             {
                 //Set the button sprite
                 clip = &play_clip[CLIP_MOUSEOUT];
             }
         }
         
         //If a mouse button was pressed
         if (event.type == SDL_MOUSEBUTTONDOWN)
         {
             //If the left mouse button was pressed
             if (event.button.button == SDL_BUTTON_LEFT)
             {
                 //Get the mouse offsets
                 x = event.button.x;
                 y = event.button.y;
                 
                 //If the mouse is over the button
                 if ((x > box.x) && (x < box.x + box.w) && (y > box.y) 
                 && (y < box.y + box.h))
                 {
                       //Set the button sprite
                       clip = &play_clip [CLIP_MOUSEDOWN];
                       
                       snakeGameRunning = true;
                       
                       playButton1.remove();
                       statsButton1.remove();
                       exitButton1.remove();
                       
                       
                       snakeGame();
                       
                       
                 }
             }
         }
         
         //If a mouse button was released
         if (event.type == SDL_MOUSEBUTTONUP)
         {
             //If the left mouse button was released
             if (event.button.button == SDL_BUTTON_LEFT)
             {
                 //Get the mouse offsets
                 x = event.button.x;
                 y = event.button.y;
                 
                 //If the mouse is over the button
                  if (( x > box.x ) && ( x < box.x + box.w ) && 
                  ( y > box.y ) && ( y < box.y + box.h ))
                  {
                      //Set the button sprite
                      clip = &play_clip [CLIP_MOUSEOUT];
                  }
             }
         }
    }
    
///Function to handle events with STATS BUTTON////////////////
void statsButton::handle_events()
    {
         //The mouse offsets
         int x = 0, y =0;
         
         //If the mouse moved
         if (event.type == SDL_MOUSEMOTION)
         {
             //Get the mouse offsets
             x = event.motion.x;
             y = event.motion.y;
             
             //If the mouse is over the button
             if ((x > box.x) && (x < box.x + box.w) && (y > box.y) &&
             (y < box.y + box.h))
             {
                  //Set the button sprite
                  clip2 = &stats_clip[CLIP_MOUSEOVER];
             }
             
             //If not 
             else
             {
                 //Set the button sprite
                 clip2 = &stats_clip[CLIP_MOUSEOUT];
             }
         }
         
         //If a mouse button was pressed
         if (event.type == SDL_MOUSEBUTTONDOWN)
         {
             //If the left mouse button was pressed
             if (event.button.button == SDL_BUTTON_LEFT)
             {
                 //Get the mouse offsets
                 x = event.button.x;
                 y = event.button.y;
                 
                 //If the mouse is over the button
                 if ((x > box.x) && (x < box.x + box.w) && (y > box.y) 
                 && (y < box.y + box.h))
                 {
                       //Set the button sprite
                       clip2 = &stats_clip [CLIP_MOUSEDOWN];
                       
                       playButton1.remove();
                       statsButton1.remove();
                       exitButton1.remove();
                       
                       statsPage();
                 }
             }
         }
         
         //If a mouse button was released
         if (event.type == SDL_MOUSEBUTTONUP)
         {
             //If the left mouse button was released
             if (event.button.button == SDL_BUTTON_LEFT)
             {
                 //Get the mouse offsets
                 x = event.button.x;
                 y = event.button.y;
                 
                 //If the mouse is over the button
                  if (( x > box.x ) && ( x < box.x + box.w ) && 
                  ( y > box.y ) && ( y < box.y + box.h ))
                  {
                      //Set the button sprite
                      clip2 = &stats_clip [CLIP_MOUSEOUT];
                  }
             }
         }
    }
    
//Funtion to handle events with EXIT BUTTON///////////////////
void exitButton::handle_events()
    {
         //The mouse offsets
         int x = 0, y =0;
         
         //If the mouse moved
         if (event.type == SDL_MOUSEMOTION)
         {
             //Get the mouse offsets
             x = event.motion.x;
             y = event.motion.y;
             
             //If the mouse is over the button
             if ((x > box.x) && (x < box.x + box.w) && (y > box.y) &&
             (y < box.y + box.h))
             {
                  //Set the button sprite
                  clip3 = &exit_clip[CLIP_MOUSEOVER];
             }
             
             //If not 
             else
             {
                 //Set the button sprite
                 clip3 = &exit_clip[CLIP_MOUSEOUT];
             }
         }
         
         //If a mouse button was pressed
         if (event.type == SDL_MOUSEBUTTONDOWN)
         {
             //If the left mouse button was pressed
             if (event.button.button == SDL_BUTTON_LEFT)
             {
                 //Get the mouse offsets
                 x = event.button.x;
                 y = event.button.y;
                 
                 //If the mouse is over the button
                 if ((x > box.x) && (x < box.x + box.w) && (y > box.y) 
                 && (y < box.y + box.h))
                 {
                       //Set the button sprite
                       clip3 = &exit_clip [CLIP_MOUSEDOWN];
                       quitApp();
                       
                 }
                 
             }
         }
         
         //If a mouse button was released
         if (event.type == SDL_MOUSEBUTTONUP)
         {
             //If the left mouse button was released
             if (event.button.button == SDL_BUTTON_LEFT)
             {
                 //Get the mouse offsets
                 x = event.button.x;
                 y = event.button.y;
                 
                 //If the mouse is over the button
                  if (( x > box.x ) && ( x < box.x + box.w ) && 
                  ( y > box.y ) && ( y < box.y + box.h ))
                  {
                      //Set the button sprite
                      clip3 = &exit_clip [CLIP_MOUSEOUT];
                  }
             }
         }
    }
    
    void SnakeHead::handle_input()
    {
         //If a key was pressed
         if (event.type == SDL_KEYDOWN)
         {
              //Adjust the velocity
              switch (event.key.keysym.sym)
              {
                     case SDLK_UP:
                          xVel = 0;
                          y += - HEAD_HEIGHT / 2; 
                          yVel = -HEAD_HEIGHT / 2;
                          break;
                     case SDLK_DOWN:
                          xVel = 0;
                          y += HEAD_HEIGHT / 2;
                          yVel = HEAD_HEIGHT / 2;
                          break;
                     case SDLK_LEFT:
                          yVel = 0;
                          x += - HEAD_HEIGHT / 2;
                          xVel = -HEAD_HEIGHT / 2;
                          break;
                     case SDLK_RIGHT:
                          yVel = 0;
                          x += HEAD_HEIGHT / 2;
                          xVel = HEAD_HEIGHT / 2;
                          break;
              }
         }
             
    }   
                     
    
////FUNCTIONS TO DISPLAY BUTTONS//////////////////////////////
/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
//Show function from playButton class
void playButton::show()
{
    //Show the PLAY button
    apply_surface( box.x, box.y, play, screen, clip);
}

//Show function from statsButton class
void statsButton::show()
{
     //Show the STATS Button
     apply_surface (box.x, box.y, stats, screen, clip2);
}

//Show function from exitButton class
void exitButton::show()
{
     //Show the EXIT Button
     apply_surface (box.x, box.y, exitGame, screen, clip3);
}

//Show function for the Snake Head
void SnakeHead::show()
{    
     //Show the head
     apply_surface (x, y, snake_head, screen);
}

//Function to show food on screen
void Food::show()
{
     //Show the food 
     apply_surface (x, y, foodIMG, screen);
}
////FUNCTIONS TO CLIP SPRITE SHEETS FOR BUTTONS/////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
void set_play_clips()
{
    //Clip the sprite sheet
    play_clip[ CLIP_MOUSEOVER ].x = 100;
    play_clip[ CLIP_MOUSEOVER ].y = 0;
    play_clip[ CLIP_MOUSEOVER ].w = 100;
    play_clip[ CLIP_MOUSEOVER ].h = 50;

    play_clip[ CLIP_MOUSEOUT ].x = 0;
    play_clip[ CLIP_MOUSEOUT ].y = 0;
    play_clip[ CLIP_MOUSEOUT ].w = 100;
    play_clip[ CLIP_MOUSEOUT ].h = 50;

    play_clip[ CLIP_MOUSEDOWN ].x = 0;
    play_clip[ CLIP_MOUSEDOWN ].y = 50;
    play_clip[ CLIP_MOUSEDOWN ].w = 100;
    play_clip[ CLIP_MOUSEDOWN ].h = 50;
        
}

void set_stats_clips()
{
    //Clip the sprite sheet
    stats_clip[ CLIP_MOUSEOVER ].x = 140;
    stats_clip[ CLIP_MOUSEOVER ].y = 0;
    stats_clip[ CLIP_MOUSEOVER ].w = 140;
    stats_clip[ CLIP_MOUSEOVER ].h = 50;

    stats_clip[ CLIP_MOUSEOUT ].x = 0;
    stats_clip[ CLIP_MOUSEOUT ].y = 0;
    stats_clip[ CLIP_MOUSEOUT ].w = 140;
    stats_clip[ CLIP_MOUSEOUT ].h = 50;

    stats_clip[ CLIP_MOUSEDOWN ].x = 0;
    stats_clip[ CLIP_MOUSEDOWN ].y = 50;
    stats_clip[ CLIP_MOUSEDOWN ].w = 140;
    stats_clip[ CLIP_MOUSEDOWN ].h = 50;
        
}

void set_exit_clips()
{
    //Clip the sprite sheet
    exit_clip[ CLIP_MOUSEOVER ].x = 100;
    exit_clip[ CLIP_MOUSEOVER ].y = 0;
    exit_clip[ CLIP_MOUSEOVER ].w = 100;
    exit_clip[ CLIP_MOUSEOVER ].h = 50;

    exit_clip[ CLIP_MOUSEOUT ].x = 0;
    exit_clip[ CLIP_MOUSEOUT ].y = 0;
    exit_clip[ CLIP_MOUSEOUT ].w = 100;
    exit_clip[ CLIP_MOUSEOUT ].h = 50;

    exit_clip[ CLIP_MOUSEDOWN ].x = 0;
    exit_clip[ CLIP_MOUSEDOWN ].y = 50;
    exit_clip[ CLIP_MOUSEDOWN ].w = 100;
    exit_clip[ CLIP_MOUSEDOWN ].h = 50;
        
}

void quitApp()
{
     SDL_Delay (500);
     SDL_Quit();
}

void SnakeHead::move()
{
     //Move the head left or right
     x += xVel;

     //Move the head up or down
     y += yVel;
     
     //Using rect for collision detection
     head.x = x;
     head.y = y;
     
     if (checkCollisionWalls (leftWall, rightWall, topWall, bottomWall,
     head))
     {
          collisionWalls = true;
     }
}

void Food::handle_collision()
{
    collisionFood = checkCollisionFood (head, food);
}

void Food::newPosition()
{
     food.x = (rand() % ((600 - 160) + 160) + 160);
     food.y = (rand() % ((440 - 20) + 20) + 20);
     
     SDL_Flip (screen);   
}


//////MAIN MENU FOR SNAKE GAME//////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
int main( int argc, char* args[] )
{
    mainMenu();   
}

int mainMenu()
{
    collisionWalls = false;
    gameOver = false;
    snakeGameRunning = false;
    createButtons();
    
    playButton1.reset();
    statsButton1.reset();
    exitButton1.reset();
    
    //Make sure program waits for a quit
    bool quit = false;
    
    //Start SDL
    if (SDL_Init (SDL_INIT_EVERYTHING) == -1)
    {
        return 1;
    }
    
    //Set up screen
    screen = SDL_SetVideoMode (SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);
    
    //If there was an error setting up the screen
    if (screen == NULL)
    {
        return 1;
    }
    
    //Set the window caption 
    SDL_WM_SetCaption ("Snake Game", NULL);
    
    //Loading Images
    background = load_image ("background.bmp");
    exitGame = load_image ("exit.png");
    play = load_image ("play.png");
    stats = load_image ("stats.png");
    
    //Clipping Sprite Sheets 
    set_play_clips();
    set_stats_clips();
    set_exit_clips();
    
    
    //Apply the background to the screen
    apply_surface (0,0, background, screen);
    
    //Apply exit button to screen
    apply_surface (260, 350, exitGame, screen, & exit_clip[CLIP_MOUSEOUT]);
    
    //Apply stats button to screen
    apply_surface (240, 285, stats, screen, & stats_clip[CLIP_MOUSEOUT]);
    
    //Apply play button to screen
    apply_surface (260, 210, play, screen, & play_clip[CLIP_MOUSEOUT]);
    
    if (SDL_Flip (screen) == -1)
    {
        return 1;
    }
    
    //////MAIN LOOP FOR EVERYTHING///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    
    //While the user hasn't quit
    while (quit == false)
    {
          //While there's an event to handle
          while (SDL_PollEvent (&event))
          {
                //Handle button events
                playButton1.handle_events();
                statsButton1.handle_events();
                exitButton1.handle_events();
                
                //If the user had X'ed out the window
                if (event.type == SDL_QUIT)
                {
                      //Quit the program
                      quit = true;
                }
               
               if (snakeGameRunning == false)
                {
                     //Show the button
                     playButton1.show();
                     statsButton1.show();
                     exitButton1.show();
                }
                
                
          
          //Update the Screen
          if (SDL_Flip (screen) == -1)
          {
              return 1;
          }
                
          }
          
    }
    
    //Free the loaded image
    SDL_FreeSurface (background);
    SDL_FreeSurface (exitGame);
    SDL_FreeSurface (stats);
    SDL_FreeSurface (play);
    
    //Quit SDL
    SDL_Quit();
    
    return 0; 
}

//The timer
class Timer
{
    private:
    //The clock time when the timer started
    int startTicks;
    
    //The ticks stored when the timer was paused
    int pausedTicks;
    
    //The timer status
    bool paused;
    bool started;
    
    public:
    //Initializes variables
    Timer();
    
    //The various clock actions
    void start();
    void stop();
    void pause();
    void unpause();
    
    //Gets the timer's time
    int get_ticks();
    
    //Checks the status of the timer
    bool is_started();
    bool is_paused();    
};

Timer::Timer()
{
    //Initialize the variables
    startTicks = 0;
    pausedTicks = 0;
    paused = false;
    started = false;    
}

void Timer::start()
{
    //Start the timer
    started = true;
    
    //Unpause the timer
    paused = false;
    
    //Get the current clock time
    startTicks = SDL_GetTicks();    
}

void Timer::stop()
{
    //Stop the timer
    started = false;
    
    //Unpause the timer
    paused = false;    
}

int Timer::get_ticks()
{
    //If the timer is running
    if( started == true )
    {
        //If the timer is paused
        if( paused == true )
        {
            //Return the number of ticks when the timer was paused
            return pausedTicks;
        }
        else
        {
            //Return the current time minus the start time
            return SDL_GetTicks() - startTicks;
        }    
    }
    
    //If the timer isn't running
    return 0;    
}

void Timer::pause()
{
    //If the timer is running and isn't already paused
    if( ( started == true ) && ( paused == false ) )
    {
        //Pause the timer
        paused = true;
    
        //Calculate the paused ticks
        pausedTicks = SDL_GetTicks() - startTicks;
    }
}

void Timer::unpause()
{
    //If the timer is paused
    if( paused == true )
    {
        //Unpause the timer
        paused = false;
    
        //Reset the starting ticks
        startTicks = SDL_GetTicks() - pausedTicks;
        
        //Reset the paused ticks
        pausedTicks = 0;
    }
}

bool Timer::is_started()
{
    return started;    
}

bool Timer::is_paused()
{
    return paused;    
}

void updateGameBG()
{
     apply_surface (0,0, gameBackground, screen);
     apply_surface (140, 5, left_wall, screen);
     apply_surface (610, 15, right_wall, screen);
     apply_surface (160, 5, top_wall, screen);
     apply_surface (140, 455, bottom_wall, screen);
}

bool checkCollisionWalls (SDL_Rect A, SDL_Rect B, SDL_Rect C, SDL_Rect D,
SDL_Rect head)
{
     //The sides of the walls and the snake head
     int leftWall, leftHead;
     int rightWall, rightHead;
     int topWall, topHead;
     int bottomWall, bottomHead;
     
     //Calculate the sides of wall
     leftWall = A.x + A.w;
     rightWall = B.x;
     topWall = C.y + C.h;
     bottomWall = D.y;
     
     //Calculate the sides of the head
     leftHead = head.x;
     rightHead = head.x + head.w;
     topHead = head.y;
     bottomHead = head.y + head.h;
     
     //If any of the sides are outside eachother
     if (bottomWall <= bottomHead)
     {
          return true;
     }
     
     if (topWall >= topHead)
     {
          return true;
     }
     
     if (rightWall <= rightHead)
     {
          return true;
     }
     
     if (leftWall >= leftHead)
     {
          return true;
     }
     
     //If no collision
     return false;
}

bool checkCollisionFood (SDL_Rect head,SDL_Rect food)
{
     //The sides of the walls and the snake head
     int leftFood, leftHead;
     int rightFood, rightHead;
     int topFood, topHead;
     int bottomFood, bottomHead;
     
     //Calculate the sides of wall
     leftFood = food.x;
     rightFood = food.x + food.w;
     topFood = food.y;
     bottomFood = food.y + food.h;
     
     //Calculate the sides of the head
     leftHead = head.x;
     rightHead = head.x + head.w;
     topHead = head.y;
     bottomHead = head.y + head.h;
     
     //If any of the sides are outside eachother
     if (bottomFood == topHead)
     {
          return true;
     }
     
     if (topFood == bottomHead)
     {
          return true;
     }
     
     if (rightFood == leftHead)
     {
          return true;
     }
     
     if (leftFood == rightHead)
     {
          return true;
     }
     
     //If no collision
     return false;
}

//This function outputs if the current game is over
void outputGameOver ()
{
     font = TTF_OpenFont ("ARLRDBD.ttf", 72);
     
     game_over = TTF_RenderText_Solid (font,"Game Over", 
     textColor);
     
     apply_surface (175, 175, game_over, screen);
     
     SDL_Flip (screen);
}

void clean_up()
{
     SDL_FreeSurface (gameBackground);
     SDL_FreeSurface (left_wall);
     SDL_FreeSurface (top_wall);
     SDL_FreeSurface (right_wall);
     SDL_FreeSurface (bottom_wall);
     SDL_FreeSurface (bomb);
     SDL_FreeSurface (foodIMG);
     SDL_FreeSurface (game_over);
     SDL_FreeSurface (screen);
     
     SDL_Flip (screen);
     
     TTF_CloseFont (font);
     
     TTF_Quit();
}
     
//Score attributes for game
int game_score = 0;
int food_eaten = 0;
int highScore;

void resetScores(int& game_score, int& food_eaten)
{
     game_score = 0;
     food_eaten = 0;
}

void outputScore (int game_score)
{
     font = TTF_OpenFont ("ARLRDBD.ttf", 50);
     
     std::ostringstream oss;
     
     oss << game_score;
     
     game_scoreOutput = TTF_RenderText_Solid (font,oss.str().c_str(), 
     textColor);
     
     apply_surface (40, 50, game_scoreOutput, screen);
     
     SDL_Flip (screen);
     
     SDL_FreeSurface (game_scoreOutput);
}

int snakeGame()
{  
     resetScores(game_score, food_eaten);
    
     //Vector to hold snake elements
     //vector <SnakeHead> snakeBody;
     
     collisionFood = false;
     collisionWalls = false;
     gameOver = false;
     
     //Starting TTF
     TTF_Init();
     
     //Dimensions for each of the walls of the game
     leftWall.x = 130;
     leftWall.y = 5;
     leftWall.w = 20;
     leftWall.h = 460;
        
     rightWall.x = 600;
     rightWall.y = 15;
     rightWall.w = 20;
     rightWall.h = 470;
        
     topWall.x = 160;
     topWall.y = 0;
     topWall.w = 470;
     topWall.h = 20;
        
     bottomWall.x = 140;
     bottomWall.y = 440;
     bottomWall.w = 470;
     bottomWall.h = 20;
     
     
     //FPS TIMER for game
     Timer fps;
     
     gameBackground = load_image ("game_background.png");
     left_wall = load_image ("leftWall_rightWall.png");
     top_wall = load_image ("bottomWall_topWall.png");
     right_wall = load_image ("leftWall_rightWall.png");
     bottom_wall = load_image ("bottomWall_topWall.png");
     bomb = load_image ("bomb.png");
     foodIMG = load_image ("food.png");
     snake_head = load_image ("snake_head.png");
     
     
     apply_surface (0,0, gameBackground, screen);
     apply_surface (140, 5, left_wall, screen);
     apply_surface (610, 15, right_wall, screen);
     apply_surface (160, 5, top_wall, screen);
     apply_surface (140, 455, bottom_wall, screen);
     
     //While the user hasn't quit
    while( quit == false )
    {
        //Start the frame timer
        fps.start();
        
        //While there's events to handle
        while( SDL_PollEvent( &event ) )
        {
            //Handle events for the snake head
            SnakeHead1.handle_input();
            
            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                quit = true;
                break;
            }
        }
        
        //Move the snake head
        SnakeHead1.move();
        
        food1.handle_collision();
        
        if (collisionFood)
        {
            game_score += 5;
            food_eaten++;
            food1.newPosition();
        }
        
        
        if (collisionWalls)
        {
             outputGameOver();
             
             SDL_Delay (500);
             clean_up();
             mainMenu();
             break;
        }
        
        //Update background after each key press
        updateGameBG();
        
        //Show the head on the screen
        SnakeHead1.show();
        
        //Show food on the screen
        food1.show();
        
        //Output score
        outputScore (game_score);
        
        //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;    
        }
        
        //Cap the frame rate
        if( fps.get_ticks() < 1000 / FRAMES_PER_SECOND )
        {
            SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
        }
    }
     
     SDL_FreeSurface (gameBackground);
     SDL_FreeSurface (left_wall);
     SDL_FreeSurface (top_wall);
     SDL_FreeSurface (right_wall);
     SDL_FreeSurface (bottom_wall);
     SDL_FreeSurface (bomb);
     SDL_FreeSurface (foodIMG);
     SDL_FreeSurface (game_over);
     SDL_FreeSurface (screen);
     
     
     TTF_CloseFont (font);
     
     TTF_Quit();

}



int statsPage()
{
     int highScore, mostFoodEaten;
     
     statsBackground = load_image ("stats_background.png");
     
     apply_surface (0, 0, statsBackground, screen);
     
     
     ifstream infile;
     
     infile.open ("stats.dat");
     
     //Reading into file and outputting highScore
     infile >> highScore;
     
     font = TTF_OpenFont ("ARLRDBD.ttf", 50);
     
     std::ostringstream oss2;
     
     oss2 << highScore;
     
     high_score = TTF_RenderText_Solid (font,oss2.str().c_str(), 
     textColor);
     
     apply_surface (200, 70, high_score, screen);
     
     SDL_Flip (screen);
     
     SDL_FreeSurface (high_score);
     
     //Reading into file and outputting mostFoodEaten
     infile >> mostFoodEaten;
     
     std::ostringstream oss3;
     
     font = TTF_OpenFont ("ARLRDBD.ttf", 50);
     
     oss3 << mostFoodEaten;
     
     most_eaten = TTF_RenderText_Solid (font,oss3.str().c_str(), 
     textColor);
     
     apply_surface (250, 130, most_eaten, screen);
     
     SDL_Flip (screen);
     
     SDL_FreeSurface (most_eaten);
     
     
     
     infile.close();
     
     
     //While the user hasn't quit
    while( quit == false )
    {   
        //While there's events to handle
        while( SDL_PollEvent( &event ) )
        {
            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                quit = true;
                break;
            }
        }
        
        //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;    
        }
    }
         

}







