Snake Game README
-----------------

Shane Brown
Project 2 

- Snake Game using SDL graphics



What works:

-Program compiles
-Buttons on main menu work and show different sprites with mouse over, mouse click, etc.
-Buttons lead to right pages on first iteration (after you navigate away and come back they do not work)
-Collision detection for snake head works with walls of the game
-Movement of the snake head works using ARROW keys
-Closing the window for the game works
-Game over is displayed if snake head hits walls

What DOES NOT Work:

-Collision detection for pellets, bombs does not work
-Reading / Writing stats to file does not work
-Counter for score does not work (since collision detection does not)
-Game does not reset after you go back to main menu and click play again
-Increasing/Decreasing snake size does not work since collision detection for food does not work




This game was exceptionally difficult and I struggled to get past some of the bugs I was running into. Although I did not finish the game,
it was a great learning experience for me and was a good way to get some initial experience using SDL.


****************************************************************************************************
EXECUTABLE for the game is inside of the directory for the game as it needs all of the .dlls to run.