Calculator Application
----------------------


Shane Brown
Project 2


What works:

-All buttons work and use correct numbers
-Recognizing operations works 
-Converting string to integers works using atoi();
-Performing math operations works correctly
-Displaying first number, second number, and final result to calculator screen works
-Clear button resets all variables to 0 
-Button sprites show when buttons clicked


What DOES NOT Work:

-Using negative numbers
-Doing multiple operations at once 
-Doing decimal numbers 



This application was also a very good tool for getting experience using SDL.



*********************************************************************************************************************
EXECUTABLE for the application is inside of the directory for the game as it needs all of the .dlls and files to run.