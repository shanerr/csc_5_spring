/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 12, 2014, 3:08 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctype.h>


using namespace std;

//Prototype for getWord
string getWord ()
{
    cout << "Please enter a word:" << endl;
    string s;
    cin >> s;
    return s;
}

//Prototype 
string clean (const string& s, const string &punct);
void cleanCase (string& );

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Declare string variable and punctuation variable
    string s = getWord();
    
    
    //Need to escape sequence the double quotation mark
    //Escape with backslash \"
    string punct ("1234567890,.()[];:'/\"<>");
    
    string cleanWord = clean (s, punct);
    
    cout << "Clean string: " << cleanWord << endl;
    
    //Clean the upper case
    cleanCase (cleanWord);
    
    cout << "Lower case: " << cleanWord << endl;
    

    return 0;
}

/*
 * The clean function cleans the string s by the punctuation
 * determined by string punct.
 * 
 * Returns a clean string with no punctuation or numbers.
 * 
 * Uses two const reference strings: 
 * 1) Original word
 * 2) Invalid characters
 */
string clean (const string& s, const string& punct)
{
    //Declare new/clean string
    
    string cleanString = "";
    
    //Use for loop to iterate through all characters
    for (int i = 0; i < s.size(); i++)
    {
        //If the character is not part of the invalid character list 
        //Largest integer -1
        if (punct.find(s[i]) == -1)
        {
            // Good character
            cleanString += s[i];
        }
    }
    return cleanString;
}

//Clean case
//Makes all letters in string lowercase
void cleanCase (string& s)
{
    //Lower case all characters
    for (int i = 0; i < s.size(); i++)
    {
        s[i] = tolower(s[i]);
    }
    
}
