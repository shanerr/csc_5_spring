/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 2, 2014, 4:13 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//Global constants
const double PI = 3.14;

//Function prototype for circumference and area
//Prototypes do not need variable names in parameters
double circumference (double);
double area (double);

//Swap Functions
void swapNonReferenced (int, int);
void swapReferenced (int&, int&);
void output (int, int);

/*
 * 
 * 
 * 
 * 
 * 
 */
int main(int argc, char** argv) {

    //Prompt the user for a radius
    cout << "Please enter a radius: ";
    int radius;
    cin >> radius;
    
    cout << "The area is: " << area(radius) << endl;
    cout << "The circumference is: " << circumference(radius) << endl;
    
    //Invoke swap functions 
    cout << "Please enter two integers: " << endl;
    int num1, num2;
    cin >> num1 >> num2;
    
    cout << endl << "Using non-referenced" << endl;
    output (num1,num2);
    swapNonReferenced (num1,num2);
    output (num1,num2);
    
    cout << endl << "Using referenced" << endl;
    output (num1,num2);
    swapReferenced (num1,num2);
    output (num1,num2);
    
    
    return 0;
}

/* 
 * Three main topics to address:
 * 1. Purpose (high-level overview of function)
 * 2. Return value (what it represents)
 * 3. Parameters (significance of them)
 * 
 * 1. Circumference function calculates the circumference of a circle
 *    with a given radius. The circumference is 2 * pi * radius.
 * 
 * 2. Return value of a circumference is a double, representing the
 *    circumference of a circle.
 * 
 * 3. One parameter. A double representing the radius
 *    of the circle.
 */
double circumference (double r)
{
    return 2 * PI * r;
}

/*
 * 1. The area function returns the area of a circle with a given radius.
 * 
 * 2. Returns a double, representing the area.
 * 
 * 3. Takes one parameter as a double called radius, representing 
 *    the radius of a circle.
 */
double area (double radius)
{
    return PI * radius * radius;
}

/*
 * swapNonReferenced attempts to swap two values of type integer.
 * 
 * The function returns nothing.
 */
void swapNonReferenced (int x, int y)
{
    int temp = x;
    x = y;
    y = temp;
}

/*
 * swapReferenced attempts to swap two values of type integer.
 * 
 * The function returns nothing.
 */
void swapReferenced (int& x, int& y)
{
    int temp = x;
    x = y;
    y = temp;
}

/*
 * Output function outputs two variables of type integer.
 * 
 * Returns nothing.
 * 
 * Takes two parameters of type integer.
 */
void output (int x, int y)
{
    cout << "First: " << x << " Second: " << y << endl;
}