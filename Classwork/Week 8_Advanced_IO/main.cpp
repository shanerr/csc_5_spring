/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 21, 2014, 4:07 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Use the get function
    //Can use in two ways
    //c = cin.get();
    // or cin.get(c);
    //Get only gets one character
    
    cout << "Enter a character: " << endl;
    char c;
    cin.get(c);
    
    cout << "Your character was: " << c << endl;
    
    //Use getline to get a sentence from the user
    cout << "Please enter a sentence: " << endl;
    
    string sentence;
    
    //Getline needs a buffer and string as arguments
    //Getline reads up until the newline character and it then
    //ignores it
    //Void function
    //Common error to mix getline and stream insertion >>
    //Error with \n (newline) character
    getline (cin, sentence);
    
    cout << "Your sentence is: " << sentence << endl;
    
    //Use the stream insertion operator
    //>> Reads until whitespace or a newline, and then
    //ignores the character
    string word;
    cout << "Please enter a word: " << endl;
    
    cin >> word;
    cout << "Your word was: " << word << endl;
    
    
    //Break my cin buffer
    while (true)
    {
        int number;
        cout << "Enter a number: " << endl;
        cin >> number;
        
        cout << "Your number was: " << number << endl;
        
        //Same exact check to see if buffer failed
        //Same as File I/O to see if file failed opening
        if (cin.fail())
        {
            //First need to clear buffer
            //Then need to ignore
                cin.clear(); // resets buffer
                cin.ignore('\n',256); //clears buffer
        }
        
    }
    

    return 0;
}

