/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 21, 2014, 3:32 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//Assuming array is rectangular
int** createDynamicArray (int row, int col)
{
    //Create dynamic array of integer pointers
    int** p = new int* [row];
    
    //Create an integer array for each integer pointer
    for (int i = 0; i < row; i++)
    {
        //p[i] is a pointer
        p[i] = new int [col];
    }
    
    return p;
}

void outputDynamicArray (int** p, int row, int col)
{
    //Use a nested for loop to access each value
    //Rows
    for (int i = 0; i < row; i++)
    {
        //Columns
        for (int j = 0; j < col; j++)
        {
            cout << p[i][j] << " ";
        }
        cout << endl;
    }
}

//Store Values Function
void storeValues (int** p, int row, int col)
{
    //Rows
    for (int i = 0; i < row; i++)
    {
        //Columns
        //Store incrementing values
        for (int j = 0; j < col; j++)
        {
            p[i][j] = i * col + j + 1;
        }
    }
}

/*
 * 
 */
int main(int argc, char** argv) {

    int row = 5;
    int col = 3;
    
    //Testing and invoking the create function
    int** p = createDynamicArray (row, col);
    outputDynamicArray (p, row, col);
    
    storeValues (p, row, col);
    outputDynamicArray (p, row, col);
    
    return 0;
}

