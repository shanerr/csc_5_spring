/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 17, 2014, 4:21 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Seed a new random number
    srand (time (0));
    
    //Random Number
    int num = (rand() % 10) + 1;
    
    //Count outside the loop
    int count = 0;
    
    
    //Sentinel-Controlled Loop
    while (num != 1000)
    {
        num = (rand() % 1000) + 1;
        cout << "Number: " << num << endl;
        
        //Increment 
        count++;
    }
    
    cout << "Number of Loops: " << count << endl;

    return 0;
}

