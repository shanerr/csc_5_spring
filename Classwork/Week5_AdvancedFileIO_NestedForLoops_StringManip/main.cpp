/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 19, 2014, 3:33 PM
 */

#include <cstdlib>
#include <fstream> //File IO
#include <iostream> //Console IO
#include <ctype.h> // tolower

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Create an ifstream object/ declare variable
    ifstream infile;
    
    //3 Step Process
    //Step 1: Open file
    infile.open ("data.dat");
    
    //Check if file exists 
    if (infile.fail())
    {
        //Get user input until good file
        do
        {
            cout << "The file failed. Enter a new one"
                 << endl;
            string fileName;
            cin >> fileName;
            
            //Open the file
            infile.open (fileName.c_str());
        }
        while (!infile);
    }
    
    //File exists, read contents from file
    //Count the number of words inside the file
    int counter = 0;
    while (!infile.eof())
    {
        counter++;
        string word;
        infile >> word;
        cout << "The word at " << counter << " is: " << word << endl;
    }
    
    //Nested for loops stair structure
    cout << "Enter a number: " << endl;
    int num;
    cin >> num;
    
    for (int i = 0; i < num; i++)
    {
        for (int j = 0; j < num - i; j++)
        {
            cout << "*";
        }
        cout << endl;
    }
    
    //String Access with for loop
    cout << "Enter a string: " << endl;
    string word;
    cin >> word;
    
    //Change all characters to lowercase
    for (int i = 0; i < word.size(); i++)
    {
        //Subscript Operator
        word [i] = tolower (word [i]);
    }
    cout << "Lower Case word is: " << word;
    
    char test = word [3];
    cout << "Test: " << test;
    
    return 0;
}

