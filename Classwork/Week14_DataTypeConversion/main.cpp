/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 28, 2014, 2:37 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctype.h> // Allows the use of the isdigit function

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Loop infinitely
    while (true)
    {
        string num;
        cout << "Please enter a number: " << endl;
        cin >> num;
        
        //Check if first character is a digit
        if (isdigit (num[0]))
        {
            //Need to convert num to a c-string
            //Convert to integer
            int val = atoi(num.c_str());
            cout <<"Your number is: " << val << endl;
            
            //Need to convert num to a c-string
            //Convert to double
            //double val = atof(num.c_str());
        }
        else 
        {
            cout << "Not a number." << endl;
        }
    }

    return 0;
}

