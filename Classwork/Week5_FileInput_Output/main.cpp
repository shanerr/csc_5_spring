/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 17, 2014, 3:27 PM
 */

#include <cstdlib>
#include <iostream> //Console I/O
#include <iomanip>
#include <fstream> //File I/O

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //First file i/o
    
    //Create an ifstream object 
    // a.k.a. Declare a variable of type 'ifstream'
    ifstream infile;
    
    //3-Step Process
    //STEP 1: Open file
    infile.open ("data.dat");
//    
//    //STEP 2: Read from file
//    string word;
//    infile >> word;
//    
//    cout << "The word is: " << word << endl;
//    
//    //STEP 3: Close the file
//    infile.close ();
    
    //File i/o with While Loop
    string word;
    
    //While loop keeps looping until no more input 
    while ( infile >> word)
    {
        cout << "The word is: " << word << endl;
    }
    
    
    //File Output
    ofstream outfile;
    
    //STEP 1: Open file
    outfile.open ("test.txt");
    
    //STEP 2: Write to file
    outfile << "Shane Brown" << endl;
    
    outfile << "Hello Shane" << endl;
    
    //STEP 3: Close file
    outfile.close ();
    
    
    
    

    return 0;
}

