/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 3:24 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    string name = "Shane Brown";
    
    //Should expect ane B
    cout <<name.substr (2,5) << endl; 
    
    //Should expect ane Bro
    //Gets a substring starting at location 2, length 7
    //0-based indexing
    cout <<name.substr (2,7) << endl;
    
    //Using find member function
    //Should return location 4
    cout << name.find ("e") << endl; 
    
    //String not located in name
    cout << name.find ("wuhebfswnfibibe") << endl;
    
    int loc = name.find ("wuhebfswnfibibe");
    
    //Increment largest location
    loc++;
    cout << "loc: " << loc << endl; 
    
    //Output the length and size of the name
    cout << "Length: " << name.length() << endl;
    
    
    //-----------------------------------------------------------------
    //IF Statements
    int x = 10;
    
    cout << "X Before IF: " << x << endl;
    if (x > 5)
    { x += 5; } // Add 5 to x only if x is greater than 5
    
    cout << "X After IF: " << x << endl;
   
    //IF-Else Statement
    int y = 5;
    
    cout << endl << "Y Before IF: " << y << endl;
    
    if (y < 5) { y += 5; } //Add 5 to y
        else {y -= 5; } //Subtract 5 from y
    
    cout << "Y After IF: " << y << endl; 
    
    
    
    
    
    
    

    return 0;
}

