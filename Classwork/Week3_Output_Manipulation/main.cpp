/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 3:29 PM
 */

#include <cstdlib>
#include <iostream> //ouput i/o
#include <iomanip> // 


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Create a constant variable so I can not change it
    const double num = 12.3294;
    
    // Illegal code
    // num = 12; 
    
    // Default is right-justified
    cout << setw(10) << num << num << endl;
    
    //main.cpp:29:22: error: expected ‘;’ before ‘left’
    //cout << setw(10) left << num << num << endl;
    
    //Fix
    cout << setw(10) << left << num << num << endl; 
    
    // 2 Digits 
    cout << setprecision(2);
    cout << setw(10) << left << num << num << endl; 
    
    // 3 Decimal Places
    // fixed sets 'setprecision' to decimal places 
    cout << setprecision(3) << fixed; 
    cout << setw(10) << left << num << num << endl; 
    
    // Set fill
    cout << setfill('='); 
    cout << setw(10) << num << num << endl;
    
    // Using two 'setw' 
    cout << setw(10) << num << setw(10) << right << num << endl;
    
    // Change set fill
    cout << setfill(' ');
    cout << setw(10) << num << setw(10) << right << num << endl;
    
    
    
    
    

    return 0;
}

