/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 10, 2014, 3:38 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Prompt the user for an integer number
    //Always prompt the user 
    cout << "Enter an integer: " << endl;
    
    //Binary Decision
    int num;
    cin >> num;
//    
//    //Determine if number is positive or negative
//    if (num > 0)
//    {
//        cout << "Your number is positive" << endl;
//    }
//    else //Ultimatum. Always will be executed if 
//        //no other condition is satisfied
//    {
//        cout << "Your number is negative" << endl;
//    }
        
        //If-else block/code
    if (num > 0)
    {
        cout << "Your number is positive" << endl;
    }
    else if (num < 0)
    {
        cout << "Your number is negative" << endl;
    }
    else
    {
        cout << "You entered the number 0" << endl;
    }
    
    
    //Switch Cases (Used for Menus)
    
    //User Prompt
    cout << "Enter a number between 1-3" << endl;
    
    //Induce infinite loop
    //Get continuous input
    while (true)
    {
    
    // Assume user is providing an integer
    int switchNum;
    cin >> switchNum;
    
    switch (switchNum)
    {
        case 1:
            cout << "You entered 1" << endl;
            break;
            
        case 2:
            cout << "You entered 2" << endl;
            break;
            //Next case is all in one line
        case 3:     
            cout << "You entered 3" << endl; break;
        
        //Default case (like else in if statement)    
        default:
            cout << "An invalid number was entered" << endl; break;
            
    }
   
    }
        
        
        //While Loop
    int x = 0; //initialization
    
    //While loop condition
    while (x < 10000)
    {
        cout << x << endl; // Code which is just output
        // x--; // Decrementor (error)
        x++; 
    }
        
        
        
        
        
    return 0;
}

