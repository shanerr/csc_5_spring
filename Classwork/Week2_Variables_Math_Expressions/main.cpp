/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 26, 2014, 3:28 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Declare two integer variables
    int num1, num2; 
    
    // Junk values are output to the screen
    cout << num1 << " " << num2 << endl; 
    
    // Initialize my variables
    num1 = num2 = 0; 
    
    //Should expect 0 0
    cout << num1 << " " << num2 << endl; 
    
    // Want to get two values from the user
    // Prompt the user when we want input
    cout << "Enter two integers:" << endl;
    
    // User input
    cin >> num1 >> num2; 
    
    cout << num1 << " " << num2 << endl; 
    
    // Calculate the average of two numbers
    // Average is the sum or total values of the numbers 
    // Divided by the quantity of numbers
    
    // Calculate total
    int total = num1 + num2; 
    
    // Calculate average with integer division
    // Calculate average with double division
    // Calculate average with static casting
    double averageIntDivision = total / 2;
    double averageDoubleDivision = total / 2.0;
    double averageStaticCast = 
                static_cast<double>(total) / 2; 
    
    cout << "Average with Integer Division: " 
            << averageIntDivision << endl;
    cout << "Average with Double Division: " 
            << averageDoubleDivision << endl;
    cout << "Average with Static Cast: " 
            << averageStaticCast<< endl;
    

    return 0;
}

