/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 14, 2014, 2:59 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

//Make a function to find the smallest number
int findSmallest (const vector<int>& v)
{
    int loc = -1;
    
    //Always check for empty vector
    if (v.size() == 0) return -1;
    
    for (int i = 0; i < v.size() - 1; i++)
    {
        //Less than or equal to for duplicate numbers
        if (v[i+1] <= v[i])
        {
            return i+1; // Smallest number in the vector
        }
    }
    
    //First value is already the smallest number
    return 0;
}

/*
 * getMedian finds the median of a circular vector
 * 
 * A circular vector is sorted but the first value is not always the
 * smallest. The sort can be mid-way through the vector.
 * 
 * Return location of the median.
 */
int getMedian (const vector<int>& v)
{
    //Check empty vector
    if (v.size() == 0) return -1;
    
    int start = findSmallest (v);
    
    cout << "Sort starts at: " << start << endl;
    cout << "Size of vector is: " << v.size() << endl;
    
    //Add the offset(start location) to the median
    int median = v.size() / 2 + start;
    
    //Account or check if current median is too large
    if (median > v.size() - 1)
    {
        median -= v.size();
    }
    
    return median;
    
}



/*
 * 
 */
int main(int argc, char** argv) {
    
    //Test already written code
    vector<int> v;
    
    v.push_back(4);
    v.push_back(5);
    v.push_back(6);
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    
    cout << "The median is at location: " << getMedian(v) << endl;

    return 0;
}

