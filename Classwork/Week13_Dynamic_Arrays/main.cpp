/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 19, 2014, 4:27 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * createDynamicArray function returns the location of a dynamic
 * array of integers.
 * 
 * Parameter is a single int, representing the size
 * 
 * Returns the pointer to the dynamic array
 */

int* createDynamicArray (int size)
{
    //Dynamic Array
    int* p = new int[size];
    
    for (int i = 0; i < size; i++)
    {
        //Implicit Dereference
        p[i] = i;
        
        //Explicit Dereference
        //*(p+i) = i;
    }
    
    return p;
}

void output (int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    
    cout << endl;
}

//Need to delete values that used by new keyword
//Deleting an array of values
void deleteArray (int a[])
{
    delete[] a; // Deallocating memory used by the array
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    int* p = createDynamicArray(10);
    
    output (p,10);
    
    deleteArray (p);

    return 0;
}

