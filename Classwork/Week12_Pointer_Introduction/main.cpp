/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 14, 2014, 3:43 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Declare a pointer
    int *p;
    cout << "Pointer value: " << p << endl;
    
    int v = 42;
    p = &v; // P is the address of v
    
    cout << "Value of v: " << v << endl;
    cout << "Dereference p: " << *p << endl;
    
    *p = 20; // Dereference p 
    cout << "Value of v: " << v << endl;
    cout << "Dereference p: " << *p << endl;
    
    //delete p; Can not delete a static location
    p = new int (42);
    cout << "Location of p: " << p << endl;
    cout << "Value at pointer: " << *p << endl;
    
    delete p;
    
    //Dangling Pointer Error
    cout << "Location of p: " << p << endl;
    cout << "Value at pointer: " << *p << endl;
    
    //Reassign pointer to null after deleting
    p = NULL; // all caps
    cout << "Location of p: " << p << endl;
    cout << "Value at pointer: " << *p << endl;
    

    return 0;
}

