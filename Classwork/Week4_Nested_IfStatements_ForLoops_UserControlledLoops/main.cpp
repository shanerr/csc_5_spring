/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 12, 2014, 3:22 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Determine if a number is positive/negative
    // Determine if number is even/odd
    
    //Prompt User
    cout << "Enter a number: " << endl;
    int num;
    cin >> num;
    
    //First way - Without Nested Statements
    //Checking if positive/negative first
    if ( num < 0)
    {
        cout << "Negative Number" << endl;
    }
    else 
    {
        cout << "Positive Number" << endl;
    }

    //Checking if number is even/odd
    if (num %2 == 0)
    {
        cout << "Even Number" << endl;
    }
    else
    {
        cout << "Odd Number" << endl;
    }
    
    
    //Nested if-statement
    if (num < 0)
    {
        cout << "Negative Number" << endl;
        
        if (num % 2 == 0)
        {
            cout << "Even Number" << endl;
        }
        else
        {
            cout << "Odd Number" << endl;
        }
    }
    else 
    {
        cout << "Positive Number" << endl;
        
        if (num % 2 == 0)
        {
            cout << "Even Number" << endl;
        }
        else
        {
            cout << "Odd Number" << endl;
        }
    }
    
    // For Loop
    // Iterate/Loop 100 times Output "i"
    for (int i = 0; i < 100; i++)
    {
        cout << "i: " << i << endl;
    }
    
    // User-Controlled Loop
    
    // Prompt user to enter a number, -1 to quit
    cout << "Enter a number. Enter -1 to quit: " << endl;
    int userNum;
    cin >> userNum;
    
    while (userNum != -1)
    {
        //Output number to user
        cout << "You entered: " << userNum << endl;
        
        //Prompt the user for another number
        cout << "Please enter another number. Enter -1 to quit: " <<
                endl;
        
        //Get input from user
        cin >> userNum;
        
    }
    
    
    
    return 0;
    
}

