/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 3:34 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//Function definition for the square function
//Function name is square
//One parameters, integer called num
//Returns integer value
int square (int num)
{
    return num * num;
}

//Create Problem 1 function

void problem1()
{
     //Invoke the square function
    cout << "Enter an integer: ";
    int value;
    cin >> value;
    
    //Call square function
    //Use value as argument
    int squareValue = square(value);
    
    cout << "The square of " << value << " is: " << squareValue << endl;
    
    cout << "The square of " << value << " is: " << square(value) << endl;
}


//Create End Program function

void endProgram()
{
    cout << "Program terminated." << endl;
}
/*
 * 
 */
int main(int argc, char** argv) {
    
    //Invoke the square function
    cout << "Enter an integer: ";
    int value;
    cin >> value;
    
    //Call square function
    //Use value as argument
    int squareValue = square(value);
    
    cout << "The square of " << value << " is: " << squareValue << endl;
    
    cout << "The square of " << value << " is: " << square(value) << endl;
    
    cout << "Enter a number.";
    cout << "'1' to run square, '-1' to quit: " << endl;
    int select;
    cin >> select;
    
    while (select != -1)
    {
        //Menu...Switch on Value
        switch (select)
        {
            case 1: 
                problem1();
                break;
            case -1:
                endProgram();
            default: cout << "Error" << endl;
        }
        
        cout << "Enter a number.";
        cout << "'1' to run square, '-1' to quit: " << endl;
        int select;
        cin >> select;
    }
    

    return 0;
}



