/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on August 20, 2013, 2:45 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctype.h>

using namespace std;

void append (string& , string );

/*
 * 
 */
int main(int argc, char** argv) {
    
    cout << "Enter a word: ";
    string input1;
    cin >> input1;
    
    cout << "Enter another word: ";
    string input2;
    cin >> input2;
    
    append (input1, input2);
    
    cout << "The two words appended are: " << input1 << " " << 
            input2 << endl;
    

    return 0;
}

void append (string& word1, string word2)
{
    for (int i = 0; i < word2.size(); i++)
    {
        if (word2 [i] == 'a' || word2 [i] == 'e' || word2 [i] == 'i'
                || word2 [i] == 'o' || word2 [i] == 'u' 
                || word2 [i] == 'y')
        {
            word1 += word2 [i];
        }
    }
    
}
