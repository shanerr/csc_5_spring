/* 
 * File:   main.cpp
 * Author: Thomas
 *
 * Created on April 12, 2014, 6:31 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctype.h>
#include <cstdlib>

using namespace std;

//Function Prototypes
void mainMenu(int&);
int chipCount(int&);
void casinoGame1(int&);
void slotsOutput(int&, int);
void slotSymbols (char&);

void casinoGame2(int&);
void rollDice(int&, int&);



/*
 * 
 * 
 * 
 * 
 */
int main(int argc, char** argv) {
    
    //Starting Chip value at 100
    int chips = 100;
    
    //Calling mainMenu () function
    mainMenu (chips);
    
     return 0;
}
void mainMenu (int& chips)
{
    //Menu-Based System for Project 
    cout << "Welcome to Oasis Resort & Casino!" << endl;
    cout << "Please select a casino game to play from the"
            " options below or enter '999'"
            " to quit." << endl << endl;
   
    //Calling chipCount() function that keeps track 
    //of the chip count
    chipCount(chips);
    
    cout << "You have " << chips << " chip(s)." << endl 
            << endl;
    cout << "Enter 1 for Slots." << endl;
    cout << "Enter 2 for Craps." << endl;
    int gameSelect;
    cin >> gameSelect;
    
    //Switching on the user game selection 
        switch (gameSelect)
        {
            case 1: 
                casinoGame1(chips);
                break;
            case 2:
                casinoGame2(chips);
            case 999:
                cout << "Thank you for visiting Oasis "
                        "Resort & Casino!"
                        << endl;
                cout << "Please come again soon." << endl;
                break;
                default: 
                cout << "Error.";
                break;
        }
}
/*
 * SLOTS Casino Game
 * (Referenced Parameter int)
 * This void function executes the Slots Game with reference 
 * to the chip count to keep track of it.
 */
void casinoGame1(int& chips)
{
    //Slots Game
    cout << endl << endl << endl << endl << endl << endl <<
            endl << endl << "Welcome to the Slots Game!" 
            << endl;
    cout << "Please enter your bet: ";
    int slotsBet;
    cin >> slotsBet;
    
    if (slotsBet > 0 && slotsBet <= chips)
    {
         chips -= slotsBet;
    }
    else
    {
        while (slotsBet > chips || slotsBet < 0)
        {
                cout << "Invalid Entry." << endl;
                cout << "Enter another bet: ";
                cin >> slotsBet;
        }
        
        chips -= slotsBet;
    }
        
    
    //User prompt to spin the wheels!
    cout << "Enter 'Spin' to spin: ";
    string spin;
    cin >> spin;
    
    if (spin != "Spin")
    {
        cout << "Try again: ";
        cin >> spin;
    }
    
    //Calling slotsOutput() function with chips and the bet
    //as arguments.
    slotsOutput (chips, slotsBet);
}

/*
 * chipCount Function
 * (Referenced Parameter int)
 * This function keeps track of the chip count by 
 * returning the referenced chip count when called.
 */
int chipCount (int& chips)
{
    return chips;
}

/*
 * slotsOutput Function
 * (Two Parameters, Referenced int and int)
 * This void function handles everything involved with
 * outputting the wheels to the user and calls other 
 * functions to determine chip count, winnings, etc.
 */
void slotsOutput (int& chips, int slotsBet)
{
    //Calling chipCount function
    chipCount (chips);
    
    //Declaring characters for symbols to be output
    char lineOne1, lineOne2, lineOne3, lineTwo1, lineTwo2, 
            lineTwo3, lineThree1, lineThree2, lineThree3;
    
    //Calling the slotSymbols function that takes each 
    //character and determines what symbol will be output
    srand(time (0));
    slotSymbols (lineOne1);
    slotSymbols (lineOne2);
    slotSymbols (lineOne3);
    slotSymbols (lineTwo1);
    slotSymbols (lineTwo2);
    slotSymbols (lineTwo3);
    slotSymbols (lineThree1);
    slotSymbols (lineThree2);
    slotSymbols (lineThree3);
    
    //Output Structure for Wheels 
    string dashes = "--------";
    cout << setw(8) << left << dashes << endl;
    cout << lineOne1 << " " << lineOne2 << " " << 
            lineOne3 << endl;
    cout << lineTwo1 << " " << lineTwo2 << " " <<
            lineTwo3 << endl;
    cout << lineThree1 << " " << lineThree2 << " " <<
            lineThree3 << endl;
    cout << setw(8) << left << dashes << endl;
    
    //'O' will act as the "Cherry"
    //'Z' will act as the "Wild"
    if (lineTwo1 == lineTwo2 && lineTwo1 == lineTwo3)
    {
        //Determining if all three symbols on the middle line
        //are wilds or not
        if (lineTwo1 == 'Z' && lineTwo2 == 'Z' && lineTwo3 == 'Z')
        {
            cout << "You win " << slotsBet * 4 << " chips!" << 
                    endl;
            chips += slotsBet * 4;
            chipCount (chips);
            cout << "You have " << chips << " chip(s).";
        }
        else
        {
            cout << "You win " << slotsBet * 3 << " chips!" << 
                    endl;
            chips += slotsBet * 3;
            chipCount (chips);
            cout << "You have " << chips << " chip(s).";
        }
      
    }
    //Determining if any of the symbols on the middle line
    //are cherries
    else if (lineTwo1 == 'O' || lineTwo2 == 'O' || lineTwo3 == 'O')
    {
        cout << "You win " << slotsBet * 2 << " chips!" << 
                    endl;
        chips += slotsBet * 2;
        chipCount (chips);
        cout << "You have " << chips << " chip(s).";
    }
    //Determining if any wilds are used to complete the middle
    //line with matching symbols
    else if ((lineTwo1 == lineTwo2 && lineTwo3 == 'Z') || 
            (lineTwo2 == lineTwo3 && lineTwo1 == 'Z') || 
            (lineTwo1 == lineTwo3 && lineTwo2 == 'Z' ))
    {
        cout << "You win " << slotsBet * 3 << " chips!" << 
                    endl;
        chips += slotsBet * 3;
        chipCount (chips);
        cout << "You have " << chips << " chip(s).";
    }
    else 
    {
        cout << "You lose." << endl;
        chipCount (chips);
        cout << "You have " << chips << " chip(s).";
    }
    
    //Asking user to if they would like to repeat game
    //or go back to main menu
    cout << endl << "Play again?" << endl;
    cout << "Enter 'Yes' to play again." << endl;
    cout << "Enter 'No' for Main Menu." << endl;
    string repeat;
    cin >> repeat;
    
    cout << endl << endl;
    
  do
  {
    if (repeat == "Yes" || repeat == "yes")
    {
        casinoGame1(chips);
        break;
    }
    else if (repeat == "No" || repeat == "no")
    {
        mainMenu (chips);
        break;
    }
    else
    {
        cout << "Try again: ";
        cin >> repeat;
       
    }
  }
    while (repeat != "Yes" || repeat != "yes" || repeat != "No"
         || repeat != "no");
    
}

/*
 * slotSymbols Function
 * (One referenced parameter, char)
 * This function takes in the reference characters from the 
 * slotsOutput function and gets a random number between 1 and 
 * 6 to determine what symbol will be represented for each slot.
 */
void slotSymbols (char& symbol)
{
    //Getting random number to convert to symbol
    int symNum = ((rand() % 6) + 1);
    
    //Declaring characters 
    char x = 'X';
    char o = 'O';
    char y = 'Y';
    char h = 'H';
    char a = 'A';
    char z = 'Z';
    
    
    //If statements to determine what symbol each character 
    //will represent
    if (symNum == 1)
    {
        symbol = x;
    }
    else if (symNum == 2)
    {
        symbol = o;
    }
    else if (symNum == 3)
    {
        symbol = y;
    }
    else if (symNum == 4)
    {
        symbol = h;
    }
    else if (symNum == 5)
    {
        symbol = a;
    }
    else 
    {
        symbol = z;
    }
    
}
 
/*
 * 
 * 
 * 
 * Craps Casino Game #2
 * (One Parameter, referenced int)
 * This function executes the void function for the game of
 * Craps with reference to the number of chips the user has.
 * 
 * 
 */
void casinoGame2 (int& chips)
{
    
    srand(time(0));
    
    //User Prompt for Line Bet 
    cout << endl << endl << endl;
    cout << "Welcome to the Craps Table." << endl;
    cout << "Enter your line bet: ";
    int lineBet;
    cin >> lineBet;
    
    //Determining that the user did not bet too many or 
    //too few chips
    if (lineBet > 0 && lineBet <= chips)
    {
        chips -= lineBet;
    }
    else
    {
        while (lineBet > chips || lineBet < 0)
        {
                cout << "Not enough chips." << endl;
                cout << "Enter another bet: ";
                cin >> lineBet;
        }
        chips -= lineBet;
    }
    
    //User Prompt to Roll the Dice
    cout << "Enter 'Roll' to roll the dice: ";
    string roll;
    cin >> roll;
    
    //Declaring Variables for die in both rounds
    int diceOne, diceTwo, diceOne_round2, diceTwo_round2;
    
    if (roll == "Roll" || roll == "roll")
    {
        //Calling rollDice() function if user types in
        //correct entry
        rollDice(diceOne, diceTwo);
    }
    else
    {
        while (roll != "Roll" || roll != "roll")
        {
                cout << "Error. Try again: ";
                cin >> roll;
        }
    }
    
    //Outputting Dice numbers for Round 1 Roll
    cout << "You rolled a " << diceOne << " and a " << diceTwo
            << endl;
    
    //Determining first round results
    if (diceOne + diceTwo == 7 || diceOne + diceTwo == 11)
    {
        cout << "You win " << lineBet * 2 << " chips!" << endl;
        chips += lineBet * 2; 
        cout << "You have " << chips << " chip(s)." << endl;
        
        cout << "Play again?" << endl;
        cout << "Type 'Yes' to play again." << endl;
        cout << "Type 'No' for Main Menu." << endl;
        string repeat;
        cin >> repeat;
        
        if (repeat == "Yes" || repeat == "yes")
        {
            casinoGame2(chips);
        }
        if (repeat == "No" || repeat == "no")
        {
            mainMenu (chips);
        }
    }
    else if (diceOne + diceTwo == 2 || diceOne + diceTwo == 3 ||
             diceOne + diceTwo == 12)
    {
        cout << "Craps. You lose." << endl;
        cout << "You have " << chips << " chip(s)." << endl;
        
        cout << "Play again?" << endl;
        cout << "Type 'Yes' to play again." << endl;
        cout << "Type 'No' for Main Menu." << endl;
        string repeat;
        cin >> repeat;
        
        if (repeat == "Yes" || repeat == "yes")
        {
            casinoGame2(chips);
        }
        if (repeat == "No" || repeat == "no")
        {
            mainMenu (chips);
        }
    }
    else // ROUND 2
    {
        //Declaring Variables for Dice in Round 2 
        int diceOne2, diceTwo2;
        
        //Declaring First Roll variable
        int firstRoll;
        
        //User Prompt for Round 2, First Roll
        cout << "Round 2." << endl;
        cout << "Enter 'Roll' to roll dice: ";
        cin >> roll;
        
            if (roll == "Roll" || roll == "roll")
            {
                //Calling rollDice() function
                //to get numbers for dice
                rollDice (diceOne_round2, diceTwo_round2);
            }
            else
            {
                while (roll != "Roll" || roll != "roll")
                {
                    cout << "Error. Try again: ";
                    cin >> roll;
                }
            }
        
        //Outputting Dice numbers for the First Roll
        //of ROUND 2
        cout << "You rolled a " << diceOne_round2 << " and a " 
                << diceTwo_round2 << endl;
        
        
        //Determining Results for Round 2 based on 
        //the numbers from the first roll
        if (firstRoll == 7)
        {
            cout << "You lose." << endl;
            cout << "You have " << chips << " chip(s)." << endl;
            
            cout << "Play again?" << endl;
            cout << "Type 'Yes' to play again." << endl;
            cout << "Type 'No' for Main Menu." << endl;
            string repeat;
            cin >> repeat;
            
            if (repeat == "Yes" || repeat == "yes")
            {
                casinoGame2(chips);
            }
            if (repeat == "No" || repeat == "no")
            {
                 mainMenu (chips);
            }
        }
        else 
            //Continuing to Roll until user rolls a 
            //7 and loses, or the user rolls the same
            //number as the first roll of Round 2 and wins
        {
            do
            {
                
                   cout << "Enter 'Roll' to roll dice: ";
                   cin >> roll;
                   
                   if (roll == "Roll" || roll == "roll")
                   {
                       //Calling rollDice() function with
                       //new dice as arguments 
                        srand(time(0));
                        rollDice(diceOne2, diceTwo2);
                   }
                   else 
                   {
                        cout << "Error. Try again: ";
                        cin >> roll;
                   }
        
                   //Outputting dice numbers for all rolls 
                   //after the first round roll
                   cout << "You rolled a " << diceOne2 
                           << " and a " << diceTwo2 << endl;
                   
                   //Determining Results for Round 2 based on
                   //dice numbers for rolls after the first roll
                   //of ROUND 2
                  if (diceOne2 + diceTwo2 == 7)
                  {
                    cout << "You lose." << endl;
                    cout << "You have " << chips << 
                            " chip(s)." << endl;
                    
                    cout << "Play again?" << endl;
                    cout << "Type 'Yes' to play again." << endl;
                    cout << "Type 'No' for Main Menu." << endl;
                    string repeat;
                    cin >> repeat;
            
                      if (repeat == "Yes" || repeat == "yes")
                      {
                         casinoGame2(chips);
                      }
                      if (repeat == "No" || repeat == "no")
                      {
                         mainMenu (chips);
                      }
                   }
                   else if (diceOne2 + diceTwo2 == firstRoll)
                   {
                        cout << "You win " << lineBet * 2 << 
                                " chips!" << endl;
                        chips += lineBet * 2;
                        cout << "You have " << chips << 
                                " chip(s)." << endl;
                
                        cout << "Play again?" << endl;
                        cout << "Type 'Yes' to play again." 
                                << endl;
                        cout << "Type 'No' for Main Menu." 
                                << endl;
                        string repeat;
                        cin >> repeat;
            
                        if (repeat == "Yes" || repeat == "yes")
                        {
                                casinoGame2(chips);
                        }
                        if (repeat == "No" || repeat == "no")
                        {
                                mainMenu (chips);
                        }
                        
                 }
                  
            }
            while (diceOne2 + diceTwo2 != firstRoll);
        }
    }
}

/*
 * rollDice Function
 * (Two Referenced Parameters, ints)
 * This function gets two random numbers between 1 and 6 
 * to represent the numbers on the dice. These values are then
 * stored in the memory locations for the variables that 
 * were used as arguments and then referenced in the 
 * parameters of the function. It is a void() function
 * and does not return any value.
 */
void rollDice (int& x, int& y)
{
    x = ((rand()% 6) + 1);
    y = ((rand()% 6) + 1);
}
