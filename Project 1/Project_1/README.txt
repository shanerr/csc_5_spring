Shane Brown
CSC-5 
4-13-14
ID #: 2431194

The program is called the Oasis Resort & Casino.

As of right now, I only have (1) casino game coded up in the project and that game
is the game of Slots.

	- For the Slots casino game, everything works as it should be working with all 
	  guidelines being met.

	- If you enter something other than 1 or 999 at the Main Menu, you will get a
	  message reading "Error." and the program will break.
	