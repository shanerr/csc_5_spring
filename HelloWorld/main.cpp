/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 19, 2014, 4:01 PM
 */

#include <cstdlib>
#include <iostream> //This is the iostream library. It allows input and output.

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Hello World" << endl << endl; // Semicolon ends my statement.
    cout << "Shane Brown" <<endl;
    return 0;
}

