/* 
 * File:   main.cpp
 * Author: Thomas
 *
 * Created on March 20, 2014, 11:13 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctype.h>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

//    //Homework 4 Problem #1
//    
//    //Variable Declaration
//    int num1, num2, num3, num4, num5, num6, num7, num8, num9,
//            num10, numNeg1, numNeg2, numNeg3, numNeg4, 
//            numNeg5, numNeg6, numNeg7, numNeg8, numNeg9,
//            numNeg10, numPos1, numPos2, numPos3,
//            numPos4, numPos5, numPos6, numPos7, numPos8,
//            numPos9, numPos10, sumPosNums, sumNegNums, sumAll,
//            posAverage, negAverage, posCount, negCount;
//    
//    //Prompting User 
//    cout << "Please enter 10 numbers, positive or negative: "
//            << endl;
//    
//    //User Input
//    cin >> num1 >> num2 >> num3 >> num4 >> num5 >> num6 
//            >> num7 >> num8 >> num9 >> num10;
//    
//    //Starting Sums at Zero
//    sumPosNums = 0;
//    sumNegNums = 0; 
//    sumAll = 0;
//    posAverage = 0;
//    negAverage = 0;
//    posCount = 0;
//    negCount = 0;
//    
//    
//    //If else statements and Calculations
//    if (num1 < 0)
//    {
//        sumNegNums += num1;
//        sumAll += num1;
//        negAverage += num1;
//        negCount++;
//    }
//    else 
//    {
//        sumPosNums += num1;
//        sumAll += num1;
//        posAverage += num1;
//        posCount++;
//    }
//    
//    if (num2 < 0)
//    {
//        sumNegNums += num2;
//        sumAll += num2;
//        negAverage += num2;
//        negCount++;
//    }
//    else
//    {
//        sumPosNums += num2;
//        sumAll += num2;
//        posAverage += num2;
//        posCount++;
//    }
//    if (num3 < 0)
//    {
//        sumNegNums += num3;
//        sumAll += num3;
//        negAverage += num3;
//        negCount++;
//    }
//    else
//    {
//        sumPosNums += num3;
//        sumAll += num3;
//        posAverage += num3;
//        posCount++;
//    }
//    if (num4 < 0)
//    {
//        sumNegNums += num4;
//        sumAll += num4;
//        negAverage += num4;
//        negCount++;
//    }
//    else
//    {
//        sumPosNums += num4;
//        sumAll += num4;
//        posAverage += num4;
//        posCount++;
//    }
//    if (num5 < 0)
//    {
//        sumNegNums += num5;
//        sumAll += num5;
//        negAverage += num5;
//        negCount++;
//    }
//    else 
//    {
//        sumPosNums += num5;
//        sumAll += num5;
//        posAverage += num5;
//        posCount++;
//    }
//    if (num6 < 0)
//    {
//        sumNegNums += num6;
//        sumAll += num6;
//        negAverage += num6;
//        negCount++;
//    }
//    else 
//    {
//        sumPosNums += num6;
//        sumAll += num6;
//        posAverage += num6;
//        posCount++;
//    }
//    if (num7 < 0)
//    {
//        sumNegNums += num7;
//        sumAll += num7;
//        negAverage += num7;
//        negCount++;
//    }
//    else 
//    {
//        sumPosNums += num7;
//        sumAll += num7;
//        posAverage += num7;
//        posCount++;
//    }
//    if (num8 < 0)
//    {
//        sumNegNums += num8;
//        sumAll += num8;
//        negAverage += num8;
//        negCount++;
//    }
//    else
//    {
//        sumPosNums += num8;
//        sumAll += num8;
//        posAverage += num8;
//        posCount++;
//    }
//    if (num9 < 0)
//    {
//        sumNegNums += num9;
//        sumAll += num9;
//        negAverage += num9;
//        negCount++;
//    }
//    else
//    {
//        sumPosNums += num9;
//        sumAll += num9;
//        posAverage += num9;
//        posCount++;
//    }
//    if (num10 < 0)
//    {
//        sumNegNums += num10;
//        sumAll += num10;
//        negAverage += num10;
//        negCount++;
//    }
//    else 
//    {
//        sumPosNums += num10;
//        sumAll += num10;
//        posAverage += num10;
//        posCount++;
//    }
//    
//     int posAverageFinal = (posAverage / posCount);
//     int negAverageFinal = (negAverage / negCount);
//     int totalAverage = (sumAll / 10);
//    
//    
//    cout << "Sum of Positive Numbers: " << sumPosNums << endl;
//    cout << "Average of Positive Numbers: " << posAverageFinal
//            << endl;
//    cout << "Sum of Negative Numbers: " << sumNegNums << endl;
//    cout << "Average of Negative Numbers: " << negAverageFinal
//            << endl;
//    cout << "Sum of All Numbers: " << sumAll << endl;
//    cout << "Total Average: " << totalAverage << endl;
//    cout << endl << "Numbers used: " << num1 << "," << num2 <<
//            "," << num3 << "," << num4 << "," << num5 << ","
//            << num6 << "," << num7 << "," << num8 << "," <<
//            num9 << "," << num10 << endl;
    
    
//******READ******
//    Previous Problem must be commented out to compile
//    Homework 4 Problem #2
    
    //Variable Declaration
    int num1, num2, num3, num4, num5, num6, num7, num8, num9,
            num10, numNeg1, numNeg2, numNeg3, numNeg4, 
            numNeg5, numNeg6, numNeg7, numNeg8, numNeg9,
            numNeg10, numPos1, numPos2, numPos3,
            numPos4, numPos5, numPos6, numPos7, numPos8,
            numPos9, numPos10, sumPosNums, sumNegNums, sumAll,
            posAverage, negAverage, posCount, negCount;
    
    
    //Getting 10 Random Values
    srand (time (0));
    num1 = (rand() % 199) - 100;
    num2 = (rand() % 199) - 100;
    num3 = (rand() % 199) - 100;
    num4 = (rand() % 199) - 100;
    num5 = (rand() % 199) - 100;
    num6 = (rand() % 199) - 100;
    num7 = (rand() % 199) - 100;
    num8 = (rand() % 199) - 100;
    num9 = (rand() % 199) - 100;
    num10 = (rand() % 199) - 100;
    
    
    //Starting Sums at Zero
    sumPosNums = 0;
    sumNegNums = 0; 
    sumAll = 0;
    posAverage = 0;
    negAverage = 0;
    posCount = 0;
    negCount = 0;
    
    
    //If else statements and Calculations
    if (num1 < 0)
    {
        sumNegNums += num1;
        sumAll += num1;
        negAverage += num1;
        negCount++;
    }
    else 
    {
        sumPosNums += num1;
        sumAll += num1;
        posAverage += num1;
        posCount++;
    }
    
    if (num2 < 0)
    {
        sumNegNums += num2;
        sumAll += num2;
        negAverage += num2;
        negCount++;
    }
    else
    {
        sumPosNums += num2;
        sumAll += num2;
        posAverage += num2;
        posCount++;
    }
    if (num3 < 0)
    {
        sumNegNums += num3;
        sumAll += num3;
        negAverage += num3;
        negCount++;
    }
    else
    {
        sumPosNums += num3;
        sumAll += num3;
        posAverage += num3;
        posCount++;
    }
    if (num4 < 0)
    {
        sumNegNums += num4;
        sumAll += num4;
        negAverage += num4;
        negCount++;
    }
    else
    {
        sumPosNums += num4;
        sumAll += num4;
        posAverage += num4;
        posCount++;
    }
    if (num5 < 0)
    {
        sumNegNums += num5;
        sumAll += num5;
        negAverage += num5;
        negCount++;
    }
    else 
    {
        sumPosNums += num5;
        sumAll += num5;
        posAverage += num5;
        posCount++;
    }
    if (num6 < 0)
    {
        sumNegNums += num6;
        sumAll += num6;
        negAverage += num6;
        negCount++;
    }
    else 
    {
        sumPosNums += num6;
        sumAll += num6;
        posAverage += num6;
        posCount++;
    }
    if (num7 < 0)
    {
        sumNegNums += num7;
        sumAll += num7;
        negAverage += num7;
        negCount++;
    }
    else 
    {
        sumPosNums += num7;
        sumAll += num7;
        posAverage += num7;
        posCount++;
    }
    if (num8 < 0)
    {
        sumNegNums += num8;
        sumAll += num8;
        negAverage += num8;
        negCount++;
    }
    else
    {
        sumPosNums += num8;
        sumAll += num8;
        posAverage += num8;
        posCount++;
    }
    if (num9 < 0)
    {
        sumNegNums += num9;
        sumAll += num9;
        negAverage += num9;
        negCount++;
    }
    else
    {
        sumPosNums += num9;
        sumAll += num9;
        posAverage += num9;
        posCount++;
    }
    if (num10 < 0)
    {
        sumNegNums += num10;
        sumAll += num10;
        negAverage += num10;
        negCount++;
    }
    else 
    {
        sumPosNums += num10;
        sumAll += num10;
        posAverage += num10;
        posCount++;
    }
    
     int posAverageFinal = (posAverage / posCount);
     int negAverageFinal = (negAverage / negCount);
     int totalAverage = (sumAll / 10);
    
    
    cout << "Sum of Positive Numbers: " << sumPosNums << endl;
    cout << "Average of Positive Numbers: " << posAverageFinal
            << endl;
    cout << "Sum of Negative Numbers: " << sumNegNums << endl;
    cout << "Average of Negative Numbers: " << negAverageFinal
            << endl;
    cout << "Sum of All Numbers: " << sumAll << endl;
    cout << "Total Average: " << totalAverage << endl;
    cout << endl << "Numbers used: " << num1 << "," << num2 <<
            "," << num3 << "," << num4 << "," << num5 << ","
            << num6 << "," << num7 << "," << num8 << "," <<
            num9 << "," << num10 << endl << endl;
    
    
//    Homework 4 Problem #3
    
    const int M = 1000;
    const int D = 500;
    const int C = 100;
    const int L = 50;
    const int X = 10;
    const int V = 5;
    const int I = 1;
    int year;
    
    cout << "Enter '-999' to Quit this program." << endl;
    cout << "Please enter a 4-Digit Year: ";
    cin >> year;
    
    while (year != -999)
    {
        //Calculate Number of 1000s
        //Calculate Number of 100s
        //Calculate number of 10s and 1s
        int thousands = year / 1000;
        year %= 1000;
        int hundreds = year / 100;
        year %= 100;
        int tens = year / 10;
        year %= 10;
        
        //Switch on the thousands
        switch (thousands)
        {
            case 3: cout << "MMM"; break;
            case 2: cout << "MM"; break;
            case 1: cout << "M"; break;
            default: cout << "Error"; break;
        }
        switch (hundreds)
        {
            case 9: cout << "CM"; break;
            case 8: cout << "DCCC"; break;
            case 7: cout << "DCC"; break;
            case 6: cout << "DC"; break;
            case 5: cout << "D"; break;
            case 4: cout << "CD"; break;
            case 3: cout << "CCC"; break;
            case 2: cout << "CC"; break;
            case 1: cout << "C"; break;
        }
        switch (tens)
        {
            case 9: cout << "XC"; break;
            case 8: cout << "LXXX"; break;
            case 7: cout << "LXX"; break;
            case 6: cout << "LX"; break;
            case 5: cout << "L"; break;
            case 4: cout << "XL"; break;
            case 3: cout << "XXX"; break;
            case 2: cout << "XX"; break;
            case 1: cout << "X"; break;
        }
        switch (year)
        {
            case 9: cout << "IX"; break;
            case 8: cout << "VIII"; break;
            case 7: cout << "VII"; break;
            case 6: cout << "VI"; break;
            case 5: cout << "V"; break;
            case 4: cout << "IV"; break;
            case 3: cout << "III"; break;
            case 2: cout << "II"; break;
            case 1: cout << "I"; break;
        }
        
        cout << endl;
        
        cout << "Enter '-999' to Quit this program." << endl;
        cout << "Please enter a 4-Digit Year: ";
        cin >> year;
        cout << endl << endl;
        
    }
    
    
    //Homework 4 Problem #4
   // Harris-Benedict Equation
    
    //Varaible Declaration
    int weight, height, age, numChocBars;
    const int chocBar = 230;
    double BMR;
    char gender;
    
    //Offering option to quit
    cout << "You may enter '999' to close this program." << endl;

    //While Loop for continuous entry until user quits
    while (weight != 999)
    {    
        
        
        //User Prompt
        cout << "Please input your weight in pounds: ";
        cin >> weight;
        if (weight == 999)
        {
            cout << "Goodbye." << endl;
        }
        else
        {
        cout << "Input your height in inches: ";
        cin >> height;
        cout << "Input your age in years: ";
        cin >> age;
        cout << "Type M for Male or F for Female: ";
        cin >> gender;


        //Male BMR Equation
        if (gender = 'M')
        {
            BMR = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
            numChocBars = BMR / chocBar;
            cout << "You need to consume " << numChocBars << " chocolate"
                    " bars to maintain your weight." << endl;

        }
        //Female BMR Equation
        else if (gender = 'F')
        {
            BMR = 655 + (4.3 * weight) + (4.7 * height) - (4.7 * age);
            numChocBars = BMR / chocBar;
            cout << "You need to consume " << numChocBars << " chocolate"
                    " bars to maintain your weight." << endl;
        }
        else 
        {
            cout << "There was an incorrect entry." << endl;
        }
        }
    
    }
    
    
    
   // Homework 4 Problem #5
   // Variable Declaration
    int numExercises, scoreReceived, totalReceived, totalPossible,
            possiblePoints;
    
    //Prompting User
    cout << "How many exercises to input? ";
    cin >> numExercises;
    
    //Initializing variables at 0
    scoreReceived = 0;
    possiblePoints = 0;
    totalReceived = 0;
    totalPossible = 0;
    
    //For Loop for Exercise entries
    for (int i = 1; i < numExercises + 1; i++)
    {
        cout << "Score received for exercise " << i << ": ";
        cin >> scoreReceived;
        cout << "Total points possible for exercise " << i << ": ";
        cin >> possiblePoints;
        
        totalReceived += scoreReceived;
        totalPossible += possiblePoints;
    }
    
    //Total Grade Calculation
    double totalGrade = (totalReceived / 
    static_cast <double> (totalPossible)) * 100;
   
    //Outputting Grade 
    cout << "Your total grade is " << totalReceived << " out of "
            << totalPossible << ", or " << 
            setprecision(2) << fixed << totalGrade << "%." << endl
            << endl;
    
    
    
    //Homework 4 Problem #6 - 7
    int gameChoice;
    cout << "Type '1' for Game with two players." << endl;
    cout << "Type '2' for Game with two computer bots." << 
            endl << endl;
    cin >> gameChoice;
    
    switch (gameChoice)
    {
        case 1:
            
    char player1, player2;
    
    do
    {
        cout << endl;
        cout << "Enter 'Z'  for both users to Quit program." << endl;
        cout << endl << "Welcome to the Rock, Paper, Scissors Game"
            << endl;
        cout << "When it is your turn, enter R for 'Rock', P for "
                "'Paper', and S for 'Scissors'." << endl;
        cout << "Player 1 enter your play: ";
        cin >> player1;
        cout << "Player 2 enter your play: ";
        cin >> player2;
    
    
    switch (player1)
    {
        case 'R':
        case 'r':
            switch (player2)
            {
                case 'R':
                case 'r':
                    cout << "Nobody Wins." << endl;
                    break;
                case 'P':
                case 'p':
                    cout << "Paper covers rock." << endl;
                    cout << "Player 2 wins!" << endl; break;
                case 'S':
                case 's':
                    cout << "Rock breaks scissors." << endl;
                    cout << "Player 1 wins!" << endl; break;
                    
                default: cout <<"Invalid entry." << endl; break;
            }
            break;
            
                case 'S':
                case 's':
                switch (player2)
                {
                    case 'R':
                    case 'r':
                        cout << "rock breaks scissors." << endl;
                        cout << "Player 2 wins!" << endl; break;
                    case 'P':
                    case 'p':
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 1 wins!" << endl; break;
                    case 'S':
                    case 's':
                        cout << "Nobody wins." << endl;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
                        
                case 'P':
                case 'p':
                switch (player2)
                {
                    case 'R':
                    case 'r':
                        cout << "Paper covers rock." << endl;
                        cout << "Player 1 wins." << endl; break;
                    case 'P':
                    case 'p':
                        cout << "Nobody wins." << endl;
                        break;
                    case 'S':
                    case 's':
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 2 wins!" << endl; break;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
         }
    
    }
    while (player1 != 'Z');
    
    break;
    
       
        
    
    //Homework 4 Problem #8
    
    case 2 :
        
    int rock = 1;
    int paper = 2;
    int scissors = 3;
    string proceedOrNot;
    int player3, player4;
    
    do
    {
        cout << endl;
        
        cout << endl << "Welcome to the Rock, Paper, Scissors Game"
            << endl;
        cout << "Enter 'No' to Quit program." << endl;
        cout << "Two computer bots will choose between Rock, "
                "Paper, and Scissors." << endl;
        cout << "Continue? ";
        cin >> proceedOrNot;
        if (proceedOrNot == "Yes")
        {
        
        //Choosing two random values between 1-3 
        //To get Rock, Paper, or Scissors for each player
        srand (time (0));
        player3 = (rand() % 3) + 1;
        player4 = (rand() % 3) + 1;
        
        string player1Choice;
        string player2Choice;
        
        //Deciding What Player 1 Chose
        if (player3 == 1)
        {
            player1Choice = "Rock";
        }
        else if (player3 == 2)
        {
            player1Choice = "Paper";
        }
        else
        {
            player1Choice = "Scissors";
        }
        cout << "Player 1 chose: ";
        cout << player1Choice << endl;
        
         //Deciding What Player 2 Chose 
        
        if (player4 == 1)
        {
            player2Choice = "Rock";
        }
        else if (player4 == 2)
        {
            player2Choice = "Paper";
        }
        else
        {
            player2Choice = "Scissors";
        }
        
        cout << "Player 2 chose: ";
        cout << player2Choice << endl;
        
    
    
    switch (player3)
    {
        case 1: 
            switch (player4)
            {
                case 1:
                    cout << "Nobody Wins." << endl;
                    break;
                case 2:
                    cout << "Paper covers rock." << endl;
                    cout << "Player 2 wins!" << endl; break;
                case 3:
                    cout << "Rock breaks scissors." << endl;
                    cout << "Player 1 wins!" << endl; break;
                    
                default: cout <<"Invalid entry." << endl; break;
            }
            break;
            
        case 2:
                switch (player4)
                {
                    case 1:
                        cout << "Paper covers rock." << endl;
                        cout << "Player 1 wins!" << endl; break;
                    case 2:
                        cout << "Nobody wins." << endl; break;
                    case 3:
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 2 wins!" << endl; break;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
                        
        case 3:
                switch (player4)
                {
                    case 1:
                        cout << "Rock breaks scissors." << endl;
                        cout << "Player 2 wins." << endl; break;
                    case 2:
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 1 wins!" << endl; break;
                    case 3:
                        cout << "Nobody wins." << endl; break;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
         }
    }
        else
        {
            cout << "Thank you for playing! Goodbye." << endl;
        }
   
    }
    while (proceedOrNot != "No");
    
    break;
        
    }

    
    return 0;
}

