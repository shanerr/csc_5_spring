/*
* Name: Shane Brown
* Student ID: 2431194
* Date: 3/6/2014
* HW: Homework 2
* Problem: 3
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   
    //Variable Declaration
    int quiz1A, quiz1B, quiz1C, quiz2A, quiz2B, quiz2C, quiz3A,
            quiz3B, quiz3C, quiz4A, quiz4B, quiz4C;
    string name1, name2, name3;
    double averageQuiz1, averageQuiz2, averageQuiz3, averageQuiz4;
    int totalQuizzes = 3;
    
    
    //Welcoming and Prompting User
    cout << "Welcome to the Quiz Average Calculator" << endl;
    cout << "Please enter your name and 4 quiz scores:" << endl;
    
    //User input 
    cin >> name1 >> quiz1A >> quiz2A >> quiz3A >> quiz4A;
    cout << "Enter another name and 4 more scores: " << endl;
    cin >> name2 >> quiz1B >> quiz2B >> quiz3B >> quiz4B;
    cout << "Enter one more name and the last 4 scores: " << endl;
    cin >> name3 >> quiz1C >> quiz2C >> quiz3C >> quiz4C;
    
    //Mathematic Calculations
    averageQuiz1 = (quiz1A + quiz1B + quiz1C)/ 
            static_cast <double> (totalQuizzes);
    averageQuiz2 = (quiz2A + quiz2B + quiz2C)/ 
            static_cast <double> (totalQuizzes);
    averageQuiz3 = (quiz3A + quiz3B + quiz3C)/ 
            static_cast <double> (totalQuizzes);
    averageQuiz4 = (quiz4A + quiz4B + quiz4C)/ 
            static_cast <double> (totalQuizzes);
    
    //Display
    cout << endl << setw(10) << left << "Name" << setw(9) << "Quiz 1" << 
            setw(9) << "Quiz 2" << setw(9) << "Quiz 3" <<
            setw(9) << "Quiz 4" << endl; 
    cout << setw(10) << left << "----" << setfill(' ') <<
            setw(9) << "------" << setw(9) << "------" <<
            setw(9) << "------" << setw(9) << "------" << endl; 
    
    //Row 1
    cout << setw(10) << left << name1;
    cout << setw(4) << right << quiz1A << setw(9) << quiz2A <<
            setw(9) << quiz3A << setw(9) << quiz4A << endl;
    //Row 2
    cout << setw(10) << left << name2;
    cout << setw(4) << right << quiz1B << setw(9) << quiz2B <<
            setw(9) << quiz3B << setw(9) << quiz4B << endl;
    //Row 3
    cout << setw(10) << left << name3;
    cout << setw(4) << right << quiz1C << setw(9) << quiz2C <<
            setw(9) << quiz3C << setw(9) << quiz4C << endl;
    
    //Displaying Averages 
    cout << endl << setw(10) << left << "Average";
    cout << setw(4) << right << setprecision(2) << 
            fixed << averageQuiz1 << setw(9) << 
            averageQuiz2 << setw(9) << averageQuiz3 << setw(9)
            << averageQuiz4;
    
    
    
    
    
    
    
    

    return 0;
}

