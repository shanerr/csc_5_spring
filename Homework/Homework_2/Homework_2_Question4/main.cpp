/*
* Name: Shane Brown
* Student ID: 2431194
* Date: 3-6-14
* HW: Homework 2
* Problem: 4
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Variable Declaration
    string name1, name2, food, num, adj, color, animal;
   
    
    //Prompting User
    cout << "Hello and Welcome to my Mad Lib Game" << endl << endl;
    cout << "Please play along by entering a corresponding object "
            "or thing when prompted!" << endl << endl;
    
    //User Input
    cout << "Please enter a name" << endl;
    cin >> name1;
    cout << "Another name?" << endl;
    cin >> name2;
    cout << "A food?" << endl;
    cin >> food;
    cout << "A number between 100 and 120?" << endl;
    cin >> num;
    cout << "An adjective?" << endl;
    cin >> adj;
    cout << "A color?" << endl;
    cin >> color;
    cout << "An animal?" << endl;
    cin >> animal;
    
    //Display of Mad Lib
    cout << endl << "Here is your Mad Lib!" << endl << endl;
    cout << "Dear " << name1 << "," << endl << endl;
    cout << "I am sorry that I am unable to turn in my homework "
            "at this time." << endl << "First I ate a rotten " << food << ","
            " which made me turn " << color << " and extremely ill." << endl
            << "I came down with a fever of " << num << ". Next, my "
            << adj << " pet " << animal << " must have smelled the "
            "remains of the " << food << " on my homework because "
            "he ate it." << endl << "I am currently rewriting my homework and "
            "hope you will accept " << endl << "it late." << endl << endl;
    cout << "Thank you for playing my Mad Lib Game!" << endl;
    cout << "Goodbye!"; 
    
   
    
    
    
    
    return 0;
}

