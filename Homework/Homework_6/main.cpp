/* 
 *Name: Shane Brown
 *ID Number: 2431194
 *Date: April 9, 2014
 *Homework: #6
 **I certify that this is my own code**
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

//Function Protoypes/ Declarations
void problem1();
void problem2();
void problem3();
void problem4();
void problem5();
void problem6();
void problem7();
double max (double, double);
double max (double, double, double);
double portionSize (int, double);
int swap (int&, int&);
void conversionInput (int&, int&);
void weightConversion (int, int, double&, double&);
void conversionOutput (double, double);
void conversionInput2 (int&, int&);
void lengthConversion (int, int, double&, double&);
void conversionOutput2 (double, double);
void computeQuarters (int, double&, int&, int&);
void computeDimes (int, int&, int&);
void computePennies (int, int&, int&);
/*         *
 *  *   *  *
 *    *    *
 *         *
 *         *
 */        

int main(int argc, char** argv) 
{
    
    //Homework Main Menu
    //Menu-Based Operation for All Homework Problems
    
    cout << "Enter 1 for Max Function with Two "
            "Parameters." << endl;
    cout << "Enter 2 for Max Function with Three "
            "Parameters." << endl;
    cout << "Enter 3 for Ice Cream Portion Sizes." 
            << endl;
    cout << "Enter 4 for Swap Function Implementation."
            << endl;
    cout << "Enter 5 for Weight Conversion." << endl;
    cout << "Enter 6 for Length Conversion." << endl;
    cout << "Enter 7 for Change Problem." << endl;
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1: 
            problem1();
            break;
        case 2: 
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 4:
            problem4();
            break;
        case 5:
            problem5();
            break;
        case 6:
            problem6();
            break;
        case 7:
            problem7();
            break;
        default: 
            cout << "Error." << endl;
            break;
    }

    return 0;
}

void problem1()
{
    //Max Function with Two Parameters
    /*
     * This program will get two double values from the user 
     * and it will then invoke the overloaded max() function
     * with TWO parameters and return the largest of the entered
     * values.
     */
    cout << "Enter either two decimal values: ";
    double num1, num2;
    cin >> num1;
    cin >> num2;
    
    
    //Function Invocation
    double maxValue = max (num1, num2);
    
    // IF max() function returns true)
    if (maxValue == true)
    {
        cout << "The numbers entered are equal." << endl;
    }
    
    else 
    {
        cout << maxValue << " is the largest value." << endl;
    }
}
void problem2()
{
    //Max Function with Three Parameters 
    /*
     * This program will get two double values from the user 
     * and it will then invoke the overloaded max() function
     * with THREE parameters and return the largest of the entered
     * values.
     */
    
    cout << "Enter either three decimal values: ";
    double num1, num2, num3;
    cin >> num1;
    cin >> num2;
    cin >> num3;
    
    //Function Invocation
    double maxValue = max (num1, num2, num3);
    
    //IF max() function returns true)
    if (maxValue == true)
    {
        cout << "The numbers entered are equal." << endl;
    }
    
    else 
    {
        cout << maxValue << " is the largest value." << endl;
    }
    
}

void problem3()
{
    //Ice Cream Portion Calculator
    /*
     * This program will get the number of customers and the
     * weight of the ice cream from the user. It will then 
     * invoke the portionSize() function to calculate how 
     * much ice cream each customer should receive.
     */
    
    cout << "Welcome to the Ice Cream Portion Size "
            "Calculator." << endl;
    cout << "Enter the number of customers: ";
    int numCustomers;
    cin >> numCustomers;
    
    cout << "Enter the weight of the ice cream: ";
    double weight;
    cin >> weight;
    
    //Function Invocation
    double customerPortion = portionSize (numCustomers, weight);
    
    //Output of portion sizes for each customer
    cout << "Each customer receives " << customerPortion <<
         " scoop(s) of ice cream." << endl;
}

void problem4()
{
    //Swap Function Implementation
    cout << "Enter two values: " << endl;
    int num1, num2;
    cin >> num1 >> num2;
    
    cout << "x: " << num1 << " y: " << num2 << endl;
    swap(num1, num2);
    cout << "x: " << num1 << " y: " << num2 << endl;
}

void problem5()
{
    //Weight Conversion Program
    int pounds;
    int ounces;
    double kilograms;
    double grams;
    
    conversionInput(pounds, ounces);
    weightConversion (pounds, ounces, kilograms, grams);
    conversionOutput (kilograms, grams);
    
}

void problem6()
{
    //Length Conversion Program
    int feet;
    int inches;
    double meters;
    double centimeters;
    
    conversionInput2 (feet, inches);
    lengthConversion (feet, inches, meters, centimeters);
    conversionOutput2 (meters, centimeters);
}

void problem7()
{
    int quarter = 25;
    int dime = 10;
    int penny = 1;
    int amountLeft;
    int numQuarters, numDimes, numPennies;
    
    cout << "Enter a change amount between $0.01 - $0.99: $";
    double change;
    cin >> change;
    
    computeQuarters (quarter, change, amountLeft, numQuarters);
    computeDimes (dime, amountLeft, numDimes);
    computePennies (penny, amountLeft, numPennies);
    
    change /= 100;
    
    cout << "$" << change << " can be given as: " << endl;
    cout << numQuarters << " quarter(s), " << numDimes <<
            " dimes(s), " << "and " << numPennies <<
            " penny(pennies)." << endl;
    
    cout << "Would you like to do another calculation? Type Yes "
            "or No: ";
    string repeat;
    cin >> repeat;
    
    if (repeat == "Yes" || repeat == "yes")
    {
        problem7();
    }
}
/*
 * Max Function #1
 * (Two Parameters, Both doubles)
 * This function takes in 2 values. With the use of IF/ELSE
 * statements, it then determines which of the values is
 * largest and then returns the largest value in the form 
 * of a double to the program to be output to the user.
 */
double max (double x, double y)
{
    
    if (x > y)
    {
        return x;
    }
    else if (x < y)
    {
        return y;
    }
    else 
    {
         return true;
    }
        
}

/*
 * Max Function #2 
 * (Three Parameters, all 3 doubles)
 * This function takes in 3 values. With the use of 
 * IF/ELSE statements, it then determines which of the three
 * values is the largest and returns the largest value
 * in the form of a double to the program it was invoked by.
 */
double max (double x, double y, double z)
{
    
    if (x > y && x > z)
    {
       return x;
    }
    else if (y > x && y > z)
    {
        return y;
    }
    else if (z > x && z > y)
    {
         return z;
    }
    else 
    {
         return true;
    }
        
}

/*
 * Ice Cream Portion Size Calculator
 * (Two Parameters, 1 int and 1 double)
 * This function takes in 1 integer value and 1 double value.
 * It then returns the double value divided by the integer value
 * in the form of a double to the program that it was invoked by.
 */
double portionSize (int x, double y)
{
    return y / x;
}

/*
 * Swap Function 
 * (Two Parameters, BOTH int&)
 * This function will take two input values from the user and
 * swap them by accessing the memory location of the values
 * using REFERENCE parameters. It does not return any value 
 * to the program it was invoked by. 
 */
int swap (int& x, int& y)
{
    int temp = x;
    x = y;
    y = temp;
    
}

/*
 * Conversion Input Function (Weight Problem)
 * (Two Parameters, Referenced int)
 * This function gets two integer values from the user
 * and stores them in the memory location for each 
 * variable. No return value.
 */
void conversionInput (int& x, int& y)
{
    cout << "This program will convert your weight from "
            "Pounds & Ounces to Kilograms & Grams." << endl;
    
    cout << "Enter number of pounds: ";
    cin >> x;
    
    cout << "Enter number of ounces: ";
    cin >> y;
  
}

/*
 * Weight Conversion Function 
 * (4 Parameters, Two int, two referenced double)
 * This function takes 4 values, two integers that were
 * input and saved by the conversionInput() function and two
 * double memory locations to be stored once the function
 * has been executed. It converts pounds to kilograms, and
 * ounces to grams, storing those values in the referenced
 * doubles. No return.
 */
void weightConversion (int w, int x, double& y, double& z)
{
    //Pounds to Kilograms
    y = w / 2.2046;
    
    //Ounces to Grams 
    z = ((x / 16.0) / 2.2046) * 1000;
    
}

/*
 * Conversion Output Function (Weight Conversion)
 * (2 Parameters, BOTH double)
 * This function takes in two doubles and outputs those values 
 * to the user. It then asks the user if they would like to 
 * repeat the program to do another calculation and if they 
 * answer "Yes" it redirects them back to the main program to
 * repeat. No return.
 */
void conversionOutput(double x, double y)
{
    cout << "That is " << x << " kilograms and " <<
            y << " grams." << endl;
    
    cout << "Would you like to do another conversion? "
            "Type Yes or No:  ";
    string repeat;
    cin >> repeat;
    
    if (repeat == "Yes" || repeat == "yes")
    {
        problem5();
    }  
    
}

/*
 * Conversion Input Function (Length Problem)
 * (Two Parameters, Referenced int)
 * This function gets two integer values from the user
 * and stores them in the memory location for each 
 * variable. No return value.
 */
void conversionInput2 (int& a, int& b)
{
    cout << "This program will convert a length from "
            "Feet & Inches to Meters & Centimeters." << endl;
    
    cout << "Enter number of feet: ";
    cin >> a;
    
    cout << "Enter number of inches: ";
    cin >> b;
  
}

/*
 * Length Conversion Function 
 * (4 Parameters, Two int, two referenced double)
 * This function takes 4 values, two integers that were
 * input and saved by the conversionInput2() function and two
 * double memory locations to be stored once the function
 * has been executed. It converts feet to meters, and
 * inches to centimeters, storing those values in the referenced
 * doubles. No return.
 */
void lengthConversion (int a, int b, double& c, double& d)
{
    //Feet to Meters
    c = a * 0.3048;
    
    //Inches to Centimeters
    d = ((b / 12.0) * 0.3048) * 100;
}

/*
 * Conversion Output Function (Length Conversion)
 * (2 Parameters, BOTH double)
 * This function takes in two doubles and outputs those values 
 * to the user. It then asks the user if they would like to 
 * repeat the program to do another calculation and if they 
 * answer "Yes" it redirects them back to the main program to
 * repeat. No return.
 */
void conversionOutput2 (double num1, double num2)
{
    cout << "That is " << num1 << " meters and " <<
            num2 << " centimeters." << endl;
    
    cout << "Would you like to do another conversion? "
            "Type Yes or No:  ";
    string repeat;
    cin >> repeat;
    
    if (repeat == "Yes" || repeat == "yes")
    {
        problem6();
    }  
}

/*
 * Quarter Computing Function
 * (4 Parameters, 1 int, 1 referenced double, 2 referenced ints)
 * This function takes in coinValue for quarters, the change that 
 * the user entered, amountLeft to be saved to memory, and 
 * numQuarters to be saved to memory. No return.
 */
void computeQuarters (int coinValue, double& num, int& amountLeft
, int& numQuarters)
{
    //Mulitplying change by 100 to get integer value to work with
    num *= 100;
    
    //Finding number of quarters
    numQuarters = static_cast <int> (num) / coinValue;
    
    //Getting amount left with modulus
    amountLeft = static_cast <int> (num) % coinValue;
}

/*
 * Dime Computing Function
 * (3 Parameters, 1 int, 2 referenced ints)
 * This function takes in coinValue for dimes, amountLeft 
 * from the previous function. and numDimes to be
 * saved to memory. No return.
 */
void computeDimes (int coinValue, int& amountLeft, int& numDimes)
{
    //Getting the number of dimes
    numDimes = amountLeft / coinValue;
    
    //Getting the amount left over for next function
    amountLeft = amountLeft % coinValue;
}

/*
 * Penny Computing Function
 * (3 Parameters, 1 int, 2 referenced ints)
 * This function takes in coinValue for pennies, amountLeft 
 * from the previous function. and numPennies to be
 * saved to memory. No return.
 */
void computePennies (int coinValue, int& amountLeft,
        int& numPennies)
{
    //Getting number of pennies
    numPennies = amountLeft / coinValue;
    
}
    
