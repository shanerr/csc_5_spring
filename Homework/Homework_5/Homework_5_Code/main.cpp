/* 
 * File:   main.cpp
 * Author: Thomas
 *
 * Created on March 30, 2014, 1:01 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>


using namespace std;

//Human version of Rock, Paper, Scissors
void problem1 ()
{
    //Execute Human version of Rock, Paper, Scissors
     char player1, player2;
    
    do
    {
        cout << endl;
        cout << "Enter 'Z'  for both users to Quit program." << endl;
        cout << endl << "Welcome to the Rock, Paper, Scissors Game"
            << endl;
        cout << "When it is your turn, enter R for 'Rock', P for "
                "'Paper', and S for 'Scissors'." << endl;
        cout << "Player 1 enter your play: ";
        cin >> player1;
        cout << "Player 2 enter your play: ";
        cin >> player2;
    
    
    switch (player1)
    {
        case 'R':
        case 'r':
            switch (player2)
            {
                case 'R':
                case 'r':
                    cout << "Nobody Wins." << endl;
                    break;
                case 'P':
                case 'p':
                    cout << "Paper covers rock." << endl;
                    cout << "Player 2 wins!" << endl; break;
                case 'S':
                case 's':
                    cout << "Rock breaks scissors." << endl;
                    cout << "Player 1 wins!" << endl; break;
                    
                default: cout <<"Invalid entry." << endl; break;
            }
            break;
            
                case 'S':
                case 's':
                switch (player2)
                {
                    case 'R':
                    case 'r':
                        cout << "rock breaks scissors." << endl;
                        cout << "Player 2 wins!" << endl; break;
                    case 'P':
                    case 'p':
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 1 wins!" << endl; break;
                    case 'S':
                    case 's':
                        cout << "Nobody wins." << endl;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
                        
                case 'P':
                case 'p':
                switch (player2)
                {
                    case 'R':
                    case 'r':
                        cout << "Paper covers rock." << endl;
                        cout << "Player 1 wins." << endl; break;
                    case 'P':
                    case 'p':
                        cout << "Nobody wins." << endl;
                        break;
                    case 'S':
                    case 's':
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 2 wins!" << endl; break;

                    default: cout << "Invalid entry." << endl; break;
                }
                
         }
    
    }
    while (player1 != 'Z');
}
    


//Bot version of Rock, Paper, Scissors
void problem2 ()
{
    //Execute Bot version of Rock, Paper Scissors
     int rock = 1;
    int paper = 2;
    int scissors = 3;
    string proceedOrNot;
    int player3, player4;
    
    do
    {
        cout << endl;
        
        cout << endl << "Welcome to the Rock, Paper, Scissors Game"
            << endl;
        cout << "Enter 'No' to Quit program." << endl;
        cout << "Two computer bots will choose between Rock, "
                "Paper, and Scissors." << endl;
        cout << "Continue? ";
        cin >> proceedOrNot;
        if (proceedOrNot == "Yes")
        {
        
        //Choosing two random values between 1-3 
        //To get Rock, Paper, or Scissors for each player
        srand (time (0));
        player3 = (rand() % 3) + 1;
        player4 = (rand() % 3) + 1;
        
        string player1Choice;
        string player2Choice;
        
        //Deciding What Player 1 Chose
        if (player3 == 1)
        {
            player1Choice = "Rock";
        }
        else if (player3 == 2)
        {
            player1Choice = "Paper";
        }
        else
        {
            player1Choice = "Scissors";
        }
        cout << "Player 1 chose: ";
        cout << player1Choice << endl;
        
         //Deciding What Player 2 Chose 
        
        if (player4 == 1)
        {
            player2Choice = "Rock";
        }
        else if (player4 == 2)
        {
            player2Choice = "Paper";
        }
        else
        {
            player2Choice = "Scissors";
        }
        
        cout << "Player 2 chose: ";
        cout << player2Choice << endl;
        
    
    
    switch (player3)
    {
        case 1: 
            switch (player4)
            {
                case 1:
                    cout << "Nobody Wins." << endl;
                    break;
                case 2:
                    cout << "Paper covers rock." << endl;
                    cout << "Player 2 wins!" << endl; break;
                case 3:
                    cout << "Rock breaks scissors." << endl;
                    cout << "Player 1 wins!" << endl; break;
                    
                default: cout <<"Invalid entry." << endl; break;
            }
            break;
            
        case 2:
                switch (player4)
                {
                    case 1:
                        cout << "Paper covers rock." << endl;
                        cout << "Player 1 wins!" << endl; break;
                    case 2:
                        cout << "Nobody wins." << endl; break;
                    case 3:
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 2 wins!" << endl; break;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
                        
        case 3:
                switch (player4)
                {
                    case 1:
                        cout << "Rock breaks scissors." << endl;
                        cout << "Player 2 wins." << endl; break;
                    case 2:
                        cout << "Scissors cut paper." << endl;
                        cout << "Player 1 wins!" << endl; break;
                    case 3:
                        cout << "Nobody wins." << endl; break;

                    default: cout << "Invalid entry." << endl; break;
                }
                break;
         }
    }
        else
        {
            cout << "Thank you for playing! Goodbye." << endl;
        }
   
    }
    while (proceedOrNot != "No");
        
}

int MPG (int numLiters, int numMiles)
{
    const double gallonsPerLiter = 0.264179;
    int numGallons = numLiters * gallonsPerLiter;
    
    return numMiles / numGallons;
}

int MPG2 (int numLiters2, int numMiles2)
{
    const double gallonsPerLiter = 0.264179;
    int numGallons2 = numLiters2 * gallonsPerLiter;
    
    return numMiles2 / numGallons2;
}

void problem3()
{
    // MPG Problem
    // Homework 5 Problem #5
    int numLiters;
    int numMiles;
    int milesPerGallon;
    
    
    while (numLiters != 999)
    {
    //Prompt User
    cout << endl;
    cout << "This program will calculate your vehicle's MPG." << endl;
    cout << "You may enter '999' to quit." << endl;
    cout << "Enter the number of liters of gasoline "
            "used by your car: ";
    cin >> numLiters;
    
    if (numLiters == 999)
    {
        cout << "Goodbye.";
    }
    else
    {
    cout << "Enter the number of miles traveled: ";
    cin >> numMiles;

    //Calling MPG Function
    int milesPerGallon = MPG (numLiters, numMiles);
    
    cout << "Your vehicle's MPG is " << milesPerGallon << 
            " miles per gallon." << endl;
    }
    }
}

void problem4()
{
    // MPG Problem
    // Homework 5 Problem #5
    int numLiters;
    int numMiles;
    int milesPerGallon;
    string quit = "Quit";
    string fileName;
    
    while (fileName != quit)
    {
    //Prompt User
    cout << endl;
    cout << "This program will calculate your vehicle's MPG." << endl;
    cout << "You may enter 'Quit' to quit." << endl;
    cout << "Enter a filename to take data from: ";

    cin >> fileName;
    
    if (fileName == quit)
    {
        cout << "Goodbye.";
    }
    else
    {
        ifstream infile;
        
        string fileContent;
        infile.open (fileName.c_str());
        infile >> numLiters;
        infile >> numMiles;
        
        infile.close ();

    //Calling MPG Function
    int milesPerGallon = MPG (numLiters, numMiles);
    
    cout << "Your vehicle's MPG is " << milesPerGallon << 
            " miles per gallon." << endl;
    
    
    }
    }
    
}

void problem5()
{
    // MPG Problem
    // Homework 5 Problem #5
    int numLiters, numLiters2;
    int numMiles, numMiles2;
    string quit = "Quit";
    string fileName;
    
    while (fileName != quit)
    {
    //Prompt User
    cout << endl;
    cout << "This program will calculate your vehicle's MPG." << endl;
    cout << "You may enter 'Quit' to quit." << endl;
    cout << "Enter a filename to take data from: ";

    cin >> fileName;
    
    if (fileName == quit)
    {
        cout << "Goodbye.";
    }
    else
    {
        ifstream infile;
        
        string fileContent;
        infile.open (fileName.c_str());
        infile >> numLiters;
        infile >> numMiles;
        infile >> numLiters2;
        infile >> numMiles2;
        
        infile.close ();

    //Calling MPG Function
    int milesPerGallon = MPG (numLiters, numMiles);
    int milesPerGallon2 = MPG2 (numLiters2, numMiles2);
    
    cout << "The first vehicle's MPG is " << milesPerGallon << 
            " miles per gallon." << endl;
    
    cout << "The second vehicle's MPG is " << milesPerGallon2 <<
            " miles per gallon." << endl;
    
    if (milesPerGallon > milesPerGallon2)
    {
        cout << "Vehicle #1 has better fuel effeciency!" << endl;
    }
    else if (milesPerGallon < milesPerGallon2)
    {
        cout << "Vehicle #2 has better fuel effeciency!" << endl;
    }
    else
    {
        cout << "Both vehicles have the same fuel effeciency." 
                << endl;
    }
    
    }
    }
    
}

/*
 *
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 *  
 */
int main(int argc, char** argv) {

    
    //Prompt User
    cout << "Enter 1 for RPS with Two Players" << endl;
    cout << "Enter 2 for RPS with Two Computer Bots" << endl;
    cout << "Enter 3 for MPG Calculator #1" << endl;
    cout << "Enter 4 for MPG Calculator with file I/O" << endl;
    cout << "Enter 5 for MPG Calculator with file I/O "
            "for 2 separate vehicles" << endl;
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1: 
            problem1();
            break;
        case 2:
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 4:
            problem4();
            break;
        case 5:
            problem5();
            break;
        default:
            cout << "Invalid Entry." << endl;
            
    }
            
    
    return 0;
}

