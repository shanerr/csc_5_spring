/* 
 * Name: Shane Brown
 * ID: 2431194
 * Date: 5-30-2014
 * HW: 10
 * Class: Monday/Wednesday
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

//Function Prototypes
void problem1();
int* createDynamicArray (int);
void storeValues (int*, int);
void outputArray (int*, int);
void problem3();
void addValue (int*, int, int&);
int* allocateMemory (int*, int&);
void outputArray2 (int*, int);
void problem4();
void outputArray3 (int*, int);
void problem5();
string* createNamesArray (int);
void displayNames (string *, int);
string* deleteEntry (string*, int&, int);
void problem6();
string* addName (string*, int&, string);
void problem7();
string* addNameAtLoc (string*, int&, string, int);
void problem8();
bool findName (string*, int&, string);
string* deleteName (string*, int&, string);
void problem9();
/*
 * 
 */
int main(int argc, char** argv) {
    
    //Main Menu/////////////////////////////////////////////////////
    int selection;
    while (selection != -1)
    {
        cout << "Homework 10" << endl << endl;
        cout << "You may enter -1 to quit." << endl;
        
        cout << "Enter 1 for Problem #1/#2" << endl;
        cout << "Enter 3 for Problem #3" << endl;
        cout << "Enter 4 for Problem #4" << endl;
        cout << "Enter 5 for Problem #5" << endl;
        cout << "Enter 6 for Problem #6" << endl;
        cout << "Enter 7 for Problem #7" << endl;
        cout << "Enter 8 for Problem #8" << endl;
        
        cin >> selection;
        
        switch (selection)
        {
            case -1:
                cout << "Goodbye." << endl;
                break;
            case 1: 
                problem1();
                break;
            case 3:
                problem3();
                break;
            case 4:
                problem4();
                break;
            case 5:
                problem5();
                break;
            case 6:
                problem6();
                break;
            case 7:
                problem7();
                break;
            case 8:
                problem8();
                break;
            case 9: 
                problem9();
                break;
            default:
                cout << "Error." << endl;
        }
    }

    return 0;
}

//PROBLEM #1 and #2//////////////////////////////////////////////////////
//Dynamic Array Size from User && Three Functions////////////////////////
void problem1()
{
    cout << "Please enter a size for a dynamic array: ";
    int size;
    cin >> size;
    
    int* array = createDynamicArray (size);
    storeValues (array, size);
    outputArray (array, size);
    
    delete[] array;
}

int* createDynamicArray (int size)
{
    int* array = new int[size];
    
    return array;
}

void storeValues (int* array,int size)
{
    srand(time(0));
    for (int i = 0; i < size; i++)
    {
        array[i] = (rand() % (20 - 1)) + 1;
    }
}

void outputArray (int* array, int size)
{
    cout << "Dynamic Array with Random Values: " << endl;
    for (int i = 0; i < size; i++)
    {
        cout << array[i] << " ";
    }
    
    cout << endl << endl;
}

//PROBLEM #3////////////////////////////////////////////////////////////
//Dynamic Array user entering values////////////////////////////////////
void problem3()
{
    int size = 5;
    int min = 999999;
    int* p = new int[size];
    int value;
    int loc = 0;
    
    cout << "You may enter -1 to stop and display final array." << endl;
    cout << "Enter values to put into an array: " << endl;
    
    while (value != -1 && loc != 5)
    {
        cin >> value;
        
        addValue (p, value, loc);
        
    }
    
    outputArray2 (p, loc);
    
    delete[] p;
}

void addValue (int* p, int value, int& loc)
{
    
    if (value != -1)
        p[loc] = value;
    
    loc++;
}

void outputArray2 (int* p, int size)
{
    cout << endl << "The final array is: " << endl;
    
    for (int i = 0; i < size; i++)
    {
        cout << p[i] << " ";
    }
    cout << endl << endl;
}

//PROBLEM #4///////////////////////////////////////////////////////
//Dynamic Array Allowing user to enter until done//////////////////
void problem4()
{
    int size = 2;
    int min = 999999;
    int* p = new int[size];
    int value;
    int loc = 0;
    
    cout << "You may enter -1 to stop and display final array." << endl;
    cout << "Enter values to put into an array: " << endl;
    
    while (value != -1)
    {
        cin >> value;
        
        addValue (p, value, loc);

        
        if (size <= loc)
        p = allocateMemory (p, size);
        
    }
    
    outputArray3 (p, size);
    
    delete[] p;
}

/*
 * allocateMemory Function
 * (Two Parameters: One int*, One int&)
 * This function allocates new memory by creating a 
 * new dynamic array and copying the values from the previous
 * array into the new one and then it deletes the old array.
 * 
 * Returns an integer pointer.
 */
int* allocateMemory (int* p, int& size)
{
    int tempSize = (size + 1);
    int* temp = new int [tempSize];
    
    for (int i = 0; i < size; i++)
    {
        temp[i] = p[i];
    }
    
    delete[] p;
    size = tempSize;
    
    return temp;
    
}

void outputArray3 (int* p, int size)
{
    cout << endl << "The final array is: " << endl;
    
    for (int i = 0; i < size - 2; i++)
    {
        cout << p[i] << " ";
    }
    cout << endl << endl;
}
//PROBLEM #5///////////////////////////////////////////////////
//Deleting Entry//////////////////////////////////////////////////
void problem5()
{
    int size = 5;
    
    string* array = createNamesArray (size);
    
    displayNames (array, size);
    
    cout << "Please enter a location to delete (0-4): ";
    int loc;
    cin >> loc;
    
    while (loc >= size)
    {
        cout << "Invalid Location. Try Again: ";
        cin >> loc;
    }
    
    string* finalArray = deleteEntry (array, size, loc);
    
    displayNames (finalArray, size);
    
}

/*
 * createNamesArray
 * (One parameter: int)
 * This function simply makes a new dynamic array and then 
 * fills it with the five names I chose.
 * 
 * Returns string pointer. (pointing to array of names)
 */
string* createNamesArray (int size)
{
    
    string* array = new string [size];
    
    array[0] = "Shane";
    array[1] = "Rachel";
    array[2] = "Kristen";
    array[3] = "Matt";
    array[4] = "Thomas";
    
    return array;
    
}

void displayNames (string* array, int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << array[i] << "  ";
    }
    
    cout << endl;
    
}

/*
 * deleteEntry function
 * (3 Parameters: string*, int&, int)
 * This function takes the original string pointer that is 
 * pointing to the array of names and deletes the entry 
 * at the location given by the user by swapping the name
 * at the location with the last name in the array and then 
 * decreasing the size of the array by 1.
 * 
 * Returns string pointer. (pointing to new array).
 */
string* deleteEntry (string* array, int& size, int loc)
{
    int tempSize = size - 1;
    string* temp = new string [size];
    
    
    for (int i = 0; i < size; i++)
    {
        temp[i] = array[i];
    }
    
    swap (temp[loc], temp[tempSize]);
  
    delete[] array;
    
    size = tempSize;
    
    return temp;
    
}

///PROBLEM #6///////////////////////////////////////////////////
//Adding Name//////////////////////////////////////////////////
void problem6()
{
    int size = 5;
    
    string* array = createNamesArray (size);
    
    displayNames (array, size);
    
    cout << "Please enter a name you would like to add: ";
    string name;
    cin >> name;
    
    string* finalArray = addName (array, size, name);
    
    displayNames (finalArray, size);
    
    
}

/*
 * addName Function
 * (3 Parameters: string*, int&, string)
 * This function creates a new temporay array and fills it
 * with the names from the old array that is passed in as 
 * a parameter, and it also increases the size by one. Then the 
 * name entered by the user is added on the end of the array.
 * 
 * Returns string pointer. (pointing to new array).
 */
string* addName (string* array, int& size, string name)
{
    int tempSize = size + 1;
    string* temp = new string [tempSize];
    
    
    for (int i = 0; i < size; i++)
    {
        temp[i] = array[i];
    }
    
    temp[size] = name;
   
    delete[] array;
    
    size = tempSize;
    
    return temp;
    
}
///PROBLEM #7///////////////////////////////////////////////////
///Adding Name at Location/////////////////////////////////////
void problem7()
{
    int size = 5;
    
    string* array = createNamesArray (size);
    
    displayNames (array, size);
    
    cout << "Please enter a name you would like to add: " << endl;
    string name;
    cin >> name;
    
    cout << "Please enter the location you would like to"
            "insert it (0-5): " << endl;
    int loc;
    cin >> loc;
    
    while (loc > size)
    {
        cout << "Location invalid. Try again: ";
        cin >> loc;
    }
    
    string* finalArray = addNameAtLoc (array, size, name, loc);
    
    displayNames (finalArray, size);
    
    
}

/*
 * addNameAtLoc Function
 * (4 Parameters: string*, int&, string, int)
 * This function takes a name and location from the user to add
 * into the array of names. It then adds the name at the location
 * specified by making a temporary array and increasing its size
 * to be one bigger than previous array. Then, all the old values
 * are copied into the temporary array and all values at and after
 * the location specified are assigned to each other essentially
 * moving them over one space. Finally, the name given by the user
 * is simply added into the array at the location.
 * 
 * Returns string pointer. (pointing to new array).
 */
string* addNameAtLoc (string* array, int& size, string name, int loc)
{
    int tempSize = size + 1;
    string* temp = new string [tempSize];
    
    
    for (int i = 0; i < size; i++)
    {
        temp[i] = array[i];
    }
    
    for (int i = size; i > loc ; i--)
    {
        temp[i] = temp[i-1];
    }
    
    temp[loc] = name;
   
    delete[] array;
    
    size = tempSize;
    
    return temp;
    
}
///PROBLEM #8////////////////////////////////////////////////
///User Entering Name to Delete Instead of Loc//////////////
void problem8()
{
    int size = 5;
    
    string* array = createNamesArray (size);
    
    displayNames (array, size);
    
    cout << "Please enter the name you would like to delete: " 
            << endl;
    string name;
    cin >> name;
    
    while (!findName)
    {
        cout << "Name not found. Try Again: ";
        cin >> name;
    }
    
    string* finalArray = deleteName (array, size, name);
    
    displayNames (finalArray, size);
    
}

/*
 * findName Function 
 * (3 Parameters: string*, int&, string)
 * This function simply iterates through the array of names 
 * to find if the name given by the user to delete is contained
 * within the array or not.
 * 
 * Returns boolean.
 */
bool findName (string* array, int& size, string name)
{
    for (int i = 0; i < size; i++)
    {
        if (array[i] == name)
            return true;
    }
    
    return false;
}

/*
 * deleteName Function 
 * (3 Parameters: string*, int&, string)
 * This creates a new dynamic array and copies the names from
 * the old array into it.It then iterates through the array of
 * names provided and assigns the location of the name the user 
 * wants to delete to the variable loc. It then simply swaps 
 * the name to be deleted with the final name in the new array 
 * before decreasing its size by one, getting rid of the 
 * unwanted name.
 * 
 * Returns string pointer. (pointing to new array).
 */
string* deleteName (string* array, int& size, string name)
{
    int loc;
    int tempSize = size - 1;
    string* temp = new string [size];
    
    
    for (int i = 0; i < size; i++)
    {
        temp[i] = array[i];
        
        if (temp[i] == name)
            loc = i;
    }
    
    swap (temp[loc], temp[tempSize]);
  
    delete[] array;
    
    size = tempSize;
    
    return temp;
    
}

///PROBLEM #9//////////////////////////////////////////////////
//Histogram of Grades/////////////////////////////////////////
void problem9()
{
    //NEED HELP ON THIS ONE///////////////////////////////////
    /////////////////////////////////////////////////////////
    
}