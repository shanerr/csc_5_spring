/* 
 * Name: Shane Brown
 * ID: 2431194
 * Date: 5-7-14
 * HW: 8
 * 
 * I certify that this is my own code.
 */

#include <cstdlib>
#include <iostream>
#include <vector>


using namespace std;

//Function Prototypes 
void problem1();
void removeLocation (vector <int>&, int);
void problem2();
void removeLocation (int [], int&, int);
void outputArray (int[], int);
void problem3();
bool found (vector <int>&, int);
void outputVector(vector <int>& );
void removeElements (vector <int>& , int);
void problem5();
bool found (int[], int, int);
void removeElements (int[], int&, int);
void problem7();
void problem8();
bool checkID (vector <int>&, int);
void planeTicket (vector <string>&, vector <int>&);
void problem9();
void outputCharArray (char x[], int&);
void delete_repeats (char x[], int&);


/*
 * 
 */
int main(int argc, char** argv) {
    
    //|||||||||||||||||MAIN MENU||||||||||||||||||||||||||||
    cout << "Homework 8" << endl;
    
    cout << "Enter 1 for Problem #1" << endl;
    cout << "Enter 2 for Problem #2" << endl;
    cout << "Enter 3 for Problem #3/#4" << endl;
    cout << "Enter 5 for Problem #5" << endl;
    cout << "Enter 7 for Problem #7" << endl;
    cout << "Enter 8 for Problem #8" << endl;
    cout << "Enter 9 for Problem #9" << endl;
    
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1:
            problem1();
            break;
        case 2:
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 5:
            problem5();
            break;
        case 7:
            problem7();
            break;
        case 8:
            problem8();
            break;
        case 9:
            problem9();
            break;
        default:
            cout << "Error." << endl;
            break;
    }

    return 0;
}

///////////////PROBLEM #1
void problem1()
{
    cout << "Below is a vector of five random integers." << endl;
    
    vector <int> fiveRandom;
    
    srand(time(0));
    
    for (int i = 0; i < 5; i++)
    {
        int num = (rand() % 9);
        fiveRandom.push_back(num);
    }
    
    outputVector (fiveRandom);
    
    cout << endl;
    
    cout << "Please enter the location of a number to delete: ";
    int loc;
    cin >> loc;
    
    removeLocation (fiveRandom, loc);
    outputVector (fiveRandom);
}

/*
 * Remove Location Function (overloaded)
 * (vector, and int as parameters)
 * This function receives a vector and the location to be
 * deleted from the user. It then deletes the value at the 
 * location given.
 */
void removeLocation (vector <int>& x, int loc)
{
    if (x.size() == 0)
        return;
    
    for (int i = loc; i < x.size() - 1; i++)
    {
        x[i] = x[i+1];
    }
    x.pop_back();
    
}

////////////////PROBLEM #2
void problem2()
{
    cout << "Below is an array of five random integers." << endl;
    
    int size = 5;
    int array1 [5];
    
    srand(time(0));
    
    for (int i = 0; i < 5; i++)
    {
        int num = (rand() % 9);
        array1[i] = num;
    }
    
    outputArray (array1, size);
    
    cout << endl;
    
    cout << "Please enter the location of a number to delete: ";
    int loc;
    cin >> loc;
    
    removeLocation (array1, size, loc);
    outputArray (array1, size);
    
    
}

/*
 * Output Array Function 
 * (array, int as parameters)
 * This function receives an array and its size as parameters 
 * and then proceeds to output the array, one value at a time 
 * using a for loop.
 */
void outputArray (int x[], int size)
{
    cout << endl;
    
    for (int i = 0; i < size; i++)
    {
        cout << x[i] << "  ";
    }
    
}

/*
 * Remove Location Function (overloaded)
 * (array, int, int as parameters)
 * This function receives an array, its referenced size, and
 * the location given by the user to remove from. It then removes 
 * the value at the location given by the user and decrements
 * the size by 1.
 */
void removeLocation (int x[], int& size, int loc)
{
    for (int i = loc; i < size - 1; i++)
    {
        x[i] = x[i + 1];
    }
    
    size--;
    
}

//////////////PROBLEM #3
void problem3()
{
    cout << "Below is a vector of five random integers." << endl;
    
    vector <int> fiveRandom;
    
    srand(time(0));
    
    for (int i = 0; i < 5; i++)
    {
        int num = (rand() % 9);
        fiveRandom.push_back(num);
    }
    
    outputVector (fiveRandom);
    
    cout << endl;
    
    cout << "Please enter the number you would like to remove: ";
    int remove;
    cin >> remove;
    
    found (fiveRandom, remove);
    
    while (!found (fiveRandom, remove)) 
    {
        cout << "The value is not in the vector." << endl <<
                "Please enter another value: ";
        cin >> remove;
        found (fiveRandom, remove);
    }
    
    
    removeElements (fiveRandom, remove);
    outputVector (fiveRandom);

}

/*
 * Found function (overloaded)
 * (vector and int as parameters)
 * This function checks to see if a vector is empty then 
 * checks if the value in question is already within the 
 * vector.
 * 
 * Returns a bool.
 */
bool found (vector <int>& x, int y)
{
    if (x.size() == 0)
        return -1;
    
    for (int i = 0; i < x.size(); i++)
    {
        if (x[i] == y)
            return true;
    }
    return false;
}

/*
 * Output Vector function
 * (vector as parameter)
 * This function simply outputs a vector, character by
 * character using a for loop.
 */
void outputVector (vector <int>& x)
{
    cout << endl;
    
    for (int i = 0; i < x.size(); i++)
    {
        cout << x[i] << "  ";
    }
    
    cout << endl;
}

/*
 * Remove Elements Function (overloaded)
 * (vector and int as parameters)
 * This function receives a vector and the number to be 
 * removed as parameters and it removes the number from the
 * vector and still maintains the right order.
 */
void removeElements (vector <int>& x, int remove)
{
    int loc;
    
    for (int i = 0; i < x.size(); i++)
    {
        if (x[i] == remove)
        {
            loc = i;
        }
    }
    
    for (int i = loc; i < x.size() - 1; i++)
    {
        x[i] = x[i + 1];
    }
    
    x.pop_back();
    
}

//////////////PROBLEM #5
void problem5()
{
    cout << "Below is an array of five random integers." << endl;
    
    int size = 5;
    int loc = 0;
    int array1 [5];
    
    srand(time(0));
    
    for (int i = 0; i < size; i++)
    {
        int num = (rand() % 9);
        array1[i] = num;
    }
    
    outputArray (array1, size);
    
    cout << endl;
    
    cout << "Please enter the number you would like to remove: ";
    int remove;
    cin >> remove;
    
    found (array1, remove, size);
    
    while (!found (array1, remove, size)) 
    {
        cout << "The value is not in the array." << endl <<
                "Please enter another value: ";
        cin >> remove;
        found (array1, remove, size);
    }
    
    
    removeElements (array1, size, remove);
    outputArray (array1, size);
    
    
}

/*
 * Found function (overloaded)
 * (array, int, int as parameters)
 * This function takes in an array, the number in question,
 * and the size of the array to check and see if the number is 
 * found within the array. 
 * 
 * Returns a bool.
 */
bool found (int x[], int remove, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (x[i] == remove)
            return true;
    }
    return false;
    
}
/*
 * Remove Elements Function (overloaded)
 * (array, int, int as parameters)
 * This function takes in an array, its referenced size, 
 * and the number that is being removed and it removes it 
 * from the array.
 */
void removeElements (int x[], int& size, int remove)
{
    int loc;
    
    for (int i = 0; i < size; i++)
    {
        if (x[i] == remove)
        {
            loc = i;
        }
    }
    
    for (int i = loc; i < size - 1; i++)
    {
        x[i] = x[i + 1];
    }
    
    size--;
}
//////////////PROBLEM #7
void problem7()
{
    vector <int> v;
    int value;
    
    //First Value being put into empty vector
    cout << "You may enter -999 to quit." << endl;
        cout << "Please enter a value to put into a vector: ";
        cin >> value;
        
        v.push_back(value);
        
        outputVector (v);
        
    while (value != -999)
    {
        cout << "You may enter -999 to quit." << endl;
        cout << "Please enter a value to put into a vector: ";
        cin >> value;
    
        //Calling found function 
        bool foundValue = found (v, value);
        
        //If value is already being used
        if (foundValue)
        {
            cout << "Value already being used. Try another: ";
        }
        
        //If not being used and user does not input -999
        else if (!foundValue && value != -999)
        {
            v.push_back(value);
        }
        else
        {
            
        }
        
        outputVector (v);
    }
    
}
/////////////////PROBLEM #8
void problem8()
{
    vector <string> names;
    vector <int> ticketID;
    
    cout << "Please enter name of passenger: " << endl;
    string name;
    cin >> name;
    
    //Adding first passenger input to empty vector
    srand (time(0));
    int IDnum = (rand() % 39) + 1;
    
    names.push_back(name);
    ticketID.push_back (IDnum);
    
    planeTicket (names, ticketID);
    
    while (name != "Quit")
    {
        cout << "You may enter 'Quit' to exit." << endl;
        cout << "Please enter name of passenger: " << endl;
        cin >> name;
        
        //Generate random passenger ID (1-40)
        IDnum = (rand() % 39) + 1;
        
        //Calling checkID function
        bool foundID = checkID (ticketID, IDnum);
        
        
        while(foundID)
        {
            IDnum = (rand() % 39) + 1;
            
            foundID = checkID (ticketID, IDnum);
        }
        
        //If user does NOT quit
        if (name != "Quit")
        {
            //Add name and ID to Current Passengers
            names.push_back(name);
            ticketID.push_back(IDnum);
        }
        else
        {
            
        }
        
        planeTicket (names, ticketID);
    }
    
}

/*
 * Check ID Number function
 * (vector and ID # as parameters)
 * This function takes in a vector and the random ID number
 * that was generated to determine if the ID number has already
 * been used by another passenger.
 * 
 * Returns a bool.
 */
bool checkID (vector <int>& ticketID, int IDnum)
{
    for (int i = 0; i < ticketID.size(); i++)
    {
        if (ticketID[i] == IDnum)
        {
            return true;
        }
    }
    return false;
    
}

/*
 * Plane Ticket Function
 * (Parallel vectors as parameters)
 * This function outputs the current passengers on the plane
 * using two parallel vectors and adding to the list 
 * each time a new passenger is input.
 */
void planeTicket (vector <string>& x, vector<int>& y)
{
    cout << endl;
    
    cout << "Current Passengers:" << endl;
    
    for (int i = 0; i < x.size(); i++)
    {
        cout << x[i] << "   " << y[i] << endl;
    } 
}

/////////////////PROBLEM #9 
void problem9()
{
    //Array of 10 spaces
    char array1[10] = {'a','a','c','d'};
    //4 spaces filled
    int size = 4;
    
    //Function Calls 
    outputCharArray (array1, size);
    delete_repeats (array1, size);
    outputCharArray (array1, size);
    
    
    
}

/*
 * Output Character Array Function
 * (Array and Int parameters)
 * This function simply outputs an array, character by
 * using the current referenced size of the array.
 */
void outputCharArray (char array1[], int& size)
{
    for (int i = 0; i < size; i++)
    {
        cout << array1[i] << "  ";
    }
    
    cout << endl;
}

/*
 * Delete Repeats Function
 * (Array and int parameters)
 * This function takes in a partially
 * filled array of characters and the 
 * size it currently contains and it then swaps the
 * duplicate character with the last character and cuts it off.
 */
void delete_repeats (char array1[], int& size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = i; j < size - 1; j++)
        {
            if (array1[j] == array1[j+1])
            {
                swap (array1[j+1], array1[size-1]);
            }
        }
    }
    size--;
    
}
