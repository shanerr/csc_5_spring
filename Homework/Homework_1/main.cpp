/* 
 * Name: Shane Brown
 * Student ID: 2431194
 * Date: February 26, 2014
 * HW: Homework_1
 * Problem: #1-7 
 * I certify this is my own work and code.
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int number_of_pods, peas_per_pod, total_peas;
    
    cout << "Hello." << endl;
    cout << "Press return after entering a number.\n"; 
    cout << "Enter the number of pods: \n";
    
    cin >> number_of_pods; 
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods * peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n" << endl; 
    
    cout << "Good-bye!\n" << endl; 
    cout << endl;
    cout << endl;
    
    
    // PROBLEM #5 on the Homework 
    
    //Variable Declaration
    int num1,num2;
    
    //Prompt user
    cout << "Please enter two integers: " << endl;
    
    //User input
    cin >> num1;
    cin >> num2;
    
    //Math Calculations 
    int sum = num1+num2;
    int product = num1*num2;
    
    //Math Expressions/Calculations Displayed to User
    cout << endl;
    cout << "Sum of your two integers: " << num1+num2 << endl;
    cout << "Product of your two integers: "<< num1*num2 << endl;
    
    cout << endl;
    cout << "This is the end of the program. \n" << endl;
    
    //CATALOG OF A FEW ERRORS
    //main.cpp:9:21: fatal error:  iostream: No such file or directory
    //Fix by having no space between < or > when including iostream library
    
    //main.cpp:16:11: error: ‘argc’ was not declared in this scope
    //Fix by including "int" in the main ()
    
    //main.cpp:63:5: error: ‘cut’ was not declared in this scope
    //Fix by not mispelling "cout"
    
    /*
     *
     *
     *
     *
     *
     *
     */
    
    
    
    
    //PROBLEM #7
    
    cout << " ***************************************************" << endl;
    cout << endl;
    cout << "            C C C                  S S S S          ! !" << endl;
    cout << "          C       C              S          S       ! !" << endl;
    cout << "         C                      S                   ! !" << endl;
    cout << "        C                        S                  ! !" << endl;
    cout << "        C                         S S S S           ! !" << endl;
    cout << "        C                                  S        ! !" << endl;
    cout << "         C                                  S       ! !" << endl;
    cout << "          C       C              S         S           " << endl;
    cout << "            C C C                  S S S S           00" << endl;
    cout << endl;
    cout << " ***************************************************" << endl;
    cout << endl;
    cout << "           COMPUTER SCIENCE IS COOL STUFF!!!" << endl;
    
     
    
    
    return 0;
}

