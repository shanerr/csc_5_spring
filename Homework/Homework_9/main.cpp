/* Name: Shane Brown
 * ID: 2431194
 * Date: 5-21-14
 * HW: 9
 * Class: CSC-5 Mon/Wed
 */

//Libraries
#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>


using namespace std;

//Global Constants (Used in Airplane Problem)
const int ROWS = 7;
const int COLS = 5;

//Function Prototypes
string input();
void problem1();
bool contained (vector <char>&, char);
void addToVector (vector <int>&, vector <char>&, char, string);
void sortLetters (vector <int>&, vector <char>&);
void outputLetters (vector <char>&, vector <int>&);
void problem2();
void fillSeatsArray (string[][COLS], bool [][COLS], ifstream&);
void displaySeats (string[][COLS]);
void problem3();

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Main Menu///////////////////////////////////////////////
    int selection;
    while (selection != -1)
    {
        cout << "Homework 9" << endl << "You may enter"
                " -1 to Quit." << endl << endl;
        

        cout << "Enter 1 for Problem #1" << endl;
        cout << "Enter 2 for Problem #2" << endl;
        cout << "Enter 3 for Problem #3" << endl;
        
        cin >> selection;

        switch (selection)
        {
            case 1: 
                problem1();
                break;
            case 2:
                problem2();
                break;
            case 3:
                problem3();
                break;
            case -1:
                break;
            default:
                cout << "Error." << endl;
                break;
        }
    }
    return 0;
}

/*
 * input Function
 * This function gets user input for the 1st problem 
 * using getline.
 * 
 * It returns the phrase that the user enters to the location
 * in problem1() where it was called.
 */
string input ()
{
    string phrase;
    cout << "Please enter a phrase:" << endl;
    
    getline (cin, phrase);
    getline (cin, phrase);
    
    return phrase;
    
}

//HOMEWORK PROBLEM #1///////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
//Letters and Occurences Problem////////////////////////////
void problem1()
{
    
    string s = input();
    
    //Declaring two vectors, one of type char and one 
    //of type int
    vector <char> containedLetters; 
    vector <int> count;
    
    //Iterating through string from user to check 
    //if letters have been counted yet
    for (int i = 0; i < s.size(); i++)
    {
        //If the letter has not been accounted for yet
        if (!contained (containedLetters, s[i]))
        {
            //Add letter to vector of letters used
            //Count how many of each letter is used
            addToVector (count, containedLetters, s[i], s);
        }
    }
    
    //Calling functions to sort letters and then output
    sortLetters (count, containedLetters);
    outputLetters (containedLetters, count);
    
}

/*
 * contained Function
 * (Parameters: vector <char>, char)
 * This function is a boolean function. It takes in the vector
 * with the contained letters and determines whether or not 
 * the character in question is in the vector already.
 * 
 * Returns a boolean value.
 */
bool contained (vector <char>& c, char loc)
{
    for (int i = 0; i < c.size(); i++)
    {
        if (c[i] == loc)
        {
            return true;
        }
    }
    return false;
}

/*
 * addToVector Function
 * (Parameters: 2 vectors, 1 char, 1 string)
 * 
 * This function takes in the two vectors being used for this 
 * problem and it checks how many of each character are 
 * used in the string from the user. It then adds the character
 * and the number of occurences of that character into 
 * parallel vectors.
 * 
 * No return.
 */
void addToVector (vector <int>& count, vector <char>& 
containedLetters, char c, string s)
{
    //Check all characters in string s and compare with character
    int counter = 0;
    
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] == c)
            counter++;
    }
    
    if (c != ' ' && c != '.')
    {
        count.push_back(counter);
        containedLetters.push_back(c);
    }
    
}

/*
 * sortLetters Function
 * (Parameters: 2 vectors)
 * 
 * This function takes in the two parallel vectors and sorts
 * them by the most number of occurences to the least
 * number of occurences of each letter.
 * 
 * No return.
 */
void sortLetters (vector <int>& count, vector <char>& 
containedLetters)
{
    for (int i = 0; i < count.size(); i++)
    {
        int loc = -1;
        int max = -1;
        
        for (int j = i; j < count.size(); j++)
        {
            if (count[j] > max)
            {
                max = count [j];
                loc = j;
            }
        }
        swap (count[i], count[loc]);
        swap (containedLetters[i], containedLetters[loc]);
    }
}

/*
 * outputLetters Function
 * (Parameters: 2 vectors)
 * 
 * This function simply takes in the two vectors used in 
 * problem1() and outputs the characters used in the string
 * from the user alongside the number of occurences seen by
 * each of those characters.
 * 
 * No return.
 */
void outputLetters (vector <char>& c,
        vector <int>& count)
{
    cout << "Letter  |  Number of Occurences" << endl;
    
    for (int i = 0; i < c.size(); i++)
    {
        cout << "  " <<  c[i] << "                " << 
                count[i] << endl;
    }
    
    cout << endl << endl;
}

//HOMEWORK PROBLEM #2/////////////////////////////////////////
//////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//Airplane Seats Problem/////////////////////////////////////
void problem2()
{
    int size = ROWS * COLS;
    
    //Array to hold seat arrangement
    string seats [ROWS][COLS];
    string seat;
    int row;
    
    //Array to hold bool values which determine whether
    //a seat is occupied or not
    bool seats2 [ROWS][COLS];
    
    //Opening 'seats.dat' file to read in seating
    //arrangement
    ifstream infile;
    infile.open ("seats.dat");
    
    fillSeatsArray (seats, seats2, infile);
    
    //Closing file
    infile.close();
    
    displaySeats (seats);
    
    //While the user still wants to reserve seats
    string quit;
    int counter = 0;
    while (quit != "No" && counter <= 28)
    {
        cout << "Which row would you like to sit in? ";
        cin >> row;
        
        //Error Checking row number entered by user
        while (row != 1 && row != 2 && row != 3 && row != 4 &&
                row != 5 && row != 6 && row != 7)
        {
            cout << endl;
            cout << "Invalid Entry. Try Again: ";
            cin >> row;
        }
        
        cout << "Which seat would you like to sit in? ";
        cin >> seat;
        
        //Error checking seat letter entered by user
        while (seat != "A" && seat != "B" && seat != "C" &&
                seat != "D")
        {
            cout << endl;
            cout << "Invalid Entry. Try Again: ";
            cin >> seat;
        }
        
        bool found = true;
        
        //Iterating through seats array and seats2 array
        //to find seat entered by user
        int loc = -1;
        for (int i = 0; i < ROWS; i++)
        {
            for (int j = 0; j < COLS; j++)
            {
                if (seats[i][j] == seat)
                    loc = j;
                seats2[row - 1][loc] = found;
            }
        }
        
        //If seat is already occupied
        if (seats[row - 1][loc] == "X")
            cout << "Seat OCCUPIED already." << endl;
        if (found)
            seats[row - 1][loc] = "X";
        
        
        displaySeats (seats);
        
        cout << "Would you like to choose another seat? Yes or "
                "No: ";
        cin >> quit;
        
        while (quit != "Yes" && quit != "No")
        {
            cout << "Invalid Entry. Try again: ";
            cin >> quit;
        }
        
        counter++;
        
        if (counter == 28)
        {
            cout << "All seats are OCCUPIED." << endl;
        }
        
    }
    
    cout << endl << "Thank you for flying with us." << endl;
    
    
    
}

/*
 * fillSeatsArray
 * (Parameters: 2 arrays, 1 infile stream)
 * 
 * This function reads the file contents of the seats.dat
 * file and fills the seats array with the contents of that
 * file to display to console.
 * 
 * No return.
 */
void fillSeatsArray (string seats [][COLS],bool seats2 [][COLS],
        ifstream& infile)
{
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLS; j++)
        {
            infile >> seats[i][j];
            seats2[i][j] = false;
        }
    }
}

/*
 * displaySeats
 * (1 string array)
 * 
 * This function simply takes in the array with the 
 * seating arrangement for the airplane and outputs it to the
 * console.
 * 
 * No return.
 */
void displaySeats (string seats [][COLS])
{
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLS; j++)
        {
            cout << seats[i][j] << " ";
        }
        cout << endl;
    }
}

//HOMEWORK PROBLEM #3/////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//Pointers Problem////////////////////////////////////////////
void problem3()
{
    int value = 42;
    int temp = value;
    
    int* p1 = &value;
    int* p2 = &temp;
    
    cout << "Value #1: " << value << endl;
    cout << "Value #1 Address: " << p1 << endl << endl;
    
    cout << "Value #2: " << temp << endl;
    cout << "Value #2 Address: " << p2 << endl << endl;
    
    //Changing Using Variable
    value = 12;
    cout << "Value #1: " << value << endl;
    
    cout << "Value #2: " << temp << endl;
    
    cout << endl;
    //Changing Using p1
    *p1 = 25;
    cout << "Value #1: " << value << endl;
    
    cout << "Value #2: " << temp << endl;
    
    cout << endl;
    //Changing Using p2
    *p1 = 60;
    p2 = p1;
    
    cout << "Value #1: " << value << endl;
    
    cout << "Value #2: " << temp << endl;
}


