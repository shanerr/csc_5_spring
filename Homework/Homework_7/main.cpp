/* 
 * File:   main.cpp
 * Author: Thomas
 *
 * Created on April 29, 2014, 8:21 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>


using namespace std;

void problem1();
void problem2();
void minMax (vector <int> &, int&, int&);
void evenNumbers (vector <int> &, int&);
void problem3();
void problem4();
void problem5();
void mergeFiles (ifstream&, ifstream&, ofstream&);
void problem6();
void countOdd (vector <int>&);
void problem7();
int findMax (vector <int>&);
void output (vector <int> &);
/*
 * 
 */
int main(int argc, char** argv) {
    
    cout << "Homework 7" << endl;
    cout << "Enter 1 for Problem #1/#2" << endl;
    cout << "Enter 2 for Problem #3" << endl;
    cout << "Enter 3 for Problem #4" << endl;
    cout << "Enter 4 for Problem #5" << endl;
    cout << "Enter 5 for Problem #6" << endl;
    cout << "Enter 6 for Problem #7/#8" << endl;
    cout << "Enter 7 for Problem #9/#10" << endl;
    int selection;
    cin >> selection;
    
    switch (selection)
    {
        case 1: 
            problem1();
            break;
        case 2:
            problem2();
            break;
        case 3:
            problem3();
            break;
        case 4:
            problem4();
            break;
        case 5:
            problem5();
            break;
        case 6:
            problem6();
            break;
        case 7:
            problem7();
            break;
        default:
            cout << "Error." << endl;
            break;
    }

    return 0;
}

void problem1()
{
    cout << "Please enter a file name: ";
    string fileName;
    cin >> fileName;
    
    ifstream infile;
    infile.open (fileName.c_str());
    
    while (infile.fail())
    {
        
        do
        {
            cout << "The file does not exist. Try again: ";
            cin >> fileName;
            infile.open (fileName.c_str());
        } while (!infile);
    }
    
    int contents;
    vector <int> values;
    
    while (!infile.eof())
    {
        infile >> contents;
        values.push_back (contents);
    }
    
    infile.close();
    
    int largest = 0;
    int smallest;
    
    minMax (values , largest, smallest);
    
    cout << "The largest number in the file is: " << largest 
            << endl;
    cout << "The smallest number in the file is: " << smallest
            << endl;
    
}

void problem2()
{
    cout << "Please enter a file name: ";
    string fileName;
    cin >> fileName;
    
    ifstream infile;
    infile.open (fileName.c_str());
    
    while (infile.fail())
    {
        
        do
        {
            cout << "The file does not exist. Try again: ";
            cin >> fileName;
            infile.open (fileName.c_str());
        } while (!infile);
    }
    
    int contents;
    vector <int> values;
    
    while (!infile.eof())
    {
        infile >> contents;
        values.push_back (contents);
    }
    
    infile.close();
    
    int numEvens = 0;
    evenNumbers (values, numEvens);
    
    cout << "There are " << numEvens << " even numbers in this"
            " file." << endl;
    
}

void minMax (vector <int>& values, int& x, int& y)
{
    for (int i = 0; i < values.size(); i++)
    {
        if (values [i] > x)
        {
            
            x = values [i];
        }
        if (values [i] < y)
        {
            y = values [i];
        }
    }
}

/*
 * Even Numbers Function
 * (vector and int as parameters)
 * This function takes in a vector and then iterates through
 * it using a for loop to find how many even numbers are in that 
 * vector.
 */
void evenNumbers (vector <int>& values, int& x)
{
    for (int i = 0; i < values.size(); i++)
    {
        if (values [i] % 2 == 0)
        {
            x++;
        }
    }
}

//Finding median in a group of integers in a file #4
void problem3()
{
    //Using fstream for file stream
    ifstream infile;
    
    //Opening file
    infile.open("data1.dat");
    
    int num;
    
    //Declaring a counter 
    int counter = 0;
    
    //Reading file until it ends
    while (!infile.eof())
    {
        counter++;
        cout << "Counter: " << counter << endl;
    }
    
    //Closing file
    infile.close();
    
    //Calculating Median of file
    ifstream readIn;
    
    readIn.open("data1.dat");
    
    if (counter % 2 == 0)//Even number of numbers in file
    {
        int before = 0;
        int after = 0;
        
        //Using for loop to read through each piece of data
        for (int i = 0; i < counter / 2; i++)
        {
            readIn >> before;
        }
        
        //Read the next number into after
        readIn >> after;
        
        cout << "Median is: " << (after + before) / 2.0 << endl;
        
    }
    else
    {
        int median;
        
        //9 / 2 = 4
        //Reading in all numbers before median
        for (int i = 0; i < counter / 2; i++)
        {
            readIn >> median;
        }
        
        //Next input will be median
        readIn >> median;
        cout << "The median is: " << median << endl;
    }
}

// Editing File Contents #5
void problem4()
{
    ifstream infile;
    
    cout << "Please enter a filename to read from: ";
    string fileName;
    cin >> fileName;
    
    infile.open(fileName.c_str());
    
    while (!infile)
    {
        cout << "The file does not exist. Try another: ";
        cin >> fileName;
        
        infile.open(fileName.c_str());
    }
    
    vector <string> contents;
    string word;
    
    while (!infile.eof())
    {   
        infile >> word;
        contents.push_back(word);
    }
    
    infile.close();
    
    ofstream outfile;
    
    cout << "Please enter a filename to write to: ";
    string outputFile;
    cin >> outputFile;
    
    outfile.open(outputFile.c_str());
    
   
    for (int i = 0; i < contents.size(); i++)
    {
        outfile << contents [i] << " ";
    }
    
    outfile.close();
}

//Merging Files Problem #6
void problem5()
{
    ifstream infile1;
    ifstream infile2;
    ofstream outfile;
    
    mergeFiles (infile1, infile2, outfile);
}

/*
 * Merge Files Function
 * (ifstream, ifstream, ofstream as parameters)
 * This function takes in two input file streams and one 
 * output file stream. It then attempts to establish
 * two vectors to store the data from the two input files and 
 * merge that data in sorted order to be output to the outfile
 * stream.
 */
void mergeFiles (ifstream& infile1, ifstream& infile2, ofstream&
outfile)
{
    infile1.open("data1.dat");
    
    vector <int> firstSet;
    int num;
    
    while (!infile1.eof())
    {
        infile1 >> num;
        firstSet.push_back(num);
    }
    
    infile1.close();
    
    infile2.open("data2.dat");
    
    vector <int> secondSet;
    int num2;
    
    while (!infile2.eof())
    {
        infile2 >> num2;
        secondSet.push_back(num2);
    }
    
    infile2.close();
    
    outfile.open("out.dat");
    
    vector <int> finalSet;
    
    for (int i = 0; i < firstSet.size() + secondSet.size(); i++)
    {
        if (firstSet[i] < secondSet[i])
        {
            finalSet.push_back(firstSet[i]);
            
            if (firstSet[i+1] < secondSet[i])
            {
                finalSet.push_back(firstSet[i+1]);
            }
            else
            {
                finalSet.push_back(secondSet[i]);
            }
        }
        if (secondSet[i] < firstSet[i])
        {
            finalSet.push_back(secondSet[i]);
            
            if (secondSet[i+1] < firstSet[i])
            {
                finalSet.push_back(secondSet[i+1]);
            }
            else
            {
                finalSet.push_back(firstSet[i]);
            }
        }
    }
    
    for (int i = 0; i < finalSet.size(); i++)
    {
        outfile << finalSet[i] << endl;
    }
    
    outfile.close();
}

//Ten Random Integer Vector #7
//Counting Number of Odd Integers #8
void problem6()
{
    srand(time(0));
    vector <int> tenRandom;
    
    int randNum;
    
    for (int i = 0; i < 10; i++)
    {
        randNum = (rand()% 50);
        tenRandom.push_back(randNum);
    }
    
    countOdd (tenRandom);
    
}

/*
 * Count Odd Numbers Function
 * (vector as parameter)
 * This function takes in a vector as a parameter and it 
 * then counts how many ODD numbers are in that vector using a 
 * for loop and a counter.
 */
void countOdd (vector <int>& tenRandom)
{
    int counter = 0;
    
    for (int i = 0; i < tenRandom.size(); i++)
    {
        if (tenRandom[i] % 2 != 0)
        {
            counter++;
        }
    }
    
    cout << endl;
    
    for (int i = 0; i < tenRandom.size(); i++)
    {
        cout << tenRandom[i] << " ";
    }
    
    cout << endl << "There are " << counter << " odd integers"
            " in this vector." << endl;
    
    
}

//Twenty Random Integer Vector #9
//Output function for vector #10
void problem7()
{
    srand(time(0));
    vector <int> twentyRandom;
    
    int randNum;
    
    for (int i = 0; i < 20; i++)
    {
        randNum = (rand() % 10);
        twentyRandom.push_back(randNum);
    }
    
    int maxValue = findMax (twentyRandom);
    
    output (twentyRandom);
    
    cout << "The maximum value for this vector is: " << maxValue 
            << endl;
    
}

/*
 * Find Max Function
 * (vector as parameter)
 * This function takes in a vector as a parameter and finds the 
 * maximum value in that vector. Then it returns that max value
 * to the program it was called by.
 */
int findMax (vector <int>& twentyRandom)
{
    int currentMax = 0;
    
    for (int i = 0; i < twentyRandom.size(); i++)
    {
        if (twentyRandom[i] > currentMax)
        {
            currentMax = twentyRandom[i];
        }
    }
    
    return currentMax;
    
}

/*
 * Output Function
 * (vector as parameter)
 * This function simply outputs each element of the vector that 
 * is being used.
 */
void output (vector <int>& twentyRandom)
{
    
    cout << endl;
    
    for (int i = 0; i < twentyRandom.size(); i++)
    {
        cout << twentyRandom[i] << " ";
    }
    
    cout << endl;
    
}


