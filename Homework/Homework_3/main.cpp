/*
* Name: Shane Brown
* Student ID: 2431194
* Date: 3-15-14 
* HW: #3
* Problem: 1-4
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Homework Problem #1
    //Prompt the User
    cout << "Please enter a telephone number with one dash"
            " in between: " << endl;
    
    //User Input
    string input;
    cin >> input;
    
    //Finding Dash in phone number
    int dashLoc = input.find ("-");
    
    //Getting first half and second half of phone number
    string firstHalf = input.substr (0, dashLoc);
    string secondHalf = input.substr (dashLoc + 1);
    
    //Outputting Phone number without dash
    cout << firstHalf << secondHalf << endl; 
    
    cout << endl;
    
    //Homework Problem #2 
    //Prompt the user 
    
    //Variable declaration
    int numApples, numOranges, numPears;
    
    //User input
    cout << "Enter the number of Apples: ";
    cin >> numApples;
    cout << "Enter the number of Oranges: ";
    cin >> numOranges;
    cout << "Enter the number of Pears: ";
    cin >> numPears;
    cout << endl;
    
    //More variables
    int numApplesLeave, numOrangesLeave, numPearsLeave;
    
    //If else statements with Calculations
    if (numApples < numOranges && numApples < numPears)
    {
        numApplesLeave = 0;
        numOrangesLeave = numOranges - numApples;
        numPearsLeave = numPears - numApples;
        
        cout << "The number of Apples to leave: " <<
                numApplesLeave << endl;
        cout << "The number of Oranges to leave: " <<
                numOrangesLeave << endl;
        cout << "The number of Pears to leave: " <<
                numPearsLeave << endl;
    }
    
    else if (numOranges < numApples && numOranges < numPears)
    {
        numOrangesLeave = 0;
        numPearsLeave = numPears - numOranges;
        numApplesLeave = numApples - numOranges;
        
        cout << "The number of Apples to leave: " <<
                numApplesLeave << endl;
        cout << "The number of Oranges to leave: " <<
                numOrangesLeave << endl;
        cout << "The number of Pears to leave: " <<
                numPearsLeave << endl;
    }
    
    else if (numPears < numApples && numPears < numOranges)
    {
        numPearsLeave = 0;
        numApplesLeave = numApples - numPears;
        numOrangesLeave = numOranges - numPears;
        
        cout << "The number of Apples to leave: " <<
                numApplesLeave << endl;
        cout << "The number of Oranges to leave: " <<
                numOrangesLeave << endl;
        cout << "The number of Pears to leave: " <<
                numPearsLeave << endl;
    }
    
    else if (numApples == numOranges && numOranges == numPears 
            && numPears == numApples)
    {
        numPearsLeave = 0;
        numApplesLeave = 0;
        numOrangesLeave = 0;
        
        cout << "The number of Apples to leave: " <<
                numApplesLeave << endl;
        cout << "The number of Oranges to leave: " <<
                numOrangesLeave << endl;
        cout << "The number of Pears to leave: " <<
                numPearsLeave << endl;
    }
    
    else 
    {
       cout << "There was an incorrect entry" << endl; 
    }
    
    cout << endl;
    
    
    
    // Homework Problem #3
    
    //Varaible Declaration
    int maxRoomCap, numPeopleAtt, numPeopleAdd, numPeopleSub;
    
    //Prompting User
    cout << "Please enter the maximum room capacity: ";
    cin >> maxRoomCap;
    cout << "Please enter the number of people attending: ";
    cin >> numPeopleAtt;
    
    //If else with Calculations 
    if (maxRoomCap >= numPeopleAtt)
    {
        cout << "According to fire regulations, this meeting is "
                "LEGAL." << endl;
        numPeopleAdd = maxRoomCap - numPeopleAtt;
        
        cout << "You may add " << numPeopleAdd << " people to "
                "your meeting." 
                << endl << endl;
    }
    else 
    {
        cout << "According to fire regulations, this meeting is "
                "ILLEGAL." << endl;
        
        numPeopleSub = numPeopleAtt - maxRoomCap;
        
        cout << "You must subtract " << numPeopleSub << " people "
                "for your meeting to meet fire standards." <<
                endl;
    }
    
    
    
    //Homework Problem #4
    
    //Variable Declaration
    int num1, num2, num3, num4, num5, num6, num7, num8, num9,
            num10, numNeg1, numNeg2, numNeg3, numNeg4, 
            numNeg5, numNeg6, numNeg7, numNeg8, numNeg9,
            numNeg10, numPos1, numPos2, numPos3,
            numPos4, numPos5, numPos6, numPos7, numPos8,
            numPos9, numPos10, sumPosNums, sumNegNums, sumTotal;
    
    //Prompting User 
    cout << "Please enter 10 numbers, positive or negative: "
            << endl;
    
    //User Input
    cin >> num1 >> num2 >> num3 >> num4 >> num5 >> num6 
            >> num7 >> num8 >> num9 >> num10;
    
    //Starting Both Sums at Zero
    sumPosNums = 0;
    sumNegNums = 0; 
    
    
    //If else statements and Calculations
    if (num1 < 0)
    {
        sumNegNums += num1;
    }
    else 
    {
        sumPosNums += num1;
    }
    
    if (num2 < 0)
    {
        sumNegNums += num2;
    }
    else
    {
        sumPosNums += num2;
    }
    if (num3 < 0)
    {
        sumNegNums += num3;
    }
    else
    {
        sumPosNums += num3;
    }
    if (num4 < 0)
    {
        sumNegNums += num4;
    }
    else
    {
        sumPosNums += num4;
    }
    if (num5 < 0)
    {
        sumNegNums += num5;
    }
    else 
    {
        sumPosNums += num5;
    }
    if (num6 < 0)
    {
        sumNegNums += num6;
    }
    else 
    {
        sumPosNums += num6;
    }
    if (num7 < 0)
    {
        sumNegNums += num7;
    }
    else 
    {
        sumPosNums += num7;
    }
    if (num8 < 0)
    {
        sumNegNums += num8;
    }
    else
    {
        sumPosNums += num8;
    }
    if (num9 < 0)
    {
        sumNegNums += num9;
    }
    else
    {
        sumPosNums += num9;
    }
    if (num10 < 0)
    {
        sumNegNums += num10;
    }
    else 
    {
        sumPosNums += num10;
    }
    
    
    cout << "Sum of Positive Numbers: " << sumPosNums << endl;
    cout << "Sum of Negative Numbers: " << sumNegNums << endl;
    cout << endl << "Numbers used: " << num1 << "," << num2 <<
            "," << num3 << "," << num4 << "," << num5 << ","
            << num6 << "," << num7 << "," << num8 << "," <<
            num9 << "," << num10 << endl;
    
    
    return 0;
}

