/*
 *  Name: Shane Brown
 * ID: 2431194
 * HW: #11
 * Date: 6-2-14
 * Class: Mon./Wed.
 *
 */

#include <cstdlib>
#include <iostream>
#include <fstream>


using namespace std;

//Function Prototypes
void problem1();
void problem2();
void problem3();
void problem5();
void problem9();
void problem10();

/*
 * 
 */
int main(int argc, char** argv) {
   
    //MAIN MENU//////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    int selection;
    
    while (selection != -1)
    {
        cout << "Homework 11" << endl << endl;
    
         cout << "You may enter -1 to Quit." << endl << endl;
    
         cout << "Enter 1 for Problem #1" << endl;
         cout << "Enter 2 for Problem #2" << endl;
         cout << "Enter 3 for Problems #3/#4" << endl;
         cout << "Enter 5 for Problem #5-8" << endl;
         cout << "Enter 9 for Problem #9" << endl;
         cout << "Enter 10 for Problem #10" << endl;
    
        cin >> selection;
    
        switch (selection)
        {
            case -1:
                cout << "Goodbye.";
                break;
            case 1:
                problem1();
                break;
            case 2:
                problem2();
                break;
            case 3:
                problem3();
                break;
            case 5:
                problem5();
                break;
            case 9:
                problem9();
                break;
            case 10:
                problem10();
                break;
            default:
                cout << "Error." << endl;
                break;
        }
    }
    
    
    return 0;
}

//PROBLEM #1//////////////////////////////////////////////////////////
//Time Structure/////////////////////////////////////////////////////
void problem1()
{
    
    cout << "See code in void problem1()..." << endl << endl;
    
    struct time
    {
        int seconds;
        int minutes;
        int hours;
    };
}

//PROBLEM #2/////////////////////////////////////////////////////////////
//Time Class////////////////////////////////////////////////////////////
void problem2()
{
    cout << "See code in void problem2()..." << endl << endl;
    
    class time 
    {
        private:
            
        public:
            int seconds;
            int minutes;
            int hours;
        
    };
    
}

//PROBLEMS #3 and #4////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void problem3()
{
    struct timeStructure
    {
        int seconds;
        int minutes;
        int hours;
    };
    
    timeStructure s;
    s.hours = 1;
    s.minutes = 60;
    s.seconds = 3600;
    
    
    
    cout << "Using Structure (Without Member Functions):" << endl;
    cout << s.hours << " hour is equal to " << s.minutes << " minutes "
            "which is equal to " << s.seconds << " seconds." << endl << endl;
    
    
    class Time 
    {
    private:
        
    public:
        int seconds;
        int minutes;
        int hours;
 
    };
    
    Time c;
    
    c.seconds = 3600;
    c.minutes = 60;
    c.hours = 1;
    
    cout << "Using Class (Member Functions):" << endl;
    cout << c.hours << " hour is equal to " << c.minutes << " minutes "
            "which is equal to " << c.seconds << " seconds." << endl << endl;
    
}

//Time class for problems 5-8
class TimeNumber2
    {
    private:
        int seconds;
        int minutes;
        int hours; 
    public:
        TimeNumber2();
        TimeNumber2(int, int, int);
        void setSeconds(int);
        void setMinutes(int);
        void setHours(int);
        int getSeconds();
        int getMinutes();
        int getHours();
        void output (ostream&);
        
    };
    
    TimeNumber2::TimeNumber2()
    {
        hours = 0;
        minutes = 0;
        seconds = 0;
    }
TimeNumber2::TimeNumber2 (int seconds, int minutes, int hours)    
{
    this-> seconds = seconds;
    this-> minutes = minutes;
    this-> hours = hours;
}

void TimeNumber2::setSeconds(int seconds)
{
    this-> seconds = 3600;
}

void TimeNumber2::setMinutes(int minutes)
{
    this-> minutes = 60;
}

void TimeNumber2::setHours(int hours)
{
    this-> hours = 1;
}

int TimeNumber2::getSeconds()
{
    return seconds;
}

int TimeNumber2::getMinutes()
{
    return minutes;
}

int TimeNumber2::getHours()
{
    return hours;
}

void TimeNumber2::output (ostream& out)
{
    out << "Hours: " << hours << endl;
    out << "Minutes: " << minutes << endl;
    out << "Seconds: " << seconds << endl;
    
    ofstream outfile;
    outfile.open ("data.dat");
    outfile << "Hours: " << hours << endl;
    outfile << "Minutes: " << minutes << endl;
    outfile << "Seconds: " << seconds << endl;
    
    outfile.close();
}





//PROBLEMS #5-8///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
void problem5()
{
    TimeNumber2 t;
    
    t.setSeconds (3600);
    t.setMinutes (60);
    t.setHours (1);
    
    cout << "Using Class (Member Functions):" << endl;
    cout << t.getHours() << " hour is equal to " 
            << t.getMinutes() << " minutes "
            "which is equal to " << t.getSeconds() << " seconds." 
            << endl << endl;
    
    t.output (cout);
 }

class TimeNumber3
{
private:
    int hours;
    int minutes;
    int seconds;
public:
    TimeNumber3();
    TimeNumber3 (int, int, int);
};

TimeNumber3::TimeNumber3()
{
    hours = 1;
    minutes = 60;
    seconds = 3600;
}

TimeNumber3::TimeNumber3 (int hours, int minutes, int seconds)
{
    this->hours = hours;
    this->minutes = minutes;
    this->seconds = seconds;
}

//CONFUSED ON WHAT THE PROBLEM WAS ASKING/////////////////
/////////////////////////////////////////////////////////
void problem9()
{
    cout << "Confused on this problem..." << endl;
    
    int hours = 1;
    int minutes = 60;
    int seconds = 3600;
    
    TimeNumber3 instance1();
    TimeNumber3 instance2();
}

//Problem #10/////////////////////////////////////////
/////////////////////////////////////////////////////

//Date CLASS inheriting from Time CLASS
class Date : TimeNumber2
{
private:
    int month;
    int year;
public:
    Date ();
    Date (int, int);
    Date (int ,int , int ,int ,int);
    void output (ostream&);
};

Date::Date ()
{
    month = 0;
    year = 0;
}

Date::Date (int month, int year)
{
    this->month = month;
    this->year = year;
}

Date::Date (int hours, int minutes, int seconds, int month, int 
year)
:TimeNumber2 (hours, minutes, seconds)
{
    this->month = month;
    this->year = year;
}

void Date::output (ostream& out)
{
    TimeNumber2::output(out);
    
    out << "Seconds in a month: " << month << endl;
    out << "Seconds in a year: " << year << endl;
}

void problem10()
{
    Date num1;
    Date num2 (2592000, 31104000);
    Date num3 (3600, 60, 1, 2592000, 31104000);
    
    num1.output(cout);
    
    cout << endl;
    
    num2.output(cout);
    
    cout << endl;
    
    num3.output(cout);
    
    cout << endl;
    
    
}


    

    


